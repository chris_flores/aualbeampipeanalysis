/************************************************************************
 **
 **
 **
 **
 **
 **
 ************************************************************************/

//Define Constants used for setting Bin Numbers
#define nCentralityBins 10
#define nRapidityBins 25
#define nParticles 10
#define nmTm0Bins 40

//Define Constants used for Bin Widths
#define rapidityWidth .1
#define mTm0Width .025

//________________________________________________________________________
TString GetParticleName(Int_t ParticleIndex);

//________________________________________________________________________
TString GetSideName(Int_t SideIndex);

//________________________________________________________________________
TString GetCollisionName(Int_t collisionNameIndex);

//________________________________________________________________________
TString GetParticleSymbol(Int_t ParticleIndex);

//________________________________________________________________________
Double_t GetParticleMass(Int_t ParticleIndex);

//_____________________________________________________________________
TString GetGeantID(Int_t ParticleIndex);

//______________________________________________________________________
Float_t GetRapidityRangeLow(Int_t RapidityIndex, Int_t side,
			    Double_t binWidth=rapidityWidth);

//______________________________________________________________________
Float_t GetRapidityRangeHigh(Int_t RapidityIndex, Int_t side,
			     Double_t binWidth=rapidityWidth);

//_____________________________________________________________________
Float_t GetRapidityRangeCenter(Int_t RapidityIndex, Int_t side,
			       Double_t binWidth=rapidityWidth);

//_____________________________________________________________________
Int_t FindRapidityIndex(Double_t trackRapidity, Int_t side, 
			Int_t yBins=nRapidityBins, 
			Double_t binWidth=rapidityWidth);

//_____________________________________________________________________
Double_t ComputemTm0(Double_t pT, Int_t particleID);
//______________________________________________________________________
Double_t ConvertmTm0ToPt(Double_t mTm0, Int_t particleID);

//______________________________________________________________________
Int_t FindmTm0Index(Double_t trackmTm0, Int_t mTm0Bins=nmTm0Bins,
		    Double_t binWidth=mTm0Width);

//______________________________________________________________________
Float_t GetmTm0RangeLow(Int_t mTm0Index, Double_t binWidth=mTm0Width);

//______________________________________________________________________
Float_t GetmTm0RangeHigh(Int_t mTm0Index, Double_t binWidth=mTm0Width);

//______________________________________________________________________
Float_t GetmTm0RangeCenter(Int_t mTm0Index, Double_t binWidth=mTm0Width);

//______________________________________________________________________
Double_t ComputepTotal(Double_t pT, Double_t pZ);

//______________________________________________________________________
Int_t FindGenericIndex(Double_t trackVal, Int_t nBins, Double_t binWidth);

//______________________________________________________________________
Float_t GetGenericRangeLow(Int_t index, Double_t binWidth);

//______________________________________________________________________
Float_t GetGenericRangeHigh(Int_t index, Double_t binWidth);

//______________________________________________________________________
Float_t GetGenericRangeCenter(Int_t index, Double_t binWidth);

//______________________________________________________________________
Float_t GetMidRapidity(Double_t ENERGY);

//_____________________________________________________________________
Color_t GetParticleColor(Int_t particleID);


//_____________________________________________________________________
Style_t GetParticleMarker(Int_t particleID);

//_____________________________________________________________________
Double_t ComputeEta(Double_t y, Double_t pT, Double_t m0);



//_____________________________________________________________________
// Double_t InvertBoseEinstein(Double_t Amp, Double_t Slope, Double_t pMass){

//   //Returns the dN/dy of the Spectra Fits by numerically inverting the
//   //Bose-Einstein Function.  Here we integrate the fitting function over dmT
//   //from 0 to 5 GeV in .01 GeV Steps

//   Double_t stepWidth = .01;
//   Double_t maxRange = 5.0;
//   Int_t nSteps = (Int_t)(maxRange/stepWidth);
//   Double_t sum = 0.0;
//   Double_t x = 0.01; // x = mT-m0

//   for (Int_t stepIndex=0; stepIndex < nSteps; stepIndex++){

//     //Increment x
//     x += stepIndex*stepWidth;

//     sum += ( (x + pMass)/(exp((x)/Slope)-1.0) ) * stepWidth;

//   } 

//   return 2*3.1459*Amp*sum;


// }

//_____________________________________________________________________
Double_t InvertBoseEinstein(Double_t Slope, Double_t pMass);


//___________________________________________________________________________
Double_t BoseEinsteinFitFunc(Double_t *x, Double_t *par);

//___________________________________________________________________________
Double_t DoubleBoseEinsteinFitFunc(Double_t *x, Double_t *par);


//___________________________________________________________________________
Double_t ThermalFitFunc(Double_t *x, Double_t *par);

//____________________________________________________________________________
Double_t PowerLaw(Double_t *x, Double_t *par);

//____________________________________________________________________________
Double_t PowerLawPlusBoseEinsteinFunc(Double_t *x, Double_t *par);

//____________________________________________________________________________
Double_t ComputeRapidity(Double_t trackpT, Double_t trackpZ, Double_t pMass);



//___________________________________________________________________________
void SaveImage(TCanvas *canvas, TString dir, TString name, TString ext);

//___________________________________________________________________________
void SaveObject(TObject *object, TFile *file, TString dir);

//____________________________________________________________________________
TString ImageDir(Double_t ENERGY);
