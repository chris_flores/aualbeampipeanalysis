Double_t linePtConstEta(Double_t *x, Double_t *par){

  //Draws a Line of Constant Eta on acceptance Plots
  // y(pt,{eta,mass})

  Double_t pT = x[0];

  Double_t eta = par[0];
  Double_t m0 = par[1];
  
  Double_t numer = sqrt(m0*m0 + pow(pT*cosh(eta),2)) + pT*sinh(eta);

  Double_t denom = sqrt(m0*m0 + pT*pT);

  Double_t f = log( numer / denom );

  return f;

}

Double_t linemTm0ConstEta(Double_t *x, Double_t *par){

  //Draws a Line of Constant Eta on acceptance Plots
  // y(pt,{eta,mass})

  Double_t mTm0 = x[0];

  Double_t eta = par[0];
  Double_t m0 = par[1];
  
  Double_t mT = mTm0 + m0;
  Double_t pT = sqrt(mT*mT - m0*m0);

  Double_t numer = sqrt(m0*m0 + pow(pT*cosh(eta),2)) + pT*sinh(eta);

  Double_t denom = sqrt(m0*m0 + pT*pT);

  Double_t f = log( numer / denom );

  return f;

}

Double_t invLineConstEta(Double_t *x, Double_t *par){

  //Returns the inverted form of lineConstEta above
  //pT(y,{eta,mass})

  Double_t y = x[0];

  Double_t eta = par[0];
  Double_t mass = par[1];

  TF1 *f1 = new TF1("InvConstEta",linePtConstEta,0,10.0,2);

  f1->FixParameter(0,eta);
  f1->FixParameter(1,mass);

  return f1->GetX(y);

}


Double_t invmTm0LineConstEta(Double_t *x, Double_t *par){

  //Returns the inverted form of lineConstEta above
  //pT(y,{eta,mass})

  Double_t y = x[0];

  Double_t eta = par[0];
  Double_t mass = par[1];

  TF1 *f1 = new TF1("InvConstEta",linemTm0ConstEta,0,10.0,2);

  f1->FixParameter(0,eta);
  f1->FixParameter(1,mass);

  return f1->GetX(y);

}
