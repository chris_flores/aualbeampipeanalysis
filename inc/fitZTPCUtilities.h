//These are commonly used functions used when extracting particle
//yeilds from the Z TPC Variable

//PID Functions
TF1 *tpcPIDFunc[nParticles];
TF1 *tofPIDFunc[nParticles];

//Particle Parameterization Struct
struct ParticlePars{

  //Parameters as a function of mTm0
  TGraphErrors *amp[nRapidityBins], *mean[nRapidityBins],
    *width[nRapidityBins];
  Int_t ampPoint[nRapidityBins], meanPoint[nRapidityBins],
    widthPoint[nRapidityBins];

  TF1 *ampFit[nRapidityBins], *meanFit[nRapidityBins],
    *widthFit[nRapidityBins];

  //Parameters as a function of Rapidity
  TGraphErrors *ampY[nmTm0Bins], *meanY[nmTm0Bins],
    *widthY[nmTm0Bins];
  Int_t ampYPoint[nmTm0Bins], meanYPoint[nmTm0Bins],
    widthYPoint[nmTm0Bins];

  TF1 *ampYFit[nmTm0Bins], *meanYFit[nmTm0Bins],
    *widthYFit[nmTm0Bins];

};

//____________________________________________________________________________
void DefineParticlePars(ParticlePars *p, TString name){

  //First Loop Over the rapidity bins to define the components as a function
  //of mTm0
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

    p->amp[yIndex] = new TGraphErrors();
    p->amp[yIndex]->SetName(Form("ampGraph_%s_%02d",name.Data(),yIndex));
    p->amp[yIndex]->SetTitle(Form("%s Amplitude %02d",name.Data(),yIndex));
    p->amp[yIndex]->SetMarkerStyle(20);
    p->ampPoint[yIndex] = 0;

    p->mean[yIndex] = new TGraphErrors();
    p->mean[yIndex]->SetName(Form("meanGraph_%s_%02d",name.Data(),yIndex));
    p->mean[yIndex]->SetTitle(Form("%s Mean %02d",name.Data(),yIndex));
    p->mean[yIndex]->SetMarkerStyle(20);
    p->meanPoint[yIndex] = 0;

    p->width[yIndex] = new TGraphErrors();
    p->width[yIndex]->SetName(Form("widthGraph_%s_%02d",name.Data(),yIndex));
    p->width[yIndex]->SetTitle(Form("%s Width %02d",name.Data(),yIndex));
    p->width[yIndex]->SetMarkerStyle(20);
    p->widthPoint[yIndex] = 0;

  }

  //Second loop over the mTm0Bins to define the compoents as a function of y
  for (Int_t mTm0Index =0; mTm0Index<nmTm0Bins; mTm0Index++){

    p->ampY[mTm0Index] = new TGraphErrors();
    p->ampY[mTm0Index]->SetName(Form("ampYGraph_%s_%02d",name.Data(),mTm0Index));
    p->ampY[mTm0Index]->SetTitle(Form("%s Amplitude %02d",name.Data(),mTm0Index));
    p->ampY[mTm0Index]->SetMarkerStyle(20);
    p->ampYPoint[mTm0Index] = 0;

    p->meanY[mTm0Index] = new TGraphErrors();
    p->meanY[mTm0Index]->SetName(Form("meanYGraph_%s_%02d",name.Data(),mTm0Index));
    p->meanY[mTm0Index]->SetTitle(Form("%s Mean %02d",name.Data(),mTm0Index));
    p->meanY[mTm0Index]->SetMarkerStyle(20);
    p->meanYPoint[mTm0Index] = 0;

    p->widthY[mTm0Index] = new TGraphErrors();
    p->widthY[mTm0Index]->SetName(Form("widthYGraph_%s_%02d",name.Data(),mTm0Index));
    p->widthY[mTm0Index]->SetTitle(Form("%s Width %02d",name.Data(),mTm0Index));
    p->widthY[mTm0Index]->SetMarkerStyle(20);
    p->widthYPoint[mTm0Index] = 0;

  }


}

//____________________________________________________________________________
void AddPointToParGraphmTm0(TGraphErrors *parGraph, Int_t *point, TF1 *func, 
			    Int_t par, Int_t mTm0Index){

  parGraph->SetPoint(*point,
		     GetmTm0RangeCenter(mTm0Index),
		     func->GetParameter(par));
  parGraph->SetPointError(*point,
			  0,
			  func->GetParError(par));
  (*point)++;

}

//____________________________________________________________________________
void AddPointToParGraphY(TGraphErrors *parGraph, Int_t *point, TF1 *func, 
			    Int_t par, Int_t yIndex){

  parGraph->SetPoint(*point,
		     GetRapidityRangeCenter(yIndex,0),
		     func->GetParameter(par));
  parGraph->SetPointError(*point,
			  0,
			  func->GetParError(par));
  (*point)++;

}


//____________________________________________________________________________
Double_t OneMinusInvExp(Double_t *x, Double_t *par){

  Double_t xx = x[0];

  return par[0] - par[1]*exp(-pow(par[2]/xx,par[3]));

}


//______________________________________________________________________________
Double_t ComputeTPCZ(Int_t interestPID, Int_t confoundPID, Int_t yIndex, Int_t mTm0Index){

  //Compute the Total Momentum Assuming interestPID Mass
  Double_t pT = ConvertmTm0ToPt(GetmTm0RangeCenter(mTm0Index),interestPID);
  Double_t eta = ComputeEta(GetRapidityRangeCenter(yIndex,0),pT,GetParticleMass(interestPID));

  Double_t totalP = pT * cosh(eta);

  Double_t interestdEdx = tpcPIDFunc[interestPID]->Eval(totalP);
  Double_t confounddEdx = tpcPIDFunc[confoundPID]->Eval(totalP);
    
  return log(confounddEdx/interestdEdx);

}

//______________________________________________________________________________
Double_t ComputeTPCZ_mTm0Func(Int_t interestPID, Int_t confoundPID, Int_t yIndex, Double_t mTm0){

  //Compute the Total Momentum Assuming interestPID Mass
  Double_t pT = ConvertmTm0ToPt(mTm0,interestPID);
  Double_t eta = ComputeEta(GetRapidityRangeCenter(yIndex,0),pT,GetParticleMass(interestPID));

  Double_t totalP = pT * cosh(eta);

  Double_t interestdEdx = tpcPIDFunc[interestPID]->Eval(totalP);
  Double_t confounddEdx = tpcPIDFunc[confoundPID]->Eval(totalP);
    
  return log(confounddEdx/interestdEdx);

}

//______________________________________________________________________________
Double_t ConfoundMeanFit(Double_t *x, Double_t *par){

  Double_t xx = x[0];

  Int_t pidInterest = (Int_t)par[0];
  Int_t pidConfound = (Int_t)par[1];
  Int_t yIndex = (Int_t)par[2];

  return par[3] + par[4]*ComputeTPCZ_mTm0Func(pidInterest,pidConfound,yIndex,xx);

}

//_____________________________________________________________________________
Double_t ConfoundMeanFit1(Double_t *x, Double_t *par){

  Double_t xx = x[0];

  Int_t pidInterest = (Int_t)par[0];
  Int_t pidConfound = (Int_t)par[1];
  Int_t yIndex = (Int_t)par[2];

  return par[3] + par[4]*pow(ComputeTPCZ_mTm0Func(pidInterest,pidConfound,yIndex,xx),par[5]);

}

//_____________________________________________________________________________
Double_t SwooshFunc(Double_t *x, Double_t *par){

  Double_t xx = x[0];

  //Piecewise
  TF1 f1("f1","[0]",par[2],2);
  f1.FixParameter(0,par[0]);

  if (xx >= par[2])
    return f1.Eval(xx);

  TF1 f2("f2","[0]+[1]*(x-[2])+[3]*pow(x-[2],2)",0,par[2]);
  f2.FixParameter(0,f1.Eval(par[2]));
  f2.SetParameter(1,par[1]);
  f2.FixParameter(2,par[2]);
  f2.SetParameter(3,par[3]);

  return f2.Eval(xx);

    //return par[0] - ( (par[1] - exp(-pow(xx,par[3]))) / (par[2]+xx) );

}

//_____________________________________________________________________________
void LoadPIDFunctions(TFile *pidFile){

  TString name[nParticles] =
    {"Pion", "Pion", "Kaon", "Kaon", "Proton",
     "Proton", "Electron", "Electron", "Deuteron", "Deuteron"};

  for (Int_t i=0; i<nParticles; i++){
    tpcPIDFunc[i] = (TF1*)pidFile->Get(name[i]);
    tofPIDFunc[i] = (TF1*)pidFile->Get("TOF"+name[i]);
  }

}
