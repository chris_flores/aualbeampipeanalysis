#ifndef TRACKCLASS_H
#define TRACKCLASS_H

//Contains the TRACK Class Declaration

#include <stdlib.h>
#include <TROOT.h>
#include "Riostream.h"

//Track Class
class TRACK:public TObject{

 private:
  Int_t nHitsFit;
  Int_t nHitsPoss;
  Int_t trackFlag;
  Double_t pZ;
  Double_t pT;
  Double_t xVertex;
  Double_t yVertex;
  Double_t zVertex;
  Double_t dEdx;
  Double_t charge;
  Double_t tof;
  Double_t tofBeta;
  Double_t eta;
  Double_t phi;
  Double_t nSigPion;
  Double_t nSigKaon;
  Double_t nSigProton;
  Double_t yPion;
  Double_t yKaon;
  Double_t yProton;
  Double_t yDeuteron;
  Double_t yTriton;
  Double_t yHelium;
  Double_t dcaD;
  Double_t dcaZ;
  Double_t nHits;
  Double_t dEdxHits;
  Double_t firstZPoint;
  Double_t lastZPoint;
  Double_t tofSigElectron;
  Double_t tofSigPion;
  Double_t tofSigKaon;
  Double_t tofSigProton;
  Double_t pathLength;
  Double_t globalDCAx;
  Double_t globalDCAy;
  Double_t globalDCAz;
  Double_t primaryDCAx;
  Double_t primaryDCAy;
  Double_t primaryDCAz;
  


 public:
  TRACK();
  TRACK(Float_t *trackArray);
  virtual ~TRACK();

  void SetTrackAttributes(Float_t *trackarray);
  void PrintTrack();

  Int_t GetnHitsFit(){return nHitsFit;}
  Int_t GetnHitsPoss(){return nHitsPoss;}
  Int_t GetTrackFlag(){return trackFlag;}
  Double_t GetpZ(){return pZ;}
  Double_t GetpT(){return pT;}
  Double_t GetxVertex(){return xVertex;}
  Double_t GetyVertex(){return yVertex;}
  Double_t GetzVertex(){return zVertex;}
  Double_t GetdEdx(){return dEdx;}
  Double_t GetCharge(){return charge;}
  Double_t GetTOF(){return tof;}
  Double_t GetTOFBeta(){return tofBeta;}
  Double_t GetEta(){return eta;}
  Double_t GetPhi(){return phi;}
  Double_t GetnSigPion(){return nSigPion;}
  Double_t GetnSigKaon(){return nSigKaon;}
  Double_t GetnSigProton(){return nSigProton;}
  Double_t GetyPion(){return yPion;}
  Double_t GetyKaon(){return yKaon;}
  Double_t GetyProton(){return yProton;}
  Double_t GetyDeuteron(){return yDeuteron;}
  Double_t GetyHelium(){return yHelium;}
  Double_t GetDCAd(){return dcaD;}
  Double_t GetDCAz(){return dcaZ;}
  Double_t GetnHits(){return nHits;}
  Double_t GetdEdxHits(){return dEdxHits;}
  Double_t GetFirstZPoint(){return firstZPoint;}
  Double_t GetLastZPoint(){return lastZPoint;}
  Double_t GetTOFSigElectron(){return tofSigElectron;}
  Double_t GetTOFSigPion(){return tofSigPion;}
  Double_t GetTOFSigKaon(){return tofSigKaon;}
  Double_t GetTOFSigProton(){return tofSigProton;}
  Double_t GetPathLength(){return pathLength;}
  Double_t GetGlobalDCAx(){return globalDCAx;}
  Double_t GetGlobalDCAy(){return globalDCAy;}
  Double_t GetGlobalDCAz(){return globalDCAz;}
  Double_t GetPrimaryDCAx(){return primaryDCAx;}
  Double_t GetPrimaryDCAy(){return primaryDCAy;}
  Double_t GetPrimaryDCAz(){return primaryDCAz;}

  ClassDef(TRACK,2);

};

#endif
