/*****************************************************************************
 ** This Macro skims the data by applying the event selection criteria      **
 **                                                                         **
 **   There are four cuts which need to by applied (some have               **
 **   been applided at the "data collection maker" level, but they          **
 **   are enforced here for completeness.  The four Cuts are:               **
 **     1. ZVertex - Make sure the z vertex of the event is between         **
 **        150 and 200 cm away from the center of the tpc (maker level)     **
 **     2. RVertex - Make sure the r vertex of the event is between         **
 **        2 and 5 cm from the beamline (maker level)                       **
 **     3. Total Z Momentum - Depending on whether the event has a          **
 **        positive or negative z vertex we make sure that the sum          **
 **	   of all the tracks z momenta are either negative (for             **
 **	   positive z vertex events) or positive (for negative z            **
 **	   vertex events).                                                  **
 **     4. Pion Multiplicity - The top 10% most central AuAl collisions     **
 **        will have a pion multiplicity of (depending on energy)           **
 **	   3.0 GeV - 11 Pions  | 3.5 GeV - 19 Pions | 4.5 GeV - 31 Pions    **
 **                                                                         **
 ** Author: Christopher E. Flores (UC Davis)                                **
 ** Email:  chrflores@ucdavis.edu                                           **
 **                                                                         ** 
 *****************************************************************************/

#include "../inc/includeList.h"

void dataSkimmer(TString INPUTFILE, TString OUTPUTFILE, Double_t ENERGY){

  //Open the Inputfile and Get the TTree
  TFile *inFile = new TFile(INPUTFILE,"READ");
  TTree *inTree = (TTree*)inFile->Get("Events");
  
  //Create an Event to hold the Entry Info
  EVENT *event = new EVENT();

  //Set the Branch address
  TBranch *inBranch = inTree->GetBranch("EventList");
  inBranch->SetAddress(&event);

  //Create the Outputfile and TTree
  TFile *outFile = new TFile(OUTPUTFILE,"RECREATE");
  //TTree *outTree = new TTree("Events","Events");
  //TBranch *leftBranch  = outTree->Branch("LeftEventList",&event,1000000000);
  //TBranch *rightBranch = outTree->Branch("RightEventList",&event,1000000000);
  TTree *leftTree  = new TTree("LeftEvents","LeftEvents");
  TTree *rightTree = new TTree("RightEvents","RightEvents");
  leftTree->Branch("LeftEventList",&event,1000000);
  rightTree->Branch("RightEventList",&event,1000000);

  //R Vertex Cut Definitions
  Double_t rVertexMin = 2.0;
  Double_t rVertexMax = 5.0;

  //Z Vertex Cut Definitions
  Double_t leftMinZ  = -200.0;
  Double_t leftMaxZ  = -150.0;
  Double_t rightMinZ = 150.0;
  Double_t rightMaxZ = 200.0;

  //Pion Multiplicity Cut Definitions
  Int_t minPion3_0 = 11;
  Int_t minPion3_5 = 19;
  Int_t minPion4_5 = 31;

  //Variables Used to Apply the Cuts in the Loop
  Double_t minZ=0, maxZ=0;
  Int_t minPions=0;

  //Couting Variables
  Int_t nLeftEvents(0);
  Int_t nRightEvents(0);
  
  //Define the Pion Cut
  if (ENERGY == 3.0)
    minPions = minPion3_0;
  else if (ENERGY == 3.5)
    minPions = minPion3_5;
  else if (ENERGY == 4.5)
    minPions = minPion4_5;

  //Get the Number of entries in the TTree
  Long64_t nEvents = inTree->GetEntries();

  cout <<"Total Number of Events: " <<nEvents <<"\n";
  cout <<"Processing Event: \n";

  //Loop Over the Events and apply the Cuts, if an event
  //passes the cut fill the outTree with it
  for (Long64_t eventIndex=0; eventIndex<nEvents; eventIndex++){

    cout <<"\t" <<eventIndex <<"\r";

    //Load the Event
    inBranch->GetEntry(eventIndex);

    //Apply the Pion Multiplicity Cut
    if (event->GetNPions() < minPions)
      continue;

    //Set Z Vertex Cut based
    if (event->GetZVertex() < 0){
      minZ = leftMinZ;
      maxZ = leftMaxZ;
    }
    else if (event->GetZVertex() > 0){
      minZ = rightMinZ;
      maxZ = rightMaxZ;
    }
      
    //Apply the Z Vertex Cut
    if (event->GetZVertex() < minZ || event->GetZVertex() > maxZ)
      continue;

    //Apply Radial Vertex Cut
    Double_t rVertex = sqrt(pow(event->GetXVertex(),2)
    			    +pow(event->GetYVertex(),2));
    if (rVertex < rVertexMin || rVertex > rVertexMax)
      continue;

    //Apply Total Z Momentum Cut
    if (event->GetZVertex() < 0 && event->GetTotalZMomentum() <= 0)
      continue;
    if (event->GetZVertex() > 0 && event->GetTotalZMomentum() >= 0)
      continue;


    //Fill Either the Right or Left Branch
    if (event->GetZVertex() < 0){
      leftTree->Fill();
      nLeftEvents++;
      //cout <<"Filled a Left Event!" <<endl;
    }
    else if (event->GetZVertex() > 0){
      rightTree->Fill();
      nRightEvents++;
      //cout <<"Filled a Right Event!" <<endl;
    }


    //Reset the Event Variable
    event->ResetEvent();

  }

  //Write out and close the file
  leftTree->Write();
  rightTree->Write();
  outFile->Close();

  cout <<"Skimming Complete!" <<endl;
  cout <<"    Number of Events with Vz<0: " <<nLeftEvents <<endl;
  cout <<"    Number of Events with Vz>0: " <<nRightEvents <<endl;
}
