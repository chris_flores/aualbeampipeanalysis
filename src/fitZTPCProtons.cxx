/*****************************************************************
 **
 **
 **
 **
 **
 **
 **
 *****************************************************************/


#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"
#include "../inc/fitZTPCUtilities.h"

//Save?
Bool_t SAVEIMG = true;
Bool_t SAVEOBJ = true;

// --- Functions ---
void LoadZPlots(TFile *inFile, Int_t SIDE);
void FitAntiProtons();
void FitProtons();

//Generate the Spectra
void DefineSpectra(Double_t ENERGY, Int_t PID, Int_t SIDE);
Bool_t MakeSpectra(TH1F *fitHisto, TF1 *fitFunc, Int_t yIndex, 
		   Int_t mTm0Index, Double_t ENERGY,Int_t PID, Int_t SIDE,TString IMAGEDIR="");

//Fitting Functions
Double_t ParticleMeanFunc(Double_t *x, Double_t *par);

// --- Global Variables ---
Int_t rapidityIndex;
Bool_t useDeuteron = true;

//Canvases
TCanvas *fittingCanvas;
TCanvas *fitParCanvas;
TCanvas *spectraCanvas;

//Histograms for the TPC Z Variable
TH1F *tpcAntiProtonHisto[nRapidityBins][nmTm0Bins];
TH1F *tpcProtonHisto[nRapidityBins][nmTm0Bins];

//AntiProton Fit
TF1 *tpcAntiProtonFit[nRapidityBins][nmTm0Bins];

//Proton Fit
TF1 *tpcProtonFit[nRapidityBins][nmTm0Bins];

//Pion Parameterizations
TGraphErrors *pionMeanGraph[nRapidityBins];
Int_t pionMeanPoint[nRapidityBins];
TF1 *pionMeanFit[nRapidityBins];
TGraphErrors *pionWidthGraph[nRapidityBins];
Int_t pionWidthPoint[nRapidityBins];
TF1 *pionWidthFit[nRapidityBins];

//Electron Parameterizations
TGraphErrors *electronMeanGraph[nRapidityBins];
Int_t electronMeanPoint[nRapidityBins];
TF1 *electronMeanFit[nRapidityBins];
TGraphErrors *electronWidthGraph[nRapidityBins];
Int_t electronWidthPoint[nRapidityBins];
TF1 *electronWidthFit[nRapidityBins];

//Particle Parameterizations
ParticlePars *pion, *proton, *deuteron, *triton;


//The Histo with the number of events on each side
TH1F *nEventsHisto;

//Spectra Graphs
TGraphErrors *rawSpectra[nRapidityBins];
Int_t spectraPoint[nRapidityBins];


// MAIN____________________________________________________________
void fitZTPCProtons(TString ZTPCFILE, TString SPECTRAFILE,
		    Double_t ENERGY, Int_t SIDE, Int_t RAPIDITYINDEX=-1, TString IMAGEDIR=""){

  //If the IMAGEDIR argument is empty then turn off saving images
  if (!IMAGEDIR.CompareTo("") && SAVEIMG == true)
    SAVEIMG = false;

  //If we are going to save images then create the subdirectories
  if (SAVEIMG && !gSystem->OpenDirectory(IMAGEDIR+"/ProtonPlus/"+GetSideName(SIDE))){
    gSystem->mkdir(IMAGEDIR+"/ProtonPlus");
    gSystem->mkdir(IMAGEDIR+"/ProtonPlus/"+GetSideName(SIDE));
  }
  
  //Pass the rapidity index value to the global
  rapidityIndex = RAPIDITYINDEX;

  //Load the File with the Yield Plots
  TFile *inFile = new TFile(ZTPCFILE,"READ");
  LoadZPlots(inFile,SIDE);

  //Load the PID Functions
  TFile *pidFile = new TFile("../src/PIDFunctions.root","READ");
  LoadPIDFunctions(pidFile);

  //Create the Output File
  TFile *outFile = new TFile(SPECTRAFILE,"UPDATE");

  //Create the Output File Directory Structure
  if (SAVEOBJ){
    outFile->mkdir(GetSideName(SIDE)+"/zTPC_Fit_Histos/"+GetParticleName(4)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/zTPC_Fit_Curves/"+GetParticleName(4)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/RawSpectra/"+GetParticleName(4)+"/");
  }

  //Set Style Options
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(112);

  //Create the Global Canvases
  fittingCanvas = new TCanvas("fittingCanvas","Fitting Canvas",20,20,800,600);
  fittingCanvas->SetLogy();
  fitParCanvas = new TCanvas("fitParCanvas","Fit Parameter Canvas",880,20,1400,1000);
  fitParCanvas->Divide(3,3);
  spectraCanvas = new TCanvas("spectraCanvas","Spectra Canvas",20,850,800,600);
  spectraCanvas->SetLogy();

  //Create the Particle Parameterization Structs
  pion = new ParticlePars();
  //electron  = new ParticlePars();
  proton = new ParticlePars();
  deuteron = new ParticlePars();
  triton = new ParticlePars();

  DefineParticlePars(pion,GetParticleName(0));
  DefineParticlePars(proton,GetParticleName(4));
  DefineParticlePars(deuteron,GetParticleName(8));
  DefineParticlePars(triton,"TritonPlus");
  
  //Fit the AntiProtons to find the Mean of the Pions  
  FitAntiProtons();

  //Fit the Protons
  FitProtons();

  //Make the Spectra
  DefineSpectra(ENERGY,4,SIDE);
  for (Int_t yIndex=5; yIndex<nRapidityBins; yIndex++){
    
    //If the user has specified a particular rapidity index and
    //if this yIndex is not it, then skip it.
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;

    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //If the fit for this bin does not exist then skip it
      if(tpcProtonFit[yIndex][mTm0Index] == NULL)
	continue;

      //Stop the Spectrum at 800 MeV
      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);
      if (mTm0 > .7)
	continue;

      //Add a Spectra Point
      Bool_t success = MakeSpectra(tpcProtonHisto[yIndex][mTm0Index],
				   tpcProtonFit[yIndex][mTm0Index],
				   yIndex,mTm0Index,ENERGY,4,SIDE,IMAGEDIR);

      if (success && SAVEOBJ){
	
	//Save the zTPC Fit Histo
	outFile->cd(GetSideName(SIDE)+"/zTPC_Fit_Histos/"+GetParticleName(4)+"/");
	tpcProtonHisto[yIndex][mTm0Index]->Write();
	
	//Save the zTPC Fit Curve
	outFile->cd(GetSideName(SIDE)+"/zTPC_Fit_Curves/"+GetParticleName(4)+"/");
	tpcProtonFit[yIndex][mTm0Index]->Write();
      }

      //gSystem->Sleep(1000);
      
    }//End Loop over mTm0Index
    
    //Save the Raw Spectra
    if (rawSpectra[yIndex]->GetN() > 3 && SAVEOBJ){
      outFile->cd(GetSideName(SIDE)+"/RawSpectra/"+GetParticleName(4)+"/");
      rawSpectra[yIndex]->Write();
    }

    //gSystem->Sleep(3000);
  }

  //Save the nEvents Histo
  outFile->cd();
  nEventsHisto->Write();

  outFile->Close();
  inFile->Close();
  pidFile->Close();

}

//________________________________________________________________
void FitAntiProtons(){

  //In this function we loop over the rapidity and mTm0 bins
  //of the antiprotons to parameterize the mean and width of the
  //pions.  This works because at the low energies of the fixed
  //target experiement we don't make any antiprotons nor kaon minus.

  for(Int_t yIndex=5; yIndex<nRapidityBins-4; yIndex++){

    //If the user has specified a particular rapidity index and
    //if this yIndex is not it, then skip it.
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;

    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins-8; mTm0Index++){
      
      //Check to Make Sure the Histogram Exists
      if (!tpcAntiProtonHisto[yIndex][mTm0Index])
	continue;

      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcAntiProtonHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcAntiProtonHisto[yIndex][mTm0Index]->SetAxisRange(-3,3);
      tpcAntiProtonHisto[yIndex][mTm0Index]->Draw("E");

      //Skip this bin if there are insufficient entries
      if (tpcAntiProtonHisto[yIndex][mTm0Index]->GetEntries() < 10)
	continue;

      //Define the Fitting Function
      tpcAntiProtonFit[yIndex][mTm0Index] = 
	new TF1(Form("antiProtonFit_%02d_%02d",yIndex,mTm0Index),
		"gaus(0)",-2.5,3);

      //The Pion Parameters
      Double_t zPionGuess = ComputeTPCZ(4,0,yIndex,mTm0Index);
      tpcAntiProtonFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcAntiProtonFit[yIndex][mTm0Index]->SetParameter(1,zPionGuess);
      tpcAntiProtonFit[yIndex][mTm0Index]->SetParameter(2,.08);

      //Use the Parameters from the previous fit if possible
      if (tpcAntiProtonFit[yIndex][mTm0Index-1]){
	tpcAntiProtonFit[yIndex][mTm0Index]->SetParameter(0,tpcAntiProtonFit[yIndex][mTm0Index-1]->
							  GetParameter(0));	
      }

      //The Amplitudes Should Never be negative
      tpcAntiProtonFit[yIndex][mTm0Index]->SetParLimits(0,0,1050);

      //The Widths Should Be Reasonable
      tpcAntiProtonFit[yIndex][mTm0Index]->SetParLimits(2,.05,.15);

      tpcAntiProtonHisto[yIndex][mTm0Index]->
	Fit(tpcAntiProtonFit[yIndex][mTm0Index],"RQ");

      fittingCanvas->Update();

      //Make Sure its a decent fit
      if (tpcAntiProtonFit[yIndex][mTm0Index]->GetChisquare() / 
	  tpcAntiProtonFit[yIndex][mTm0Index]->GetNDF() > 5.0)
	continue;
 
      //Add Pion Amplitude to the Graph
      pion->amp[yIndex]->SetPoint(pion->ampPoint[yIndex],
				  mTm0,
				  tpcAntiProtonFit[yIndex][mTm0Index]->GetParameter(0));
      pion->amp[yIndex]->SetPointError(pion->ampPoint[yIndex],
				       0,
				       tpcAntiProtonFit[yIndex][mTm0Index]->GetParError(0));   
      pion->ampPoint[yIndex]++;

      //Add the Pion Means to the Graph
      pion->mean[yIndex]->SetPoint(pion->meanPoint[yIndex],
                                   GetmTm0RangeCenter(mTm0Index),
                                   tpcAntiProtonFit[yIndex][mTm0Index]->GetParameter(1));
      pion->mean[yIndex]->SetPointError(pion->meanPoint[yIndex],
					0,
					tpcAntiProtonFit[yIndex][mTm0Index]->GetParError(1));
      pion->meanPoint[yIndex]++;


      //Add the Pion Width to the Graph
      pion->width[yIndex]->SetPoint(pion->widthPoint[yIndex],
				    GetmTm0RangeCenter(mTm0Index),
				    tpcAntiProtonFit[yIndex][mTm0Index]->GetParameter(2));
      pion->width[yIndex]->SetPointError(pion->widthPoint[yIndex],
					 0,
					 tpcAntiProtonFit[yIndex][mTm0Index]->GetParError(2));
      pion->widthPoint[yIndex]++;


      //Draw the Pion Mean and Width
      fitParCanvas->cd(1);
      pion->amp[yIndex]->Draw("APZ");
      fitParCanvas->cd(2);
      pion->mean[yIndex]->Draw("APZ");
      fitParCanvas->cd(3);
      pion->width[yIndex]->Draw("APZ");

      fitParCanvas->Update();

      //gSystem->Sleep(1000);

    }//End Loop Over mTm0Index

    //Find the maximum value of the pion amplitude and fit the graph above that value
    Int_t maxIndex = TMath::LocMax(pion->amp[yIndex]->GetN(),pion->amp[yIndex]->GetY());
    Double_t maxmTm0 = pion->amp[yIndex]->GetX()[maxIndex];
    pion->ampFit[yIndex] = new TF1(Form("pionAmplitudeFit_%02d",yIndex),"expo",maxmTm0,1);
    
    //Create the Pion Mean Fit Function
    pion->meanFit[yIndex] = new TF1(Form("pionMeanFit_%02d",yIndex),ConfoundMeanFit,.05,1,5);
    pion->meanFit[yIndex]->FixParameter(0,4);
    pion->meanFit[yIndex]->FixParameter(1,1);
    pion->meanFit[yIndex]->FixParameter(2,yIndex);
    pion->meanFit[yIndex]->SetParameter(3,.1);
    pion->meanFit[yIndex]->SetParameter(4,1);
	
    //Create the Pion Width Fit Function
    pion->widthFit[yIndex] = new TF1(Form("pionWidthFit_%02d",yIndex),SwooshFunc,.05,1,4);    
    pion->widthFit[yIndex]->SetParameters(.2,.5,.3,.1);
    pion->widthFit[yIndex]->SetParLimits(3,0,1);

    //Fit The Pion Parameters
    fitParCanvas->cd(1);
    pion->amp[yIndex]->Fit(pion->ampFit[yIndex],"RQ");
    fitParCanvas->cd(2);
    pion->mean[yIndex]->Fit(pion->meanFit[yIndex],"RQ");
    fitParCanvas->cd(3);
    pion->width[yIndex]->Fit(pion->widthFit[yIndex],"RQ");
    
    fitParCanvas->Update();
    
  }//End Loop Over yIndex

}

//_______________________________________________________________________
void FitProtons(){

  for (Int_t yIndex=5; yIndex<nRapidityBins-4; yIndex++){

    //If the user has specified a particular rapidity index and 
    //if this yIndex is not it, then skip it.
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;

    //*********************************************************************
    //Round 1 - The Pion Mean, Width, and Amplitude are fixed using
    //  the parameterizations from FitAntiProtons.  Here Amplitudes 
    //  are parameterized.  In addition the means and widths of all 
    //  particles are parameterized
    //*********************************************************************
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //Make Sure the Histogram Exists
      if (!tpcProtonHisto[yIndex][mTm0Index])
	continue;

      //Make Sure the Anti Proton Histogram Exists, because if it doesn't 
      //how do we know the pion parameters
      if (!tpcAntiProtonHisto[yIndex][mTm0Index])
	continue;

      //Compute the mT-m0 of this bin
      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Check to make sure there is a value of the Pion Amplitude for this bin
      if (mTm0 < pion->amp[yIndex]->GetX()[0] || 
	  mTm0 > pion->amp[yIndex]->GetX()[pion->amp[yIndex]->GetN()-1]){
	tpcProtonFit[yIndex][mTm0Index] = NULL;
	continue;
      }

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcProtonHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcProtonHisto[yIndex][mTm0Index]->SetAxisRange(-3,3);
      tpcProtonHisto[yIndex][mTm0Index]->Draw("E");

      //Mean Starting Values
      Double_t protonZGuess = ComputeTPCZ(4,4,yIndex,mTm0Index);
      Double_t deuteronZGuess  = ComputeTPCZ(4,8,yIndex,mTm0Index);

      //Define the Fitting Function
      tpcProtonFit[yIndex][mTm0Index] = 
	new TF1(Form("zProtonFit_%02d_%02d",yIndex,mTm0Index),
		"gaus(0)+gaus(3)+gaus(6)",-2,
		TMath::Max(deuteronZGuess,pion->meanFit[yIndex]->Eval(mTm0))+.2);
      
      //The Proton Parameters
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(1,protonZGuess);
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(2,.08);

      //The Pion Parameters
      Double_t minRange(0), maxRange(0);
      pion->ampFit[yIndex]->GetRange(minRange,maxRange);
      if (mTm0 > minRange && mTm0 < maxRange)
	tpcProtonFit[yIndex][mTm0Index]->FixParameter(3,pion->ampFit[yIndex]->Eval(mTm0));
      else
	tpcProtonFit[yIndex][mTm0Index]->FixParameter(3,pion->amp[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(4,pion->meanFit[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(5,pion->widthFit[yIndex]->Eval(mTm0));

      //The Deuteron Parameters
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(6,1000);
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(7,deuteronZGuess);
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(8,.08);

      //The Proton Mean Should not Deviate Too Much from 0
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(1,-.05,.05);
      
      //The Deuteron SHould Deviate too much from its guess
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(7,deuteronZGuess*.93,deuteronZGuess*1.07);

      //Amplitudes Can't be Negative
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(0,0,10000);
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(6,0,10000);

      //Widths Need to Be Reasonable
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(2,.05,.15);
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(8,.05,.15);

      tpcProtonHisto[yIndex][mTm0Index]->Fit(tpcProtonFit[yIndex][mTm0Index],"RQ");

      //Add Point to the Graphs only if the distance between the means of the protons
      //and deuterons are sufficiently far apart
      if (fabs(tpcProtonFit[yIndex][mTm0Index]->GetParameter(1) - 
	       tpcProtonFit[yIndex][mTm0Index]->GetParameter(7)) < 2*0.1)
	continue;
	  

      //Add Points to the Proton Graphs
      proton->mean[yIndex]->SetPoint(proton->meanPoint[yIndex],
				     mTm0,
				     tpcProtonFit[yIndex][mTm0Index]->GetParameter(1));
      proton->mean[yIndex]->SetPointError(proton->meanPoint[yIndex],
					  0,
					  tpcProtonFit[yIndex][mTm0Index]->GetParError(1));
      proton->meanPoint[yIndex]++;

      proton->width[yIndex]->SetPoint(proton->widthPoint[yIndex],
				     mTm0,
				     tpcProtonFit[yIndex][mTm0Index]->GetParameter(2));
      proton->width[yIndex]->SetPointError(proton->widthPoint[yIndex],
					  0,
					  tpcProtonFit[yIndex][mTm0Index]->GetParError(2));
      proton->widthPoint[yIndex]++;

      //Add Points to the Deuteron Graphs
      deuteron->mean[yIndex]->SetPoint(deuteron->meanPoint[yIndex],
				     mTm0,
				     tpcProtonFit[yIndex][mTm0Index]->GetParameter(7));
      deuteron->mean[yIndex]->SetPointError(deuteron->meanPoint[yIndex],
					  0,
					  tpcProtonFit[yIndex][mTm0Index]->GetParError(7));
      deuteron->meanPoint[yIndex]++;

      deuteron->width[yIndex]->SetPoint(deuteron->widthPoint[yIndex],
				     mTm0,
				     tpcProtonFit[yIndex][mTm0Index]->GetParameter(8));
      deuteron->width[yIndex]->SetPointError(deuteron->widthPoint[yIndex],
					  0,
					  tpcProtonFit[yIndex][mTm0Index]->GetParError(8));
      deuteron->widthPoint[yIndex]++;
  

      //Draw the Parameter Graphs
      fitParCanvas->cd(5);
      proton->mean[yIndex]->Draw("APZ");
      fitParCanvas->cd(6);
      proton->width[yIndex]->Draw("APZ");
      fitParCanvas->cd(8);
      deuteron->mean[yIndex]->Draw("APZ");
      fitParCanvas->cd(9);
      deuteron->width[yIndex]->Draw("APZ");
  
      fitParCanvas->Update();
      fittingCanvas->Update();
      //gSystem->Sleep(250);

    }//End Loop Over mTm0Index for Round 1

    //Check the Deuteron Graph for points. If it has more than 5 then we can parameterize it,
    //otherwise set useDeuteron to false
    if (deuteron->width[yIndex]->GetN() < 7){
      useDeuteron = false;
      cout <<"NOTE: Insufficient number of points to parameterize deuterons. Setting useDeuteron to false." <<endl
	   <<"      Proton yield will be computed by taking the bin contents of the histogram in the drawing" <<endl
	   <<"      and subtracting the pion contribution." <<endl;
      break;
    }
    
    //Define the Fits for the Proton Width
    proton->widthFit[yIndex] = new TF1(Form("protonWidthFit_%02d",yIndex),SwooshFunc,.05,1,4);
    proton->widthFit[yIndex]->SetParameters(.2,.5,.3,.1);
    proton->widthFit[yIndex]->SetParLimits(3,0,1.0);

    //Define the Fits for the Deuteron Mean and Width
    deuteron->meanFit[yIndex] = new TF1(Form("deuteronMeanFit_%02d",yIndex),ConfoundMeanFit,.05,1,5);
    deuteron->meanFit[yIndex]->FixParameter(0,4);
    deuteron->meanFit[yIndex]->FixParameter(1,8);
    deuteron->meanFit[yIndex]->FixParameter(2,yIndex);
    deuteron->meanFit[yIndex]->SetParameter(3,.1);
    deuteron->meanFit[yIndex]->SetParameter(4,1);

    if (deuteron->width[yIndex]->GetN() < 7)
      deuteron->widthFit[yIndex] = new TF1(Form("deuteronWidthFit_%02d",yIndex),"pol1",.05,1);
    else{
      deuteron->widthFit[yIndex] = new TF1(Form("deuteronWidthFit_%02d",yIndex),SwooshFunc,.05,1,4);
      deuteron->widthFit[yIndex]->SetParameters(.2,.5,.3,.1);
      deuteron->widthFit[yIndex]->SetParLimits(3,0,1.0);
    }
    
    //Fit the Graphs
    fitParCanvas->cd(5);
    //proton->mean[yIndex]->Fit(proton->meanFit[yIndex],"RQ");
    fitParCanvas->cd(6);
    proton->width[yIndex]->Fit(proton->widthFit[yIndex],"RQ");
    fitParCanvas->cd(8);
    deuteron->mean[yIndex]->Fit(deuteron->meanFit[yIndex],"RQ");
    fitParCanvas->cd(9);
    deuteron->width[yIndex]->Fit(deuteron->widthFit[yIndex],"RQ");

    fitParCanvas->Update();

    //gSystem->Sleep(200);
    
    //*********************************************************************
    //Round 2 - The Deuteron mean and width is fixed and its amplitude is
    //parameterized
    //*********************************************************************
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //Make Sure the Histogram Exists
      if (!tpcProtonHisto[yIndex][mTm0Index])
	continue;

      //Make Sure the Anti Proton Histogram Exists, because if it doesn't
      //how do we know the pion parameters
      if (!tpcAntiProtonHisto[yIndex][mTm0Index])
	continue;

      //Compute the mT-m0 of this bin
      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Check to make sure there is a value of the Pion Amplitude for this bin
      if (mTm0 < pion->amp[yIndex]->GetX()[0] ||
          mTm0 > pion->amp[yIndex]->GetX()[pion->amp[yIndex]->GetN()-1]){
	tpcProtonFit[yIndex][mTm0Index] = NULL;
        continue;
      }

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcProtonHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcProtonHisto[yIndex][mTm0Index]->SetAxisRange(-3,3);
      tpcProtonHisto[yIndex][mTm0Index]->Draw("E");

      //Define the Fitting Function
      tpcProtonFit[yIndex][mTm0Index]->Delete();
      tpcProtonFit[yIndex][mTm0Index] = 
	new TF1(Form("zProtonFit_%02d_%02d",yIndex,mTm0Index),
		"gaus(0)+gaus(3)+gaus(6)",-2,
		TMath::Max(pion->meanFit[yIndex]->Eval(mTm0),deuteron->meanFit[yIndex]->Eval(mTm0))+.2);
      
      //The Proton Parameters
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(1,proton->mean[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(2,proton->widthFit[yIndex]->Eval(mTm0));

      //The Pion Parameters
      Double_t minRange(0), maxRange(0);
      pion->ampFit[yIndex]->GetRange(minRange,maxRange);
      if (mTm0 > minRange && mTm0 < maxRange)
	tpcProtonFit[yIndex][mTm0Index]->FixParameter(3,pion->ampFit[yIndex]->Eval(mTm0));
      else
        tpcProtonFit[yIndex][mTm0Index]->FixParameter(3,pion->amp[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(4,pion->meanFit[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(5,pion->widthFit[yIndex]->Eval(mTm0));

      //The Deuteron Parameters
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(6,1000);
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(7,deuteron->meanFit[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(8,deuteron->widthFit[yIndex]->Eval(mTm0));
      
      //Amplitudes Can't be Negative
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(0,0,10000);
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(6,0,10000);
 
      tpcProtonHisto[yIndex][mTm0Index]->Fit(tpcProtonFit[yIndex][mTm0Index],"RQ");

      //Make Sure its a decent fit                                                            
      if (tpcProtonFit[yIndex][mTm0Index]->GetChisquare() /
          tpcProtonFit[yIndex][mTm0Index]->GetNDF()> 5.0)
	continue;

      //Add the Proton Amp to the Graph
      proton->amp[yIndex]->SetPoint(proton->ampPoint[yIndex],
				    mTm0,
				    tpcProtonFit[yIndex][mTm0Index]->GetParameter(0));
      proton->amp[yIndex]->SetPointError(proton->ampPoint[yIndex],
				    0,
				    tpcProtonFit[yIndex][mTm0Index]->GetParError(0));
      proton->ampPoint[yIndex]++;

      //Add the Deuteron Amp to the Graph
      deuteron->amp[yIndex]->SetPoint(deuteron->ampPoint[yIndex],
				      mTm0,
				      tpcProtonFit[yIndex][mTm0Index]->GetParameter(6));
      deuteron->amp[yIndex]->SetPointError(deuteron->ampPoint[yIndex],
					   0,
					   tpcProtonFit[yIndex][mTm0Index]->GetParError(6));
      deuteron->ampPoint[yIndex]++;

      //Draw the Proton Amp
      fitParCanvas->cd(4);
      proton->amp[yIndex]->Draw("APZ");
      fitParCanvas->Update();

      //Draw the Deuteron Amp
      fitParCanvas->cd(7);
      deuteron->amp[yIndex]->Draw("APZ");
      fitParCanvas->Update();

      fittingCanvas->Update();
      //gSystem->Sleep(500);

    }//End Loop Over mTm0Index For Round 2

    //Find the maximum value of the deuteron amplitude and fit the graph above that value 
    Int_t maxIndex = TMath::LocMax(deuteron->amp[yIndex]->GetN(),deuteron->amp[yIndex]->GetY());
    Double_t maxmTm0 = deuteron->amp[yIndex]->GetX()[maxIndex];
    deuteron->ampFit[yIndex] = new TF1(Form("deuteronAmplitudeFit_%02d",yIndex),"expo",maxmTm0,1);

    fitParCanvas->cd(7);
    deuteron->amp[yIndex]->Fit(deuteron->ampFit[yIndex],"RQ");
    fitParCanvas->Update();

    //gSystem->Sleep(1000);


    //*********************************************************************
    //Round 3 - The only parameter allowed to float is the proton amplitude.
    //all others are fixed by the parameterizations above
    //*********************************************************************
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //Make Sure the Histogram Exists
      if (!tpcProtonHisto[yIndex][mTm0Index])
	continue;

      //Make Sure the Anti Proton Histogram Exists, because if it doesn't
      //how do we know the pion parameters
      if (!tpcAntiProtonHisto[yIndex][mTm0Index])
	continue;


      //Compute the mT-m0 of this bin
      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcProtonHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcProtonHisto[yIndex][mTm0Index]->SetAxisRange(-3,3);
      tpcProtonHisto[yIndex][mTm0Index]->Draw("E");

      //Define the Yield Histogram Fit
      if (tpcProtonFit[yIndex][mTm0Index] != NULL)
	tpcProtonFit[yIndex][mTm0Index]->Delete();
      tpcProtonFit[yIndex][mTm0Index] = 
	new TF1(Form("zProtonFit_%02d_%02d",yIndex,mTm0Index),
		"gaus(0)+gaus(3)+gaus(6)",-1,deuteron->meanFit[yIndex]->Eval(mTm0)+
		2*deuteron->widthFit[yIndex]->Eval(mTm0));

      //Set the Parameter names
      tpcProtonFit[yIndex][mTm0Index]->SetParName(0,"p_{Amp.}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(1,"#mu_{p}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(2,"#sigma_{p}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(3,"#pi_{Amp.}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(4,"#mu_{#pi}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(5,"#sigma_{#pi}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(6,"d_{Amp.}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(7,"#mu_{d}");
      tpcProtonFit[yIndex][mTm0Index]->SetParName(8,"#sigma_{d}");

      //The Proton Parameters
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcProtonFit[yIndex][mTm0Index]->SetParameter(1,proton->mean[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(2,proton->widthFit[yIndex]->Eval(mTm0));

      //The Pion Parameters
      Double_t minRange(0), maxRange(0);
      pion->ampFit[yIndex]->GetRange(minRange,maxRange);
      if (mTm0 > minRange && mTm0 < maxRange)
	tpcProtonFit[yIndex][mTm0Index]->FixParameter(3,pion->ampFit[yIndex]->Eval(mTm0));
      else
        tpcProtonFit[yIndex][mTm0Index]->FixParameter(3,pion->amp[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(4,pion->meanFit[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(5,pion->widthFit[yIndex]->Eval(mTm0));

      //The Deuteron Parameters
      deuteron->ampFit[yIndex]->GetRange(minRange,maxRange);
      if (mTm0 > minRange && mTm0 < maxRange)
	tpcProtonFit[yIndex][mTm0Index]->FixParameter(6,deuteron->ampFit[yIndex]->Eval(mTm0));
      else {
	tpcProtonFit[yIndex][mTm0Index]->SetParameter(6,deuteron->amp[yIndex]->Eval(mTm0));
      }
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(7,deuteron->meanFit[yIndex]->Eval(mTm0));
      tpcProtonFit[yIndex][mTm0Index]->FixParameter(8,deuteron->widthFit[yIndex]->Eval(mTm0));
      
      //Amplitudes Can't be Negative
      tpcProtonFit[yIndex][mTm0Index]->SetParLimits(0,0,10000);

      //Fit the Yield Histogram
      tpcProtonHisto[yIndex][mTm0Index]->Fit(tpcProtonFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();
    }//End Loop Over mTm0Index For Round 3


  }//Endl Loop Over yIndex
  
  cout <<"Finished Parameterization of Pion, Protons and Deuterons" <<endl;
  
}

//________________________________________________________________________
void DefineSpectra(Double_t ENERGY, Int_t PID, Int_t SIDE){

  //Loop Over the Rapidity Bins and Define the Spectra
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

    rawSpectra[yIndex] = new TGraphErrors();
    rawSpectra[yIndex]->SetMarkerStyle(20);
    rawSpectra[yIndex]->SetName("rawSpectra_"+GetSideName(SIDE)+"_"+GetParticleName(PID)+
				+TString::Format("_%02d",yIndex));
    rawSpectra[yIndex]->SetTitle(GetParticleSymbol(PID)+" Raw Spectra "+GetSideName(SIDE)+
				 TString::Format(" Au_{Like}+Al at %.2f (%.2f <= y < %.2f)",
						 ENERGY,GetRapidityRangeLow(yIndex,SIDE),
						 GetRapidityRangeHigh(yIndex,SIDE))+
				 TString::Format(";m_{T}-m_{%s};",GetParticleSymbol(PID).Data()));
    
    spectraPoint[yIndex] = 0;

  }
}

//________________________________________________________________
void LoadZPlots(TFile *inFile, Int_t SIDE){

  //This Function Loads the tpcZ Proton Plots

  //Loop Over the Rapidity Bins and mTm0 Bins and Load the Histograms
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
    for (Int_t mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
      
      //The Proton
      inFile->cd(GetSideName(SIDE)+"/"+GetParticleName(4)+"/zTPC/");

      tpcProtonHisto[yIndex][mTm0Index] = (TH1F*)gDirectory->
	Get(GetParticleName(4)+"_z_TPC_"+GetSideName(SIDE)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index));

      //The AntiProton
      inFile->cd(GetSideName(SIDE)+"/"+GetParticleName(5)+"/zTPC/");
      
      tpcAntiProtonHisto[yIndex][mTm0Index] = (TH1F*)gDirectory->
	Get(GetParticleName(5)+"_z_TPC_"+GetSideName(SIDE)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index));
      
    }
  }

  inFile->cd();
  nEventsHisto = (TH1F*)inFile->Get("nEventsSide");

}



//____________________________________________________________________________
Double_t ParticleMeanFunc(Double_t *x, Double_t *par){

  Double_t mTm0 = x[0];

  Int_t fitParticleID = (Int_t)par[0];
  Int_t refParticleID = (Int_t)par[1];
  Double_t y = par[2];

  Double_t pt = ConvertmTm0ToPt(mTm0,fitParticleID);
  Double_t eta = ComputeEta(y,pt,GetParticleMass(fitParticleID));
  Double_t totalP = pt * cosh(ComputeEta(y,pt,eta));
  
  Double_t fitParticledEdx = tpcPIDFunc[fitParticleID]->Eval(totalP);
  Double_t refParticledEdx = tpcPIDFunc[refParticleID]->Eval(totalP);
  
  //cout <<mTm0 <<" " <<pt <<" " <<eta <<" " <<totalP <<" " <<fitParticledEdx <<" " <<refParticledEdx 
  //   <<" " <<log(fitParticledEdx/refParticledEdx) <<endl;

  return par[3] + par[4]*log(fitParticledEdx/refParticledEdx);

}



//___________________________________________________________________________
Bool_t MakeSpectra(TH1F *fitHisto, TF1 *fitFunc, Int_t yIndex, Int_t mTm0Index,
		   Double_t ENERGY, Int_t PID, Int_t SIDE, TString IMAGEDIR){

  //Adds a point to the spectra 
  //Return false on fail, and true on success

  //Make Sure the Histogram and Fit Exist
  if (!fitHisto || !fitFunc)
    return false;

  //Set the Attributes of the Full Fitting Function
 
  Double_t axisRangeLow(-1.8), axisRangeHigh(3);
  //Draw the Final Version of the Fit
  fittingCanvas->Clear();
  fittingCanvas->cd();
  fittingCanvas->SetTopMargin(.05);
  fittingCanvas->SetRightMargin(.05);
  fittingCanvas->SetLeftMargin(.05);
  fitHisto->SetMarkerStyle(kOpenCircle);
  fitHisto->SetTitle("");
  fitHisto->SetAxisRange(axisRangeLow,axisRangeHigh);
  fitHisto->Draw("E");

  //Draw the Fit
  //fitFunc->SetLineStyle(7);
  fitFunc->SetLineWidth(3);
  fitFunc->SetLineColor(kBlack);
  fitFunc->SetNpx(10000);
  fitFunc->Draw("SAME");

  //Create a Label
  TPaveText label(.07,.8,.30,.92,"BL NDC");
  label.SetTextAlign(11);
  label.AddText(Form("Z(%s) Yield Extraction Fit",GetParticleSymbol(PID).Data()));
  label.AddText(Form("  Au_{Like}+Al #sqrt{s_{NN}} = %.2f GeV %s",
		     ENERGY,GetSideName(SIDE).Data()));
  label.AddText(Form("  %.2f #leq y_{%s} < %.2f | %.2f #leq m_{T}-m_{%s} < %.2f",
		     GetRapidityRangeLow(yIndex,SIDE),GetParticleSymbol(PID).Data(),
		     GetRapidityRangeHigh(yIndex,SIDE),GetmTm0RangeLow(mTm0Index),
		     GetParticleSymbol(PID).Data(),GetmTm0RangeHigh(mTm0Index)));
  label.GetLine(0)->SetTextSize(.03);
  label.GetLine(1)->SetTextSize(.02);
  label.GetLine(2)->SetTextSize(.02);
  label.SetFillColor(kWhite);
  label.SetBorderSize(0);
  label.Draw("SAME");

  //Make A Function for Each of the Particles
  TF1 protonG("protonG","gaus(0)",-20,20);
  TF1 pionG("pionG","gaus(0)",-20,20);
  TF1 deuteronG("deuteronG","gaus(0)",-20,20);

  //Set their parameters from the fit function
  protonG.FixParameter(0,fitFunc->GetParameter(0));
  protonG.FixParameter(1,fitFunc->GetParameter(1));
  protonG.FixParameter(2,fitFunc->GetParameter(2));
  protonG.SetParError(0,fitFunc->GetParError(0));
  protonG.SetParError(1,fitFunc->GetParError(1));
  protonG.SetParError(2,fitFunc->GetParError(2));
  protonG.SetLineColor(GetParticleColor(PID));
  protonG.SetLineWidth(3);
  protonG.SetLineStyle(7);
  protonG.SetNpx(10000);

  pionG.FixParameter(0,fitFunc->GetParameter(3));
  pionG.FixParameter(1,fitFunc->GetParameter(4));
  pionG.FixParameter(2,fitFunc->GetParameter(5));
  pionG.SetParError(0,fitFunc->GetParError(3));
  pionG.SetParError(1,fitFunc->GetParError(4));
  pionG.SetParError(2,fitFunc->GetParError(5));
  pionG.SetLineColor(GetParticleColor(0));
  pionG.SetLineWidth(3);
  pionG.SetLineStyle(7);
  pionG.SetNpx(10000);

  deuteronG.FixParameter(0,fitFunc->GetParameter(6));
  deuteronG.FixParameter(1,fitFunc->GetParameter(7));
  deuteronG.FixParameter(2,fitFunc->GetParameter(8));
  deuteronG.SetParError(0,fitFunc->GetParError(6));
  deuteronG.SetParError(1,fitFunc->GetParError(7));
  deuteronG.SetParError(2,fitFunc->GetParError(8));
  deuteronG.SetLineColor(GetParticleColor(8));
  deuteronG.SetLineWidth(3);
  deuteronG.SetLineStyle(7);
  deuteronG.SetNpx(10000);

  //What we draw depends on whether we are using the deuteron or not
  if (useDeuteron){
    protonG.Draw("SAME");
    pionG.Draw("SAME");
    deuteronG.Draw("SAME");
  }
  else {
    TF1 *first = (TF1 *)fitHisto->GetListOfFunctions()->First();
    if (first)
      first->Delete();
    TF1 *last  = (TF1 *)fitHisto->GetListOfFunctions()->Last();
    if (last)
      last->Delete();

    if (fitFunc)
      fitFunc->Delete();

    pionG.Draw("SAME");    

  }

  //Get the Stats Box and Set its attributes
  TPaveStats *statsBox = (TPaveStats*)fitHisto->FindObject("stats");
  if (statsBox){
    statsBox->SetX1NDC(.7);
    statsBox->SetY1NDC(.15);
    statsBox->SetX2NDC(.97);
    statsBox->SetY2NDC(.93);
  }

  gPad->Update();

  //Make A Legend
  TLegend leg;
  leg.SetOption("brNDC");

  leg.SetX1NDC(.42);
  leg.SetY1NDC(.88);
  leg.SetX2NDC(.69);
  leg.SetY2NDC(.93);

  leg.SetNColumns(5);
  if (useDeuteron){
    leg.AddEntry(fitFunc,"Fit","L");
    leg.AddEntry("pionG","#pi","L");
    leg.AddEntry("protonG","p","L");
    leg.AddEntry("deuteronG","d","L");
  }
  else {
    leg.AddEntry("pionG","#pi","L");
  }

  leg.SetFillColor(kWhite);
  leg.SetBorderSize(0);
  leg.Draw("SAME");
  
  fittingCanvas->Update();

  //Pi
  Double_t pi = TMath::Pi();

  //Normalization Factors
  Double_t zBinWidth = (Double_t)fitHisto->GetBinWidth(2);
  Double_t mTm0BinCenter = GetmTm0RangeCenter(mTm0Index);
  Double_t nEvents = (Double_t)nEventsHisto->GetBinContent(nEventsHisto->FindBin((Double_t)SIDE));

  //Set the Normalization Factor 
  Double_t normFactor = (1.0/(mTm0BinCenter+GetParticleMass(PID))) * (1.0/mTm0Width) * (1.0/rapidityWidth) *
    (1.0/nEvents) * (1.0/(2.0*pi));

  //Extract the Yield depending on whether we use deuterons or not
  //If useDeuteron is TRUE (Default): then the yield is extracted from the proton fit parameters
  //If useDeuteron is FALSE: then find the pion yield in this bin and subtract it from the entries in
  //   the histogram. Use this option if protons and deuterons are indistinguishable and this is not
  //   the driving systematic of the proton spectrum

  //Extract the Yield
  Double_t amp = protonG.GetParameter(0);
  Double_t ampError = protonG.GetParError(0);
  Double_t width = protonG.GetParameter(2);
  Double_t widthError = protonG.GetParError(2);

  //Compute the Raw Yield
  Double_t rawYield, rawYieldError;

  if (useDeuteron){
    rawYield = ( (amp * width * sqrt(2.0*pi)) / zBinWidth ) * normFactor;
    rawYieldError = ( (amp * width * sqrt(pow(ampError/amp,2)+pow(widthError/width,2)) * sqrt(2.0*pi)) / zBinWidth) * normFactor;
  }
  
  else{
    Double_t pionAmp(0),pionWidth(0);
    Double_t pionAmpErr(0), pionWidthErr(0);
    pionAmp = pionG.GetParameter(0);
    pionWidth = pionG.GetParameter(2);
    pionAmpErr = pionG.GetParError(0);
    pionWidthErr = pionG.GetParError(2);
    Double_t pionYield = ( (pionAmp * pionWidth * sqrt(2.0*pi)) / zBinWidth);
    Double_t pionYieldErr = ( (pionAmp * pionWidth * sqrt(pow(pionAmpErr/pionAmp,2)+pow(pionWidthErr/pionWidth,2))
			       * sqrt(2.0*pi)) / zBinWidth);
    rawYield = (fitHisto->Integral(fitHisto->FindBin(axisRangeLow),
				   fitHisto->FindBin(axisRangeHigh)) - pionYield) * normFactor;
    rawYieldError = rawYield * sqrt(pow(sqrt((Double_t)fitHisto->GetEntries()),2) + pow(pionYieldErr,2)) * normFactor;
  }

  //Make sure the raw yield is positive
  if (rawYield <=0 )
    return false;

  //Add the Point to the Spectra
  rawSpectra[yIndex]->SetPoint(spectraPoint[yIndex],
			       GetmTm0RangeCenter(mTm0Index),
			       rawYield);
  rawSpectra[yIndex]->SetPointError(spectraPoint[yIndex],
				    mTm0Width/2.0,
				    rawYieldError);
  spectraPoint[yIndex]++;
  
  spectraCanvas->cd();
  rawSpectra[yIndex]->Draw("APZ");
  spectraCanvas->Update();

  //Save the Fit Image
  if (SAVEIMG){

    TString fullImageDir = IMAGEDIR+"/"+GetParticleName(PID);
    fullImageDir += "/"+GetSideName(SIDE);

    SaveImage(fittingCanvas,
	      fullImageDir,
  	      Form("zTPCFit_%s_%s_%02d_%02d",
  		   GetParticleName(PID).Data(),
  		   GetSideName(SIDE).Data(),
  		   yIndex,mTm0Index),
  	      "eps");
  }

  //If we are not using the deuteron we need to return false since 
  //there wasn't a final fit to be saved to the root file
  if (!useDeuteron)
    return false;
  
  return true;

}

