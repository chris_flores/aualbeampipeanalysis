#include "TrackClass.h"

//Contains the Member Functions for the TRACK Class

ClassImp(TRACK);

//_____________________________________________________________________
TRACK::TRACK(){
  
  //Constructor
  //Sets all Values to -999
  nHitsFit       = -999;
  nHitsPoss      = -999;
  trackFlag      = -999;
  pZ             = -999;
  pT             = -999;
  xVertex        = -999;
  yVertex        = -999;
  zVertex        = -999;
  dEdx           = -999;
  charge         = -999;
  tof            = -999;
  tofBeta        = -999;
  eta            = -999;
  phi            = -999;
  nSigPion       = -999;
  nSigKaon       = -999;
  nSigProton     = -999;
  yPion          = -999;
  yKaon          = -999;
  yProton        = -999;
  yDeuteron      = -999;
  yTriton        = -999;
  yHelium        = -999;
  dcaD           = -999;
  dcaZ           = -999;
  nHits          = -999;
  dEdxHits       = -999;
  firstZPoint    = -999;
  lastZPoint     = -999;
  tofSigElectron = -999;
  tofSigPion     = -999;
  tofSigKaon     = -999;
  tofSigProton   = -999;
  pathLength     = -999;
  globalDCAx     = -999;
  globalDCAy     = -999;
  globalDCAz     = -999;
  primaryDCAx    = -999;
  primaryDCAy    = -999;
  primaryDCAz    = -999; 
  
}

//_____________________________________________________________________
TRACK::~TRACK(){

  //TRACK Destructor

}

//_____________________________________________________________________
TRACK::TRACK(Float_t *trackarray){

  //Sets All the Track Attributes - ORDER MATTERS!
  nHitsFit       = (Int_t)trackarray[0];
  nHitsPoss      = (Int_t)trackarray[1];
  trackFlag      = (Int_t)trackarray[2];
  pZ             = (Double_t)trackarray[3];
  pT             = (Double_t)trackarray[4];
  xVertex        = (Double_t)trackarray[5];
  yVertex        = (Double_t)trackarray[6];
  zVertex        = (Double_t)trackarray[7];
  dEdx           = (Double_t)trackarray[8];
  charge         = (Double_t)trackarray[9];
  tof            = (Double_t)trackarray[10];
  tofBeta        = (Double_t)trackarray[11];
  eta            = (Double_t)trackarray[12];
  phi            = (Double_t)trackarray[13];
  nSigPion       = (Double_t)trackarray[14];
  nSigKaon       = (Double_t)trackarray[15];
  nSigProton     = (Double_t)trackarray[16];
  yPion          = (Double_t)trackarray[17];
  yKaon          = (Double_t)trackarray[18];
  yProton        = (Double_t)trackarray[19];
  yDeuteron      = (Double_t)trackarray[20];
  yTriton        = (Double_t)trackarray[21];
  yHelium        = (Double_t)trackarray[22];
  dcaD           = (Double_t)trackarray[23];
  dcaZ           = (Double_t)trackarray[24];
  nHits          = (Double_t)trackarray[25];
  dEdxHits       = (Double_t)trackarray[26];
  firstZPoint    = (Double_t)trackarray[27];
  lastZPoint     = (Double_t)trackarray[28];
  tofSigElectron = (Double_t)trackarray[29];
  tofSigPion     = (Double_t)trackarray[30];
  tofSigKaon     = (Double_t)trackarray[31];
  tofSigProton   = (Double_t)trackarray[32];
  pathLength     = (Double_t)trackarray[33];
  globalDCAx     = (Double_t)trackarray[34];
  globalDCAy     = (Double_t)trackarray[35];
  globalDCAz     = (Double_t)trackarray[36];
  primaryDCAx    = (Double_t)trackarray[37];
  primaryDCAy    = (Double_t)trackarray[38];
  primaryDCAz    = (Double_t)trackarray[39];

}

//_____________________________________________________________________
void TRACK::SetTrackAttributes(Float_t *trackarray){

  //Sets All the Track Attributes - ORDER MATTERS!
  nHitsFit       = (Int_t)trackarray[0];
  nHitsPoss      = (Int_t)trackarray[1];
  trackFlag      = (Int_t)trackarray[2];
  pZ             = (Double_t)trackarray[3];
  pT             = (Double_t)trackarray[4];
  xVertex        = (Double_t)trackarray[5];
  yVertex        = (Double_t)trackarray[6];
  zVertex        = (Double_t)trackarray[7];
  dEdx           = (Double_t)trackarray[8];
  charge         = (Double_t)trackarray[9];
  tof            = (Double_t)trackarray[10];
  tofBeta        = (Double_t)trackarray[11];
  eta            = (Double_t)trackarray[12];
  phi            = (Double_t)trackarray[13];
  nSigPion       = (Double_t)trackarray[14];
  nSigKaon       = (Double_t)trackarray[15];
  nSigProton     = (Double_t)trackarray[16];
  yPion          = (Double_t)trackarray[17];
  yKaon          = (Double_t)trackarray[18];
  yProton        = (Double_t)trackarray[19];
  yDeuteron      = (Double_t)trackarray[20];
  yTriton        = (Double_t)trackarray[21];
  yHelium        = (Double_t)trackarray[22];
  dcaD           = (Double_t)trackarray[23];
  dcaZ           = (Double_t)trackarray[24];
  nHits          = (Double_t)trackarray[25];
  dEdxHits       = (Double_t)trackarray[26];
  firstZPoint    = (Double_t)trackarray[27];
  lastZPoint     = (Double_t)trackarray[28];
  tofSigElectron = (Double_t)trackarray[29];
  tofSigPion     = (Double_t)trackarray[30];
  tofSigKaon     = (Double_t)trackarray[31];
  tofSigProton   = (Double_t)trackarray[32];
  pathLength     = (Double_t)trackarray[33];
  globalDCAx     = (Double_t)trackarray[34];
  globalDCAy     = (Double_t)trackarray[35];
  globalDCAz     = (Double_t)trackarray[36];
  primaryDCAx    = (Double_t)trackarray[37];
  primaryDCAy    = (Double_t)trackarray[38];
  primaryDCAz    = (Double_t)trackarray[39];

}

//_____________________________________________________________________
void TRACK::PrintTrack(){

  //Print Track Information
  cout <<"    " <<"TRACK| " <<"Vx: " <<xVertex <<" Vy: " <<yVertex
       <<" Vz: " <<zVertex <<"\n";

}

