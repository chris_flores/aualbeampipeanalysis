

#include <TGraphErrors.h>

#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"

//Should the Results be saved?
Bool_t SAVEIMG = true;
Bool_t SAVEOBJ = true;

// --- Functions ---
void LoadEnergyLossCurves(TFile *file, Int_t PID, Int_t SIDE);
Double_t InvBeta2Func(Double_t *x, Double_t *par);
void saveImage(TCanvas *CANVAS, TString DIR, TString NAME, TString EXT);

// --- Global Variables ---
TGraphErrors *energyLossGraph[nRapidityBins];
TF1 *energyLossFit[nRapidityBins];

TPaveText label(.57,.31,.88,.42,"BL");

// --- MAIN ---
void fitEnergyLossCurves(TString EMBEDDINGFILE, TString OUTPUTFILE, Double_t ENERGY,
			 Int_t PID, Int_t SIDE, TString IMAGEDIR=""){

  TString imageOutDir = IMAGEDIR+"/";
  //If the image directory is empty then turn off the saving of images
  if (!imageOutDir.CompareTo("") && SAVEIMG == true)
    SAVEIMG = false;

  //Files
  TFile *inFile = new TFile(EMBEDDINGFILE,"READ");
  TFile *outFile = NULL;
  if (SAVEOBJ)
    outFile = new TFile(OUTPUTFILE,"UPDATE");

  //Make subdirectories in the output file
  if (SAVEOBJ){
    //Create Directories
    outFile->mkdir("efficiency/"+GetSideName(SIDE)+"/"+GetParticleName(PID)+"/EnergyLossCurves");
    outFile->mkdir("efficiency/"+GetSideName(SIDE)+"/"+GetParticleName(PID)+"/FitEnergyLossGraphs");
  }
  
  //Make System sub-directories if we are saving images
  if (SAVEIMG){
    gSystem->mkdir(imageOutDir+GetParticleName(PID));
    gSystem->mkdir(imageOutDir+GetParticleName(PID)+"/"+GetSideName(SIDE));
  }


  //Create a Canvas and Frame
  TCanvas *fitCanvas = new TCanvas("fitCanvas","fitCanvas",20,20,800,600);
  fitCanvas->SetRightMargin(.05);
  fitCanvas->SetTopMargin(.05);
  gStyle->SetOptFit(1);

  label.SetTextAlign(11);
  label.SetBorderSize(0);
  label.SetFillColor(kWhite);
  label.Draw("SAME");
  label.Clear();
  fitCanvas->Modified();
  fitCanvas->Update();

  LoadEnergyLossCurves(inFile,PID,SIDE);

  for(Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

    //Check to make sure the Graph has Points
    if (energyLossGraph[yIndex]->GetN() == 0)
      continue;

    //Create Fit Function
    energyLossFit[yIndex] = new TF1(Form("energyLossFit_%02d",yIndex),InvBeta2Func,
				    .1,//energyLossGraph[yIndex]->GetX()[0],
				    1.8,//energyLossGraph[yIndex]->GetX()[energyLossGraph[yIndex]->GetN()],
				    4);

    //Set the Attributes of the Parameters
    energyLossFit[yIndex]->SetParNames("Mass","Rapidity","#it{A}","#it{B}");
    energyLossFit[yIndex]->FixParameter(0,GetParticleMass(PID));
    energyLossFit[yIndex]->SetParameter(1,GetRapidityRangeCenter(yIndex,SIDE));
    energyLossFit[yIndex]->SetParLimits(1,GetRapidityRangeLow(yIndex,SIDE),GetRapidityRangeHigh(yIndex,SIDE));
    energyLossFit[yIndex]->SetParameter(2,5);
    energyLossFit[yIndex]->SetParameter(3,1);
    energyLossFit[yIndex]->SetNpx(10000);

    //Reset the Canvas    
    fitCanvas->Clear();

    TH1F *frame =fitCanvas->DrawFrame(0,PID==0 ? -.03:-.08,2.0,PID==0 ? .01:.02);
    frame->SetTitle(";p_{T}^{Emb};p_{T}^{Reco} - p_{T}^{Emb}");
    frame->GetYaxis()->SetTitleOffset(1.25);
    frame->GetXaxis()->SetTitleOffset(1.2);

    energyLossGraph[yIndex]->Draw("P");

    fitCanvas->Update();

    energyLossGraph[yIndex]->Fit(energyLossFit[yIndex],"R");
    fitCanvas->Update();

    TPaveStats *statsBox = (TPaveStats*)energyLossGraph[yIndex]->GetListOfFunctions()->FindObject("stats");
    statsBox->SetX1NDC(.5);
    statsBox->SetY1NDC(.15);
    statsBox->SetX2NDC(.9);
    statsBox->SetY2NDC(.38);

    label.Clear();
    label.SetX1NDC(.48);
    label.SetY1NDC(.39);
    label.SetX2NDC(.9);
    label.SetY2NDC(.49);
    label.AddText(GetParticleSymbol(PID)+" TPC Energy Loss Correction");
    label.AddText(TString::Format("  Au_{Like}+Al #sqrt{s_{NN}} = %.1f ",ENERGY)+GetSideName(SIDE));
    label.AddText(Form("  %.2f #leq y_{%s} < %.2f",GetRapidityRangeLow(yIndex,SIDE),
		       GetParticleSymbol(PID).Data(),GetRapidityRangeHigh(yIndex,SIDE)));    
    label.GetLine(0)->SetTextSize(.04);
    label.Draw("SAME");

    fitCanvas->Modified();
    fitCanvas->Update();

    if (SAVEIMG){
      saveImage(fitCanvas,imageOutDir+GetParticleName(PID)+"/"+GetSideName(SIDE)+"/",
		GetParticleName(PID)+TString::Format("_TPC_EnergyLoss_%02d",yIndex),"eps");
    }

    if (SAVEOBJ){
      outFile->cd("efficiency/"+GetSideName(SIDE)+"/"+GetParticleName(PID)+"/FitEnergyLossGraphs");
      energyLossGraph[yIndex]->Write();
      outFile->cd("efficiency/"+GetSideName(SIDE)+"/"+GetParticleName(PID)+"/EnergyLossCurves");
      energyLossFit[yIndex]->Write();
    }

    gSystem->Sleep(1000);

  }//End Loop over yIndex
  

  inFile->Close();
  return;
}



//___________________________________________________
void LoadEnergyLossCurves(TFile *file, Int_t PID, Int_t SIDE){

  TString getDir = "efficiency/"+GetSideName(SIDE)+"/"+
    GetParticleName(PID)+"/EnergyLoss/ptLoss/";

  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
    
    energyLossGraph[yIndex] = (TGraphErrors*)file->
      Get(getDir+TString::Format("ptLoss_%s%s_%02d",
				 GetParticleName(PID).Data(),
				 GetSideName(SIDE).Data(),
				 yIndex));

    }

}




//________________________________________________________________
Double_t InvBeta2Func(Double_t *x, Double_t *par){

  Double_t pTEmb = x[0];
  
  Double_t mass = par[0];
  Double_t rapidity = par[1];

  TF1 rapidityFunc("rapidityFunc","log((sqrt([0]*[0] + [1]*[1]*cosh(x)*cosh(x)) + [1]*sinh(x)) / (sqrt([0]*[0] + [1]*[1])) )",0,5);
  rapidityFunc.FixParameter(0,mass);
  rapidityFunc.FixParameter(1,pTEmb);

  Double_t eta = rapidityFunc.GetX(fabs(rapidity));
  Double_t theta =  2.0 * atan(exp(-eta));

  Double_t invBeta2 = (mass*mass + pTEmb*pTEmb*cosh(eta)*cosh(eta)) / (pTEmb*pTEmb*cosh(eta)*cosh(eta));

  return -par[2] * sin(theta) * pow(invBeta2,par[3]);

}

//_______________________________________________________________
void drawInvBeta2Func(){

  TF1 *f1 = new TF1("f1",InvBeta2Func,0,2,2);
  f1->FixParameter(0,.938);
  f1->FixParameter(1,.05);

  f1->Draw();

}


//_________________________________________________________________
void saveImage(TCanvas *CANVAS, TString DIR, TString NAME, TString EXT){
  CANVAS->SaveAs(DIR+NAME+"."+EXT);
}
