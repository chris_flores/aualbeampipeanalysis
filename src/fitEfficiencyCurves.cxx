//This macro fits the efficiency curves for the the rapidity dependent
//Au+Al beampipe analysis.  This means that the efficiency functions 
//are only fit starting at an mT-m0 of .11 GeV.  This is to avoid the 
//divet region in the lower mT-m0 bins present in the Y+ events.


#include <TGraphAsymmErrors.h>

#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"

//_____________________________________________________________________________
Double_t effFitCurve(Double_t *x, Double_t *par){

  //This is efficiency curve is used to fit both pions and protons
  //and should only be used for pions down to an mT-m0 of .11 GeV

  Double_t xx = x[0];
  Double_t f=0;

  f = par[0]*exp(-pow(par[1]/xx,par[2]));

  return f;
}

//______________________________________________________________________________
Double_t ampParFitFunc(Double_t *x, Double_t *par){

  //We expect the amplitude to be flat until far forward rapidity
  
  Double_t xx = x[0];
  Double_t f=0;

  //Check Which SIDE to use Par3
  if (par[3] == 0){

    TF1 *f1 = new TF1("f1","[0]",0,par[2]);
    f1->SetParameter(0,par[0]);
    
    if (xx < par[2])
      f = f1->Eval(xx);
    
    TF1 *f2 = new TF1("f2","[0]*(x-[1])+[2]",par[2],1.7);
    f2->FixParameter(1,par[2]);
    f2->FixParameter(2,f1->Eval(par[2]));
    f2->SetParameter(0,par[1]);
    
    if (xx > par[2])
      f = f2->Eval(xx);
  }

  else if (par[3] == 1){
    TF1 *f1 = new TF1("f1","[0]",0,par[2]);
    f1->SetParameter(0,par[0]);
    
    if (xx > par[2])
      f = f1->Eval(xx);
    
    TF1 *f2 = new TF1("f2","[0]*(x-[1])+[2]",par[2],1.7);
    f2->FixParameter(1,par[2]);
    f2->FixParameter(2,f1->Eval(par[2]));
    f2->SetParameter(0,par[1]);
    
    if (xx < par[2])
      f = f2->Eval(xx);
  }

  return f;

}

//______________________________________________________________________________
void saveImage(TCanvas *CANVAS, TString DIR, TString NAME, TString EXT){

  CANVAS->SaveAs(DIR+NAME+"."+EXT);

}

//______________________________________________________________________________
void fitEfficiencyCurves(TString INPUTFILE, TString OUTPUTFILE, Double_t ENERGY, 
			 Int_t PID, Int_t SIDE, TString IMAGEDIR=""){

  //Save Images and Objects?
  Bool_t SAVE = true;
  Bool_t SAVEIMG = true;

  //microseconds between drawings
  Int_t WAIT = 0; 

  //NOTE: The Efficiency functions in this script are defined to start at a 
  //      variable mT-m0 depending on the rapidity slice
  Double_t effFitRangeLow[3] = {.05,.1,.125}; //Rapidity Dependent Lower Bound
  Double_t effFitRangeHigh = 1.0;

  //Open the Root File
  TFile *inFile = new TFile(INPUTFILE,"READ");
  
  //Create the output File
  TFile *outFile = NULL;
  if (SAVE)
    outFile = new TFile(OUTPUTFILE,"UPDATE");

 //Set the Side String
  TString sideStr;
  if (SIDE == 0)
    sideStr = "PosY";
  else if (SIDE == 1)
    sideStr = "NegY";

  //Set the Image OutputDirectory
  TString imageOutDir = IMAGEDIR+"/";

  //If the image directory is empty then turn off saving images
  if (!imageOutDir.CompareTo("") && SAVEIMG == true)
    SAVEIMG = false;

  //Make the New subdirectories if we are saving images
  if (SAVEIMG){
    gSystem->mkdir(imageOutDir+GetParticleName(PID));
    gSystem->mkdir(imageOutDir+GetParticleName(PID)+"/"+sideStr);
  }
  
  /*
  if (ENERGY == 3.0)
    imageOutDir = "/home/chris/BeamPipeAnalysis/AuAl_3_0/Images/embedding/";
  else if (ENERGY == 3.5)
    imageOutDir = "/home/chris/BeamPipeAnalysis/AuAl_3_5/Images/embedding/";
  else if (ENERGY == 4.5)
    imageOutDir = "/home/chris/BeamPipeAnalysis/AuAl_4_5/Images/embedding/";
  */

  //Make a new Directory for the Efficiency Curves
  if (SAVE){
    outFile->mkdir("efficiency/"+sideStr+"/"+GetParticleName(PID)+"/EfficiencyCurves");
    outFile->mkdir("efficiency/"+sideStr+"/"+GetParticleName(PID)+"/FitEfficiencyGraphs");
  }

  //Declare Variables
  TGraphAsymmErrors *effGraph[nRapidityBins];
  TF1 *effCurve[nRapidityBins][4];  //There are four rounds of fitting
  TGraphErrors *effParGraph[3][4];
  TF1 *effParFitCurve[3];
  Int_t nPoint = 0;

  //The Final Efficiency curve which is written to the file
  TF1 *effFinalCurve = new TF1("",effFitCurve,0,1.2,3); 
  effFinalCurve->SetNpx(100000);

  //Create Canvases
  TCanvas *fitCanvas = new TCanvas("fitCanvas","fitCanvas",0,0,800,600);
  fitCanvas->SetMargin(.07,.01,.1,.03);
  fitCanvas->SetTicks(1,1);
  gStyle->SetOptFit(1);

  Int_t scale = SAVEIMG ? 2 : 1;
  TCanvas *parCanvas = new TCanvas("parCanvas","parCanvas",
				   910,0,900*scale,PID==0 ? 900*scale : 600*scale);
  parCanvas->Divide(3,PID==0 ? 3 : 2);

  //Define the Fit Parameter Graphs
  TString parName[3] = {"Amplitude","Slope","Power"};
  for (Int_t i=0; i<3; i++){
    for (Int_t j=0; j<4; j++){
      effParGraph[i][j] = new TGraphErrors();
      effParGraph[i][j]->SetName(Form("effFit_Par_%d_round_%d",i,j));
      effParGraph[i][j]->SetMarkerStyle(20);
      effParGraph[i][j]->SetTitle(Form("%s %.1f GeV %s  Parameter: %s  Fit Round: %d;y_{%s};Parameter Value",
				       GetParticleSymbol(PID).Data(),ENERGY,sideStr.Data(),parName[i].Data(),j,
				       GetParticleSymbol(PID).Data()));
      effParGraph[i][j]->GetHistogram()->SetTitleSize(.05);
      }
  }

  //Define the Parameter Fit Functions
  effParFitCurve[0] = new TF1("parFit_"+parName[0],ampParFitFunc,
			      SIDE==0 ? .2 : -.2,SIDE==0 ? 1.7 : -1.7,4);
  effParFitCurve[0]->SetParameter(0,.88);
  if (SIDE == 0){
    effParFitCurve[0]->SetParameter(1,-.1);
    effParFitCurve[0]->SetParameter(2,1.33);
  }
  else if (SIDE == 1){
    effParFitCurve[0]->SetParameter(1,.1);
    effParFitCurve[0]->SetParameter(2,-1.33);
  }
  effParFitCurve[0]->FixParameter(3,SIDE);

  effParFitCurve[0]->SetParNames("Constant","Slope","Boundary","Side Index");

  effParFitCurve[1] = new TF1("parFit"+parName[1],PID==0 ? "pol2" : "pol3",
			      0,SIDE==0 ? 1.7 : -1.7);

  effParFitCurve[2] = new TF1("parFit"+parName[2],"pol4",
			      0,SIDE==0 ? 1.7 : -1.7);

  //Get the Efficiency Graphs from the File
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
    
    effGraph[yIndex] = 
      (TGraphAsymmErrors *)inFile->Get("efficiency/"+sideStr+"/"+
				       GetParticleName(PID)+"/Histograms/"+
				       GetParticleName(PID)+"_TPC_Efficiency_"+
				       TString::Format("%02d",yIndex));
    
  }//End Get Efficiency Graphs

  //Set the Label for Drawing Efficiency Curves in last Loop
  TPaveText *label = new TPaveText(.57,.31,.88,.42,"BL");
  label->SetTextAlign(11);
  label->SetBorderSize(0);
  label->SetFillColor(kWhite);
  TPaveStats *parStats0, *parStats1, *parStats2;


  //The Fitting of the efficiency curves proceeds in multiple loops where after
  //each loop a parameter of the fit is extracted and then fixed in the next
  //iteration of the loop. For the pions three loops are needed while for the 
  //proton only two loops are necessary
  Int_t nLoops = PID==0 ? 3 : 2;
  for (Int_t loopIndex=0; loopIndex<nLoops; loopIndex++){
    
    //Within each loop iterations we loop over the rapidity bins to fit
    //the efficiency curve with the efficiency function
    nPoint = 0;
    for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      //Check to Make sure the efficiency graph has entries
      effCurve[yIndex][loopIndex] = 0;
      if (effGraph[yIndex]->GetN() <= 1){
	continue;
      }

      //Set the Lower Fit Bound
      Double_t lowerBound = effFitRangeLow[2];
      if (yIndex < 14)
	lowerBound = effFitRangeLow[0];
      if (yIndex >=14 && yIndex < 17)
	lowerBound = effFitRangeLow[1];

      //If we are Fitting Proton Efficiencies the Lower Range Does not matter
      if (PID == 4)
	lowerBound = 0.025;

      //Define the Efficiency Function
      effCurve[yIndex][loopIndex] = 
	new TF1(Form(GetParticleName(PID)+"_effFit_yIndex_%02d_round_%d",
		     yIndex,loopIndex),effFitCurve,lowerBound,effFitRangeHigh,3);
      effCurve[yIndex][loopIndex]->SetParNames("Amplitude","Slope","Power");
      effCurve[yIndex][loopIndex]->SetLineWidth(3);

      //If this is the zeroth loop Set the Parameters to some initial guess
      if (loopIndex == 0){

	//Check if the previous yIndex effCurve parameters can be used as seeds
	cout <<effCurve[yIndex-1][loopIndex] <<" " <<inFile<<endl;
	if (effCurve[yIndex-1][loopIndex]){

	  effCurve[yIndex][loopIndex]->SetParameters(effCurve[yIndex-1][loopIndex]->GetParameter(0),
	  					     effCurve[yIndex-1][loopIndex]->GetParameter(1),
	  					     effCurve[yIndex-1][loopIndex]->GetParameter(2));
	}
	else{
	  effCurve[yIndex][loopIndex]->SetParameters(.8,.07,2.0);
	}
      }

      cout <<effCurve[yIndex-1][loopIndex] <<endl;

      //If this is the 1st loop Fix the amplitude parameter of the efficiency Function
      //and use the previous values of the fit for the seed values of this fit
      if (loopIndex == 1){
	effCurve[yIndex][loopIndex]->FixParameter(0,effParFitCurve[0]->
						  Eval(GetRapidityRangeCenter(yIndex,SIDE)));
	effCurve[yIndex][loopIndex]->SetParameter(1,
						  effCurve[yIndex][loopIndex-1]->GetParameter(1));
	effCurve[yIndex][loopIndex]->SetParameter(2,
						  effCurve[yIndex][loopIndex-1]->GetParameter(2));
						  
      }

      //If this is the 2nd loop fix both the amplidtude and slope parameters
      if (loopIndex == 2){
	effCurve[yIndex][loopIndex]->FixParameter(0,effParFitCurve[0]->
						  Eval(GetRapidityRangeCenter(yIndex,SIDE)));
	effCurve[yIndex][loopIndex]->FixParameter(1,effParFitCurve[1]->
						  Eval(GetRapidityRangeCenter(yIndex,SIDE)));
	effCurve[yIndex][loopIndex]->SetParameter(2,
						  effCurve[yIndex][loopIndex-1]->GetParameter(2));
      }

      //If this is the 3rd loop fix all three parameters
      if (loopIndex == 3){
 	effCurve[yIndex][loopIndex]->FixParameter(0,effParFitCurve[0]->
						  Eval(GetRapidityRangeCenter(yIndex,SIDE)));
	effCurve[yIndex][loopIndex]->FixParameter(1,effParFitCurve[1]->
						  Eval(GetRapidityRangeCenter(yIndex,SIDE)));
	effCurve[yIndex][loopIndex]->FixParameter(2,effParFitCurve[2]->
						  Eval(GetRapidityRangeCenter(yIndex,SIDE)));
      }

      //Draw the Efficiency Graph on the Canvas
      fitCanvas->cd();
      effGraph[yIndex]->Draw("APZ");
      gPad->Update();
      
      //Fit the Efficiency Graph
      effGraph[yIndex]->Fit(effCurve[yIndex][loopIndex],"R");

      //Set the Location of the Stats Box
      TPaveStats *statsBox = (TPaveStats*)effGraph[yIndex]->GetListOfFunctions()->FindObject("stats");
      
      // -- If this is the last Loop Prepare the Efficiency Graph for Saving ---
      // --- then Save it to the input file ---
      if (loopIndex == nLoops-1){
	effGraph[yIndex]->SetTitle("");
	effGraph[yIndex]->GetXaxis()->SetTitleSize(0.045);
	effGraph[yIndex]->GetXaxis()->SetTitleOffset(.85);
	effGraph[yIndex]->SetMarkerStyle(20);
	effGraph[yIndex]->SetMarkerSize(.7);

	statsBox->SetX1NDC(.57);
	statsBox->SetY1NDC(.15);
	statsBox->SetX2NDC(.88);
	statsBox->SetY2NDC(.31);

	label->Clear();
	label->SetX1NDC(.555);
	label->SetY1NDC(.31);
	label->SetX2NDC(.88);
	label->SetY2NDC(.43);
	label->AddText(GetParticleSymbol(PID)+" TPC Efficiency #times Acceptance");
	label->AddText(TString::Format("  Au_{Like}+Al #sqrt{s_{NN}} = %.1f ",ENERGY)+sideStr);
	label->AddText(Form("  %.2f #leq y_{%s} < %.2f",GetRapidityRangeLow(yIndex,SIDE),
			    GetParticleSymbol(PID).Data(),GetRapidityRangeHigh(yIndex,SIDE)));

	label->GetLine(0)->SetTextSize(.04);

	label->Draw("SAME");

	//Pass the Values of the effCurve to the effFinalCurve
	effFinalCurve->SetName(GetParticleName(PID)+"_"+sideStr+
			       TString::Format("_effFit_%02d",yIndex));
	effFinalCurve->FixParameter(0,effCurve[yIndex][loopIndex]->GetParameter(0));
	effFinalCurve->FixParameter(1,effCurve[yIndex][loopIndex]->GetParameter(1));
	effFinalCurve->FixParameter(2,effCurve[yIndex][loopIndex]->GetParameter(2));	

	//Save the Efficiency Curve
	if (SAVEIMG){
	  saveImage(fitCanvas,imageOutDir+GetParticleName(PID)+"/"+sideStr+"/",
	  	    effGraph[yIndex]->GetName(),"eps");
	}
	if (SAVE){
	  outFile->cd("efficiency/"+sideStr+"/"+GetParticleName(PID)+"/EfficiencyCurves/");
	  effFinalCurve->Write();
	  outFile->cd("efficiency/"+sideStr+"/"+GetParticleName(PID)+"/FitEfficiencyGraphs/");
	  effGraph[yIndex]->SetName(GetParticleName(PID)+"_"+sideStr+
				    TString::Format("_effGraph_%02d",yIndex));
	  effGraph[yIndex]->Write();
	}
      }

      //Update the Canvas and Pause
      fitCanvas->Update();
      gSystem->Sleep(WAIT);
        
      //Add the results of the fit to the efficiency Parameter Graphs
      effParGraph[0][loopIndex]->SetPoint(nPoint,
					  GetRapidityRangeCenter(yIndex,SIDE),
					  effCurve[yIndex][loopIndex]->GetParameter(0));
      effParGraph[0][loopIndex]->SetPointError(nPoint,0,
					       effCurve[yIndex][loopIndex]->GetParError(0));		       
      effParGraph[1][loopIndex]->SetPoint(nPoint,
					  GetRapidityRangeCenter(yIndex,SIDE),
					  effCurve[yIndex][loopIndex]->GetParameter(1));
      effParGraph[1][loopIndex]->SetPointError(nPoint,0,
					       effCurve[yIndex][loopIndex]->GetParError(1));
      effParGraph[2][loopIndex]->SetPoint(nPoint,
					  GetRapidityRangeCenter(yIndex,SIDE),
					  effCurve[yIndex][loopIndex]->GetParameter(2));
      effParGraph[2][loopIndex]->SetPointError(nPoint,0,
					       effCurve[yIndex][loopIndex]->GetParError(2));

      nPoint++;

    }//End Loop Over y Index

    //Draw the Parameter Graphs
    TPaveText *title = 0;
    for (Int_t parIndex=0; parIndex<3; parIndex++){
      Int_t startPad[4] = {1,4,7,10};
      parCanvas->cd(startPad[loopIndex]+parIndex);
      effParGraph[parIndex][loopIndex]->Draw("APZ");
      effParGraph[parIndex][loopIndex]->GetHistogram()->GetYaxis()->SetTitleOffset(1.5);
      effParGraph[parIndex][loopIndex]->GetHistogram()->GetXaxis()->SetTitleSize(.045);
      effParGraph[parIndex][loopIndex]->GetHistogram()->GetYaxis()->SetTitleSize(.045);
      gPad->SetMargin(0.125,0.02,.1,.08);
      title = (TPaveText*)gPad->GetPrimitive("title");
      if (title)
	title->SetTextSize(.04);
      gPad->Modified();     
  
    }

    //Fit the Parameter Curve
    if (loopIndex == 0){
      effParGraph[0][loopIndex]->Fit(effParFitCurve[0],"R");
      parCanvas->Update();
      parStats0 = (TPaveStats*)effParGraph[0][loopIndex]->
	GetListOfFunctions()->FindObject("stats");
      parStats0->SetX1NDC(SIDE==0 ? .165 : .465);
      parStats0->SetY1NDC(.15);
      parStats0->SetX2NDC(SIDE==0 ? .63 : .95);
      parStats0->SetY2NDC(.45);
      parCanvas->cd(1);
      parStats0->Draw();
    }
    else if (loopIndex == 1 && loopIndex < nLoops - 1){
      effParGraph[1][loopIndex]->Fit(effParFitCurve[1],"R");
      parCanvas->Update();
      parStats1 = (TPaveStats*)effParGraph[1][loopIndex]->
	GetListOfFunctions()->FindObject("stats");
      parStats1->SetX1NDC(SIDE==0 ? .165 : .465);
      parStats1->SetY1NDC(.58);
      parStats1->SetX2NDC(SIDE==0 ? .65 : .95);
      parStats1->SetY2NDC(.87);
      parCanvas->cd(5);
      parStats1->Draw();
    }
    else if (loopIndex == 2 && loopIndex < nLoops - 1){
      effParGraph[2][loopIndex]->Fit(effParFitCurve[2],"R");
      parCanvas->Update();
      parStats2 = (TPaveStats*)effParGraph[2][loopIndex]->
	GetListOfFunctions()->FindObject("stats");
      parStats2->SetX1NDC(SIDE==0 ? .46 : .51);
      parStats2->SetY1NDC(SIDE==0 ? .13 : .59);
      parStats2->SetX2NDC(SIDE==0 ? .90 : .95);
      parStats2->SetY2NDC(SIDE==0 ? .42 : .88);
      parCanvas->cd(9);
      parStats2->Draw();
    }
    parCanvas->Update();
    
  }//End Loop Over Four Fitting iterations

  //Save the Parameter Canvas
  if (SAVEIMG){
    saveImage(parCanvas,imageOutDir+GetParticleName(PID)+"/"+sideStr+"/",
  	      GetParticleName(PID)+"_FitParameters_Rapidity","eps");
  }


  inFile->Close();
  outFile->Close();

  return;
}
