/*******************************************************************
 **
 **
 **
 **
 **
 **
 **
 *******************************************************************/

#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"

//Save?
const Bool_t SAVEOBJ=false;
const Bool_t SAVEIMG=false;

// --- Functions ---
Double_t PionRatioFit(Double_t *x, Double_t *par);
void GetPionSpectra(TFile *spectraFile);
void GetSpectraFits(TFile *spectraFile);
void MakeRatio(TGraphErrors *numerator,
	       TGraphErrors *denominator,
	       TGraphErrors *ratio);

// --- Global Variables

//Pion Spectra
TGraphErrors *pionPlusSpectra[nRapidityBins];
TGraphErrors *pionMinusSpectra[nRapidityBins];

//Spectra Fits (Needed for the Slope Parameters)
TF1 *pionPlusSpectraFit[nRapidityBins];
TF1 *pionMinusSpectraFit[nRapidityBins];
TF1 *protonPlusSpectraFit[nRapidityBins];

//Pion Ratio
TGraphErrors *pionRatio[nRapidityBins];
TF1 *pionRatioFit[nRapidityBins];

//Canvases
TCanvas *ratioCanvas;

//Stat Boxes and Labels
TPaveStats *ratioStats;
TPaveText *ratioLabel;

// MAIN _____________________________________________________________
void makePionRatios(TString SPECTRAFILE, Double_t ENERGY){

  //Load the File and Get the Pion Spectra
  TFile *spectraFile = new TFile(SPECTRAFILE,SAVEOBJ ? "UPDATE":"READ");
  GetPionSpectra(spectraFile);
  GetSpectraFits(spectraFile);

  //Create New Directories in the Spectra File
  if (SAVEOBJ)
    spectraFile->mkdir("PionRatios/");

  //Create the Canvases
  ratioCanvas = new TCanvas("ratioCanvas","Ratio Canvas",20,20,800,600);
  ratioCanvas->SetTopMargin(.05);
  ratioCanvas->SetRightMargin(.05);
  TH1F *ratioFrame;

  //Set the Ratio Label
  ratioLabel = new TPaveText(.61,.38,.93,.47,"BL NDC");
  ratioLabel->SetTextAlign(11);
  ratioLabel->SetFillColor(kWhite);
  ratioLabel->SetBorderSize(0);

  //Set Style Options
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(112);

  //Loop Over the Rapidity Bins and Compute the Pion Ratio
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

    //Make Sure the Spectra Exist
    if (!pionPlusSpectra[yIndex] || !pionMinusSpectra[yIndex])
      continue;

    //Make Sure the Spectra Fits Exist
    if (!pionPlusSpectraFit[yIndex] || !pionMinusSpectraFit[yIndex] ||
	!protonPlusSpectraFit[yIndex]){
      cout <<"A Spectra Fit is Missing!" <<endl;
      continue;
    }

    //Define the Ratio Frame
    ratioFrame = ratioCanvas->DrawFrame(0,.5,.8,1.2);
    ratioFrame->SetYTitle(Form("%s/%s",GetParticleSymbol(0).Data(),
			      GetParticleSymbol(1).Data()));
    ratioFrame->SetXTitle("m_{T}-m_{#pi} (GeV)");

    //Define the Ratio Graph
    pionRatio[yIndex] = new TGraphErrors();
    pionRatio[yIndex]->SetName(Form("pionRatio_%02d",yIndex));
    pionRatio[yIndex]->SetMarkerStyle(20);

    //Construct the Ratio
    MakeRatio(pionPlusSpectra[yIndex],
	      pionMinusSpectra[yIndex],
	      pionRatio[yIndex]);

    //Draw the Ratio
    ratioCanvas->cd();
    pionRatio[yIndex]->Draw("PZ");

    //Compute the Average Pion Temperature
    Double_t pionPlusTemp = pionPlusSpectraFit[yIndex]->GetParameter(1);
    Double_t pionMinusTemp = pionMinusSpectraFit[yIndex]->GetParameter(1);
    Double_t avgPionTemp = .5 * (pionPlusTemp + pionMinusTemp);

    //Get the Proton Temp
    Double_t protonTemp = protonPlusSpectraFit[yIndex]->GetParameter(1);

    cout <<pionPlusTemp <<" " <<pionMinusTemp <<" " <<protonTemp <<endl;

    //Define the Pion Ratio Fitting Function
    pionRatioFit[yIndex] = new TF1(Form("pionRatioFit_%02d",yIndex),
				   PionRatioFit,.05,.4,5);
    pionRatioFit[yIndex]->SetParameter(0,.92);         //Initial Pion Ratio
    pionRatioFit[yIndex]->SetParameter(1,4.3);         //Coulomb Potential
    pionRatioFit[yIndex]->FixParameter(2,avgPionTemp); //Average Pion Temp
    pionRatioFit[yIndex]->FixParameter(3,protonTemp);  //Proton Temp
    //pionRatioFit[yIndex]->FixParameter(4,0.0);
    pionRatioFit[yIndex]->FixParameter(4,GetRapidityRangeCenter(yIndex,0)-
				       GetMidRapidity(ENERGY));

    pionRatioFit[yIndex]->SetParLimits(0,.3,1);
    pionRatioFit[yIndex]->SetParLimits(1,1,20);

    pionRatioFit[yIndex]->SetParNames("Init. R_{#pi}","V_{C}",
				      Form("#pi_{TSlope}"),
				      Form("p_{TSlope}"),
				      "y_{#pi}-y_{Mid}");

    //Fit the Pion Ratio
    ratioCanvas->cd();
    pionRatio[yIndex]->Fit(pionRatioFit[yIndex],"R EX0");
    gPad->Update();

    //Move the Stats Box
    ratioStats = (TPaveStats*)pionRatio[yIndex]->FindObject("stats");
    ratioStats->SetX1NDC(.62);
    ratioStats->SetY1NDC(.15);
    ratioStats->SetX2NDC(.93);
    ratioStats->SetY2NDC(.37);

    //Set and Draw the Label
    ratioLabel->Clear();
    ratioLabel->AddText("Pion Ratio");
    ratioLabel->AddText(Form("  Au_{Like}+Al #sqrt{s_{NN}} = %.1f GeV",ENERGY));
    ratioLabel->AddText(Form("  %.2f #leq y_{#pi} < %.2f",
			     GetRapidityRangeLow(yIndex,0),
			     GetRapidityRangeHigh(yIndex,0)));
    ratioLabel->GetLine(0)->SetTextSize(.04);
    ratioLabel->Draw("SAME");

    ratioCanvas->Modified();
    ratioCanvas->Update();
    
    if (SAVEOBJ)
      SaveObject(pionRatio[yIndex],spectraFile,"PionRatios/");

    if (SAVEIMG)
      SaveImage(ratioCanvas,
		ImageDir(ENERGY)+"PionRatios/",
		Form("pionRatio_%02d",yIndex),
		"png");

    gSystem->Sleep(1000);

  }//End Loop Over yIndex

  spectraFile->Close();
}

//___________________________________________________________________
void MakeRatio(TGraphErrors *numerator, TGraphErrors *denominator,
	       TGraphErrors *ratioGraph){

    //Get the Number of Points for each Spectra
  Int_t nPoints0 = numerator->GetN();
  Int_t nPoints1 = denominator->GetN();

  //Arrays to Keep the Values of the Points
  Double_t *x0, *y0, *xE0, *yE0;
  Double_t *x1, *y1, *xE1, *yE1;
  Double_t ratio, ratioErr, ratioPoint=0;
  Double_t xAvg;

  //Get All the Quantities
  x0 = numerator->GetX();
  y0 = numerator->GetY();
  xE0 = numerator->GetEX();
  yE0 = numerator->GetEY();
  x1 = denominator->GetX();
  y1 = denominator->GetY();
  xE1 = denominator->GetEX();
  yE1 = denominator->GetEY();

    //Loop Over the Possible Values of mT-m0 and compute the Weighted Average
  for (Int_t mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
    
    //Loop Over the Points of the Left Sided Spectra and find the point
    //Belonging to the current mTm0Index
    Int_t pt0 = -10;
    for (Int_t i=0; i<nPoints0; i++){      
      if (x0[i] > GetmTm0RangeLow(mTm0Index) && x0[i] < GetmTm0RangeHigh(mTm0Index)){
	pt0 = i;
	break;
      }
    }

    //Loop Over the Points of the Right Sided Spectra and find the point
    //Belonging to the current mTm0Index
    Int_t pt1 = -10;
    for (Int_t i=0; i<nPoints1; i++){
      if (x1[i] > GetmTm0RangeLow(mTm0Index) && x1[i] < GetmTm0RangeHigh(mTm0Index)){
	pt1 = i;
	break;
      } 
    }

    //Skip Points where there is no data
    if (pt0 == -10 || pt1 == -10)
      continue;

    //Compute the Ratio of the points
    ratio = y0[pt0]/y1[pt1];
    ratioErr = ratio * sqrt(pow(yE0[pt0]/y0[pt0],2) + pow(yE1[pt1]/y1[pt1],2));

    //Compute the Average x Location
    xAvg = .5 * (x0[pt0] + x1[pt1]);

    //Set the Point in the Ratio Graph
    ratioGraph->SetPoint(ratioPoint,xAvg,ratio);
    ratioGraph->SetPointError(ratioPoint,0,ratioErr);
    ratioPoint++;

  }//End Loop Over mTm0Index



}

//___________________________________________________________________
void GetPionSpectra(TFile *spectraFile){


  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
    
    spectraFile->cd("AveragedSpectra/"+GetParticleName(0));
    
    pionPlusSpectra[yIndex] = (TGraphErrors*)gDirectory->
      Get("avgSpectra_"+GetParticleName(0)+
	  TString::Format("_%02d",yIndex));

    spectraFile->cd("AveragedSpectra/"+GetParticleName(1));
    pionMinusSpectra[yIndex] = (TGraphErrors*)gDirectory->
      Get("avgSpectra_"+GetParticleName(1)+
	  TString::Format("_%02d",yIndex));
    
  }

}

//___________________________________________________________________
void GetSpectraFits(TFile *spectraFile){

  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

    spectraFile->cd("SpectraFitCurves/"+GetParticleName(0));
    pionPlusSpectraFit[yIndex] = (TF1*)gDirectory->
      Get("spectraFit_"+GetParticleName(0)+
	  TString::Format("_%02d",yIndex));

    spectraFile->cd("SpectraFitCurves/"+GetParticleName(1));
    pionMinusSpectraFit[yIndex] = (TF1*)gDirectory->
      Get("spectraFit_"+GetParticleName(1)+
	  TString::Format("_%02d",yIndex));

    spectraFile->cd("SpectraFitCurves/"+GetParticleName(4));
    protonPlusSpectraFit[yIndex] = (TF1*)gDirectory->
      Get("spectraFit_"+GetParticleName(4)+
	  TString::Format("_%02d",yIndex));

  }

}

//___________________________________________________________________
Double_t PionRatioFit(Double_t *x, Double_t *par){

  Double_t mTm0 = x[0];

  //Fit Parameters
  Double_t initialRatio = par[0];
  Double_t coulombV = par[1]/1000.0;
  Double_t pionTemp = par[2];  //This Should be Fixed the pion Spectra Fit
  Double_t protonTemp = par[3];//This Should be Fixed from the proton Spectra Fit
  Double_t y = par[4];         //This Should be Fixed (y-yMid)

  //Additional Variables for -->Pion<--
  Double_t m0 = GetParticleMass(0);
  Double_t mT = mTm0 + m0;
  Double_t energy = mT*cosh(y);
  //Double_t p = sqrt(energy*energy - m0*m0);
  //Double_t pT = sqrt(mT*mT - m0*m0);
  Double_t gamma = energy/m0;                   
  Double_t beta = sqrt(1.0 - pow(1.0/gamma,2)); 

  //Additional Variables for -->Proton<--
  Double_t mP = GetParticleMass(4);

  //Compute the Effective Coulomb Potential
  Double_t eMax = sqrt(pow(mP*beta*gamma,2) + pow(mP,2)) - mP;
  Double_t effCoulombV = coulombV * (1.0 - exp(-eMax/protonTemp));


  //Compute the Transformation Jacobian
  Double_t num = sqrt(pow(energy - effCoulombV,2) - pow(m0,2));
  Double_t denom = sqrt(pow(energy + effCoulombV,2) - pow(m0,2));
  Double_t Jacobian = (energy - effCoulombV) / (energy + effCoulombV);
  Jacobian = Jacobian * ( num/denom );

  //Compute the Emmision Factor for Bose Einstein
  Double_t emmisionFactor = exp( (energy + effCoulombV)/pionTemp ) - 1.0;
  emmisionFactor = emmisionFactor / (exp( (energy - effCoulombV)/pionTemp) - 1.0);

  //Assemble it all Together
  return initialRatio * emmisionFactor * Jacobian;

  

}
