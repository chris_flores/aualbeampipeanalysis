/****************************************************************************
 **This macro generates the nSigma Histograms for all particles.
 **
 **
 **
 **
 **
 **Author: Christopher E. Flores (UC Davis)
 **Email:  chrflores@ucdavis.edu
 **
 ****************************************************************************/

#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"

// --- Functions ---
void DefineHistograms(Double_t ENERGY);
void PutTrackInNsigmaHisto(TRACK *track, TString side);
void FillHistogramInRange(TH1F *Histo, Double_t Value);
void FillPureSampleHistograms(TRACK *track, Int_t sideIndex,Int_t pIndex, 
			      Int_t yIndex, Int_t mTm0Index);
void Save(TFile *outFile);

//Particle Idenfication Functions
void LoadPIDFunctions(TFile *pidFile);
Double_t ComputeTPCZ(TRACK *track, Int_t particleID);
Double_t ComputeTOFZ(TRACK *track, Int_t particleID);

// --- Global Variables ---

//Full PID Plots
TH2F *dEdxFull[2]; //Two Sides
TH2F *invBeta[2];  //Two Sides

//nSigmaTPC Variables
TH1F *nSigmaTPCHisto[2][6][nRapidityBins][nmTm0Bins]; //[sides][particles][..][..]
TH1F *nSigmaTPCPureSampleHisto[2][6][nRapidityBins][nmTm0Bins][10]; //nSigma With TOF Cut
TH2F *nSigmaTPCdEdx[2][6][nRapidityBins][nmTm0Bins];
TH2F *nSigmaTPCdEdxPureSample[2][6][nRapidityBins][nmTm0Bins][10];
TH2F *nSigmaTPCInvBeta[2][6][nRapidityBins][nmTm0Bins];
TH2F *nSigmaTPFInvBetaPureSample[2][6][nRapidityBins][nmTm0Bins][10];

//dEdx Variables
TH1F *dEdxTPCHisto[2][6][nRapidityBins][nmTm0Bins]; //[sides][particles][..][..]
TH1F *dEdxTPCPureSampleHisto[2][6][nRapidityBins][nmTm0Bins][10]; //dEdx With TOF Cut
TH2F *dEdxTPCdEdx[2][6][nRapidityBins][nmTm0Bins];
TH2F *dEdxTPCdEdxPureSample[2][6][nRapidityBins][nmTm0Bins][10];

//ZTPC Variables
TH1F *zTPCHisto[2][6][nRapidityBins][nmTm0Bins]; //[sides][particles][..][..]
TH1F *zTPCPureSampleHisto[2][6][nRapidityBins][nmTm0Bins][10]; //zTPC With TOF Cut
TH2F *zTPCdEdx[2][6][nRapidityBins][nmTm0Bins];
TH2F *zTPCdEdxPureSample[2][6][nRapidityBins][nmTm0Bins][10];

//nSigmaTOF Variables
TH1F *nSigmaTOFHisto[2][6][nRapidityBins][nmTm0Bins];//[sides][particles][..][..]
TH1F *nSigmaTOFPureSampleHisto[2][6][nRapidityBins][nmTm0Bins][10]; //nSigma TOF STILL with TOF Cut
TH2F *nSigmaTOFInvBeta[2][6][nRapidityBins][nmTm0Bins];
TH2F *nSigmaTOFInvBetaPureSample[2][6][nRapidityBins][nmTm0Bins][10];

//invBeta Variables
TH1F *invBetaTOFHisto[2][6][nRapidityBins][nmTm0Bins];//[sides][particles][..][..]
TH1F *invBetaTOFPureSampleHisto[2][6][nRapidityBins][nmTm0Bins][10]; //invBeta TOF STILL with TOF Cut
TH2F *invBetaTOFInvBeta[2][6][nRapidityBins][nmTm0Bins];
TH2F *invBetaTOFInvBetaPureSample[2][6][nRapidityBins][nmTm0Bins][10];

//ZTOF Variables
TH1F *zTOFHisto[2][6][nRapidityBins][nmTm0Bins];//[sides][particles][..][..]
TH1F *zTOFPureSampleHisto[2][6][nRapidityBins][nmTm0Bins][10]; //zTOF TOF STILL with TOF Cut
TH2F *zTOFInvBeta[2][6][nRapidityBins][nmTm0Bins];
TH2F *zTOFInvBetaPureSample[2][6][nRapidityBins][nmTm0Bins][10];

//PID Function Variables
//TFile *pidFile;
TF1 *tpcPIDFunc[nParticles];
TF1 *tofPIDFunc[nParticles];


//Number of Events on Each side of the Detector
TH1F *nEventsSideHisto;

// MAIN______________________________________________________________________________________
void makeYieldHistograms(TString SKIMFILE, TString PARTICLEYIELDFILE, TString PIDFILE, Double_t ENERGY){

  //Load the Skimmed Data File and Create the Trees for Reading in the Data
  TFile *inFile = new TFile(SKIMFILE,"READ");  
  TTree *leftTree = (TTree*)inFile->Get("LeftEvents");
  TTree *rightTree = (TTree*)inFile->Get("RightEvents");

  //Load the PIDFunction File and Load the PID Functions
  TFile *pidFile = new TFile(PIDFILE,"READ");
  LoadPIDFunctions(pidFile);
  if (tpcPIDFunc[0])
    cout <<"PID Functions Loaded." <<endl;

  //Create an Event and Trackto hold the Entry Info
  EVENT *event = new EVENT();
  TRACK *track;

  //Set the Branch address
  TBranch *leftBranch = leftTree->GetBranch("LeftEventList");
  leftBranch->SetAddress(&event);
  TBranch *rightBranch = rightTree->GetBranch("RightEventList");
  rightBranch->SetAddress(&event);

  //Make the Particle Yield File
  TFile *outFile = new TFile(PARTICLEYIELDFILE,"RECREATE");

  //Create the Directory Structure in the Output File
  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t pIndex=0; pIndex<6; pIndex++){
      outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTPC/");
      outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/dEdxTPC/");
      outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTPC/");
      outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTOF/");
      outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/invBetaTOF/");
      outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTOF/");

      if (pIndex == 0 || pIndex == 2 || pIndex == 4){
	for (Int_t pureIndex=0; pureIndex<10; pureIndex+=2){	
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTPC/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/dEdxTPC/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTPC/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTOF/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/invBetaTOF/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTOF/PureSamples/"+GetParticleName(pureIndex));	       
	}
      }
      //cout <<pIndex%2 <<endl;
      else if (pIndex == 1 || pIndex == 3 || pIndex == 5){
	for (Int_t pureIndex=1; pureIndex<10; pureIndex+=2){	
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTPC/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/dEdxTPC/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTPC/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTOF/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/invBetaTOF/PureSamples/"+GetParticleName(pureIndex));
	  outFile->mkdir(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTOF/PureSamples/"+GetParticleName(pureIndex));	    
	}
      }
    }
  }//End Create Output Directory Structure
  cout <<"Output File Directory Structure Created." <<endl;

  //Define the Histograms
  DefineHistograms(ENERGY);
  cout <<"Histograms Defined." <<endl;

  //First we do the PosY Sided Events
  cout <<"Processing PosY Events." <<endl;
  Long64_t nLeftEvents = leftTree->GetEntries();
  //nLeftEvents=1000;
  for (Long64_t eventIndex=0; eventIndex<nLeftEvents; eventIndex++){

    //Load an Event
    leftTree->GetEntry(eventIndex);

    //Fill the NEvents Histogram
    nEventsSideHisto->Fill(0);

    //Loop over the tracks and put them in their nSigmaHistogram
    Int_t nTracks = event->GetTrackArrayEntries();
    for (Int_t trackIndex=0; trackIndex<nTracks; trackIndex++){

      //Load the Track
      track = event->GetTrack(trackIndex);

      PutTrackInNsigmaHisto(track,"LEFT");

    }//End Loop Over Tracks in Event

    event->ResetEvent();

  }//End Loop Over PosY Events


  //Second we do the Right Sided Events
  cout <<"Processing NegY Events." <<endl;
  Long64_t nRightEvents = rightTree->GetEntries();
  //nRightEvents = 1000;
  for (Long64_t eventIndex=0; eventIndex<nRightEvents; eventIndex++){
    
    //Load an Event
    rightTree->GetEntry(eventIndex);

    //Fill the NEvents Histogram
    nEventsSideHisto->Fill(1);

    //Loop over the Tracks and put them in their nSigmaHistogram
    Int_t nTracks = event->GetTrackArrayEntries();
    for (Int_t trackIndex=0; trackIndex<nTracks; trackIndex++){
      
      //Load the Track
      track = event->GetTrack(trackIndex);
      
      PutTrackInNsigmaHisto(track,"RIGHT");

    }//End Loop Over Tracks

    event->ResetEvent();


  }//End Loop Over Right Events

  
  //Save all the Histograms
  cout <<"Saving Histograms." <<endl;
  Save(outFile);

  gROOT->GetListOfFiles()->Remove(outFile);
  outFile->Close();
  inFile->Close();

  cout <<"Finished!" <<endl;

}


//__________________________________________________________________________________
void Save(TFile *outFile){

  nEventsSideHisto->Write();


  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t pIndex=0; pIndex<6; pIndex++){
      for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
	for (Int_t mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

	  //Save the nSigma TPC Histograms
	  outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTPC/");
	  if (nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetEntries() >= 10)
	    nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->Write();

	  //Save the dEdx TPC Histograms
	  outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/dEdxTPC/");
	  if (dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetEntries() >= 10)
	    dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->Write();

	  //Save the z TPC Histograms
	  outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTPC/");
	  if (zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetEntries() >= 10)
	    zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->Write();

	  //Save the nSigma TOF Histograms
	  outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTOF/");
	  if (nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetEntries() >= 10)
	    nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->Write();

	  //Save the invBeta TOF Histograms
	  outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/invBetaTOF/");
	  if (invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetEntries() >= 10)
	    invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->Write();

	  //Save the z TOF Histograms
	  outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTOF/");
	  if (zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetEntries() >= 10)
	    zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->Write();

	  //Save the Pure Sample Histograms
	  if (pIndex==0 || pIndex==2 || pIndex==4){
	    for (Int_t pureIndex=0; pureIndex<10; pureIndex+=2){

	      //Save the nSigma TPC Pure Samples
	      if (nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTPC/PureSamples/"+GetParticleName(pureIndex));
	      nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the dEdx TPC Pure Samples
	      if (dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/dEdxTPC/PureSamples/"+GetParticleName(pureIndex));
	      dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the z TPC Pure Samples
	      if (zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTPC/PureSamples/"+GetParticleName(pureIndex));
	      zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the nSigma TOF Pure Samples
	      if (nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTOF/PureSamples/"+GetParticleName(pureIndex));
	      nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the invBeta TOF Pure Samples
	      if (invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/invBetaTOF/PureSamples/"+GetParticleName(pureIndex));
	      invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the z TOF Pure Samples
	      if (zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTOF/PureSamples/"+GetParticleName(pureIndex));
	      zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();	      

	    }
	  }
	  else {
	    for (Int_t pureIndex=1; pureIndex<10; pureIndex+=2){

	      if (nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTPC/PureSamples/"+GetParticleName(pureIndex));
	      nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the dEdx TPC Pure Samples
	      if (dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/dEdxTPC/PureSamples/"+GetParticleName(pureIndex));
	      dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the z TPC Pure Samples
	      if (zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTPC/PureSamples/"+GetParticleName(pureIndex));
	      zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the nSigma TOF Pure Samples
	      if (nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/nSigmaTOF/PureSamples/"+GetParticleName(pureIndex));
	      nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the invBeta TOF Pure Samples
	      if (invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/invBetaTOF/PureSamples/"+GetParticleName(pureIndex));
	      invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();

	      //Save the z TOF Pure Samples
	      if (zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->GetEntries() < 10)
		continue;
	      outFile->cd(GetSideName(sideIndex)+"/"+GetParticleName(pIndex)+"/zTOF/PureSamples/"+GetParticleName(pureIndex));
	      zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->Write();	      

	    }
	  }
	  //End Save Pure Samples

	}
      }
    }
    cout <<"Finished Saving: " <<GetSideName(sideIndex) <<endl;
  }//End Save All Histograms


}

//__________________________________________________________________________________
void PutTrackInNsigmaHisto(TRACK *track, TString side){

  //Function which places the track in the nSigma Histogram

  Int_t sideIndex = 10;
  if (side == "LEFT")
    sideIndex = 0;
  else if (side == "RIGHT")
    sideIndex = 1;
  else{
    cout <<"ERROR in Function PutTrackInNsigmaHisto: side Index not set!" <<endl;
    exit (EXIT_FAILURE);
  }

  Double_t mTm0Pion,mTm0Kaon,mTm0Proton;  
  Int_t mTm0PionIndex,mTm0KaonIndex,mTm0ProtonIndex;
  Int_t yPionIndex, yKaonIndex, yProtonIndex;

  //Assume the Track is a Pion (PID Index = 0 or 1)
  mTm0Pion = ComputemTm0(track->GetpT(),0);
  mTm0PionIndex = FindmTm0Index(mTm0Pion);
  yPionIndex = FindRapidityIndex(track->GetyPion(),sideIndex);

  //Check the Indecies and fill the histograms
  if (mTm0PionIndex < nmTm0Bins && yPionIndex < nRapidityBins){
    if (track->GetCharge() > 0){
      FillHistogramInRange(nSigmaTPCHisto[sideIndex][0][yPionIndex][mTm0PionIndex],track->GetnSigPion());
      FillHistogramInRange(dEdxTPCHisto[sideIndex][0][yPionIndex][mTm0PionIndex],track->GetdEdx()*pow(10,6));
      FillHistogramInRange(zTPCHisto[sideIndex][0][yPionIndex][mTm0PionIndex],ComputeTPCZ(track,0));
      FillPureSampleHistograms(track,sideIndex,0,yPionIndex,mTm0PionIndex);
      if (track->GetTOFSigPion() > -10){ //This checks to make sure track has TOF info
	FillHistogramInRange(nSigmaTOFHisto[sideIndex][0][yPionIndex][mTm0PionIndex],track->GetTOFSigPion());
	FillHistogramInRange(invBetaTOFHisto[sideIndex][0][yPionIndex][mTm0PionIndex],1.0/track->GetTOFBeta());
	FillHistogramInRange(zTOFHisto[sideIndex][0][yPionIndex][mTm0PionIndex],ComputeTOFZ(track,0));
      }
    }
    else if (track->GetCharge() < 0){
      FillHistogramInRange(nSigmaTPCHisto[sideIndex][1][yPionIndex][mTm0PionIndex],track->GetnSigPion());
      FillHistogramInRange(dEdxTPCHisto[sideIndex][1][yPionIndex][mTm0PionIndex],track->GetdEdx()*pow(10,6));
      FillHistogramInRange(zTPCHisto[sideIndex][1][yPionIndex][mTm0PionIndex],ComputeTPCZ(track,0));
      FillPureSampleHistograms(track,sideIndex,1,yPionIndex,mTm0PionIndex);
      if (track->GetTOFSigPion() > -10){//This checks to make sure track has TOF info
	FillHistogramInRange(nSigmaTOFHisto[sideIndex][1][yPionIndex][mTm0PionIndex],track->GetTOFSigPion());
	FillHistogramInRange(invBetaTOFHisto[sideIndex][1][yPionIndex][mTm0PionIndex],1.0/track->GetTOFBeta());
	FillHistogramInRange(zTOFHisto[sideIndex][1][yPionIndex][mTm0PionIndex],ComputeTOFZ(track,0));
      }
    }
  }
  
  //Assume the Track is a Kaon (PID Index = 2 or 3)
  mTm0Kaon = ComputemTm0(track->GetpT(),2);
  mTm0KaonIndex = FindmTm0Index(mTm0Kaon);
  yKaonIndex = FindRapidityIndex(track->GetyKaon(),sideIndex);

  //Check the Indecies and fill the histograms
  if (mTm0KaonIndex < nmTm0Bins && yKaonIndex < nRapidityBins){
    if (track->GetCharge() > 0){
      FillHistogramInRange(nSigmaTPCHisto[sideIndex][2][yKaonIndex][mTm0KaonIndex],track->GetnSigKaon());
      FillHistogramInRange(dEdxTPCHisto[sideIndex][2][yKaonIndex][mTm0KaonIndex],track->GetdEdx()*pow(10,6));
      FillHistogramInRange(zTPCHisto[sideIndex][2][yKaonIndex][mTm0KaonIndex],ComputeTPCZ(track,2));
      FillPureSampleHistograms(track,sideIndex,2,yKaonIndex,mTm0KaonIndex);
      if (track->GetTOFSigKaon() > -10){//This checks to make sure track has TOF info
	FillHistogramInRange(nSigmaTOFHisto[sideIndex][2][yKaonIndex][mTm0KaonIndex],track->GetTOFSigKaon());
	FillHistogramInRange(invBetaTOFHisto[sideIndex][2][yKaonIndex][mTm0KaonIndex],1.0/track->GetTOFBeta());
	FillHistogramInRange(zTOFHisto[sideIndex][2][yKaonIndex][mTm0KaonIndex],ComputeTOFZ(track,2));
      }
    }
    else if (track->GetCharge() < 0){
      FillHistogramInRange(nSigmaTPCHisto[sideIndex][3][yKaonIndex][mTm0KaonIndex],track->GetnSigKaon());
      FillHistogramInRange(dEdxTPCHisto[sideIndex][3][yKaonIndex][mTm0KaonIndex],track->GetdEdx()*pow(10,6));
      FillHistogramInRange(zTPCHisto[sideIndex][3][yKaonIndex][mTm0KaonIndex],ComputeTPCZ(track,2));
      FillPureSampleHistograms(track,sideIndex,3,yKaonIndex,mTm0KaonIndex);
      if (track->GetTOFSigKaon() > -10){//This checks to make sure track has TOF info
	FillHistogramInRange(nSigmaTOFHisto[sideIndex][3][yKaonIndex][mTm0KaonIndex],track->GetTOFSigKaon());
	FillHistogramInRange(invBetaTOFHisto[sideIndex][3][yKaonIndex][mTm0KaonIndex],1.0/track->GetTOFBeta());
	FillHistogramInRange(zTOFHisto[sideIndex][3][yKaonIndex][mTm0KaonIndex],ComputeTOFZ(track,2));
      }
    }
  }
  
  //Assume the Track is a Proton (PID Index = 4 or 5)
  mTm0Proton = ComputemTm0(track->GetpT(),4);
  mTm0ProtonIndex = FindmTm0Index(mTm0Proton);
  yProtonIndex = FindRapidityIndex(track->GetyProton(),sideIndex);
  
  //Check the Indecies and fill the histograms
  if (mTm0ProtonIndex < nmTm0Bins && yProtonIndex < nRapidityBins){
    if (track->GetCharge() > 0){
      FillHistogramInRange(nSigmaTPCHisto[sideIndex][4][yProtonIndex][mTm0ProtonIndex],track->GetnSigProton());
      FillHistogramInRange(dEdxTPCHisto[sideIndex][4][yProtonIndex][mTm0ProtonIndex],track->GetdEdx()*pow(10,6));
      FillHistogramInRange(zTPCHisto[sideIndex][4][yProtonIndex][mTm0ProtonIndex],ComputeTPCZ(track,4));
      FillPureSampleHistograms(track,sideIndex,4,yProtonIndex,mTm0ProtonIndex);
      if (track->GetTOFSigProton() > -10){//This checks to make sure track has TOF info
	FillHistogramInRange(nSigmaTOFHisto[sideIndex][4][yProtonIndex][mTm0ProtonIndex],track->GetTOFSigProton());
	FillHistogramInRange(invBetaTOFHisto[sideIndex][4][yProtonIndex][mTm0ProtonIndex],1.0/track->GetTOFBeta());
	FillHistogramInRange(zTOFHisto[sideIndex][4][yProtonIndex][mTm0ProtonIndex],ComputeTOFZ(track,2));
      }
    }
    else if (track->GetCharge() < 0){
      FillHistogramInRange(nSigmaTPCHisto[sideIndex][5][yProtonIndex][mTm0ProtonIndex],track->GetnSigProton());
      FillHistogramInRange(dEdxTPCHisto[sideIndex][5][yProtonIndex][mTm0ProtonIndex],track->GetdEdx()*pow(10,6));
      FillHistogramInRange(zTPCHisto[sideIndex][5][yProtonIndex][mTm0ProtonIndex],ComputeTPCZ(track,4));
      FillPureSampleHistograms(track,sideIndex,5,yProtonIndex,mTm0ProtonIndex);
      if (track->GetnSigProton() > -10){//This checks to make sure track has TOF info
	FillHistogramInRange(nSigmaTOFHisto[sideIndex][5][yProtonIndex][mTm0ProtonIndex],track->GetTOFSigProton());
	FillHistogramInRange(invBetaTOFHisto[sideIndex][5][yProtonIndex][mTm0ProtonIndex],1.0/track->GetTOFBeta());
	FillHistogramInRange(zTOFHisto[sideIndex][5][yProtonIndex][mTm0ProtonIndex],ComputeTOFZ(track,2));
      }
    }
  }

}


//________________________________________________________________________________
void FillPureSampleHistograms(TRACK *track, Int_t sideIndex,Int_t pIndex, 
			      Int_t yIndex, Int_t mTm0Index){

  //This Function Fill the pure sample histograms associated with a particle
  //type assumtion for the specified binning.  

  //Set the Charge Index -- chargeIndex == 0 for positive, 1 for Negative
  Int_t chargeIndex = track->GetCharge() > 0 ? 0:1;
  
  //If the Track does not have TOF information or if the TOF information
  //is set to the default value of -999 skip it
  if (track->GetTOFBeta() < -100)
    return;

  //Get TPC nSigma for the current Particle Type (pIndex)
  Double_t nSigmaTPC = -999;
  if (pIndex == 0 || pIndex == 1)
    nSigmaTPC = track->GetnSigPion();
  else if (pIndex == 2 || pIndex == 3)
    nSigmaTPC = track->GetnSigKaon();
  else if (pIndex == 4 || pIndex == 5)
    nSigmaTPC = track->GetnSigProton();

  //Get the TOF nSigma for the current Particle Type (pIndex)
  Double_t nSigmaTOF = -999;
  if (pIndex == 0 || pIndex == 1)
    nSigmaTOF = track->GetTOFSigPion();
  else if (pIndex == 2 || pIndex == 3)
    nSigmaTOF = track->GetTOFSigKaon();
  else if (pIndex == 4 || pIndex == 5)
    nSigmaTOF = track->GetTOFSigProton();

  //Is the Track a Pion According to the TOF?
  if (fabs(track->GetTOFSigPion()) < .8){
    FillHistogramInRange(nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][0+chargeIndex],nSigmaTPC);
    FillHistogramInRange(dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][0+chargeIndex],track->GetdEdx()*pow(10,2));
    FillHistogramInRange(zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][0+chargeIndex],ComputeTPCZ(track,pIndex));
    FillHistogramInRange(nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][0+chargeIndex],nSigmaTOF);
    FillHistogramInRange(invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][0+chargeIndex],1.0/track->GetTOFBeta());
    FillHistogramInRange(zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][0+chargeIndex],ComputeTOFZ(track,pIndex));
  }

  //Is the Track a Kaon According to the TOF?
  if (fabs(track->GetTOFSigKaon()) < .8){
    FillHistogramInRange(nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][2+chargeIndex],nSigmaTPC);
    FillHistogramInRange(dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][2+chargeIndex],track->GetdEdx()*pow(10,2));
    FillHistogramInRange(zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][2+chargeIndex],ComputeTPCZ(track,pIndex));
    FillHistogramInRange(nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][2+chargeIndex],nSigmaTOF);
    FillHistogramInRange(invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][2+chargeIndex],1.0/track->GetTOFBeta());
    FillHistogramInRange(zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][2+chargeIndex],ComputeTOFZ(track,pIndex));
  }

  //Is the Track a Proton According to the TOF?
  if (fabs(track->GetTOFSigProton()) < .8){
    FillHistogramInRange(nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][4+chargeIndex],nSigmaTPC);
    FillHistogramInRange(dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][4+chargeIndex],track->GetdEdx()*pow(10,2));
    FillHistogramInRange(zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][4+chargeIndex],ComputeTPCZ(track,pIndex));
    FillHistogramInRange(nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][4+chargeIndex],nSigmaTOF);
    FillHistogramInRange(invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][4+chargeIndex],1.0/track->GetTOFBeta());
    FillHistogramInRange(zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][4+chargeIndex],ComputeTOFZ(track,pIndex));
  }

  //Is the Track an Electron According to the TOF
  if (fabs(track->GetTOFSigElectron()) < .8){
    FillHistogramInRange(nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][6+chargeIndex],nSigmaTPC);
    FillHistogramInRange(dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][6+chargeIndex],track->GetdEdx()*pow(10,2));
    FillHistogramInRange(zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][6+chargeIndex],ComputeTPCZ(track,pIndex));
    FillHistogramInRange(nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][6+chargeIndex],nSigmaTOF);
    FillHistogramInRange(invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][6+chargeIndex],1.0/track->GetTOFBeta());
    FillHistogramInRange(zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][6+chargeIndex],ComputeTOFZ(track,pIndex));
  }

  //Is the Track a Deuteron According to the TOF
  //We don't have nSigmaTOF info for the deuteron, so instead we compute the Z variable
  if (fabs(ComputeTOFZ(track,8)) < 2.0){
    FillHistogramInRange(nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][8+chargeIndex],nSigmaTPC);
    FillHistogramInRange(dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][8+chargeIndex],track->GetdEdx()*pow(10,2));
    FillHistogramInRange(zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][8+chargeIndex],ComputeTPCZ(track,pIndex));
    FillHistogramInRange(nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][8+chargeIndex],nSigmaTOF);
    FillHistogramInRange(invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][8+chargeIndex],1.0/track->GetTOFBeta());
    FillHistogramInRange(zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][8+chargeIndex],ComputeTOFZ(track,pIndex));
    }

}



//________________________________________________________________________________
void DefineHistograms(Double_t ENERGY){

  //This Function is used to define all the histograms used in the 
  //main part of this macro

  cout <<"Starting to Define Histograms" <<endl;

  TString name, title;

  //Create the nSigmaHistograms
  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t pIndex=0; pIndex<6; pIndex++){
      for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
	for (Int_t mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
	  
	  // --- Set the Kinematic String used in the Histo Titles
	  TString kinematicString = 
	    TString::Format(" [%.2f < y < %.2f, %.3f < m_{T}-m_{0} < %.3f (GeV)]",
			    GetRapidityRangeLow(yIndex,sideIndex),GetRapidityRangeHigh(yIndex,sideIndex),
			    GetmTm0RangeLow(mTm0Index),GetmTm0RangeHigh(mTm0Index));


	  // --- The TPC nSigma Histograms
	  //------------------------------------
	  name = GetParticleName(pIndex)+"_nSigma_TPC_"+GetSideName(sideIndex)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index);
	  
	  title = TString::Format("n#sigma_{%s} TPC %s Au_{Like}+Al at #sqrt{s_{NN}} = %.1f ",
				  GetParticleSymbol(pIndex).Data(),
				  GetSideName(sideIndex).Data(),
				  ENERGY);	  
	  title += kinematicString;
	  
	  nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index] = 
	    new TH1F(name,title,300,-25,50);
	  
	  nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetXaxis()->
	    SetTitle(Form("n#sigma_{%s} (TPC)",GetParticleSymbol(pIndex).Data()));


	  // --- The TPC dEdx Histograms
	  //--------------------------------------
	  name = GetParticleName(pIndex)+"_dEdx_TPC_"+GetSideName(sideIndex)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index);
	  
	  title = TString::Format("dE/dx_{%s} TPC %s Au_{Like}+Al at #sqrt{s_{NN}} = %.1f ",
				  GetParticleSymbol(pIndex).Data(),
				  GetSideName(sideIndex).Data(),
				  ENERGY);	  
	  title += kinematicString;
	  
	  dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index] = 
	    new TH1F(name,title,300,-1,50);
	  
	  dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetXaxis()->
	    SetTitle(Form("#frac{dE}{dx}_{%s} (TPC)",GetParticleSymbol(pIndex).Data()));


	  // --- The TPC Z Histograms
	  //---------------------------------------
	  name = GetParticleName(pIndex)+"_z_TPC_"+GetSideName(sideIndex)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index);
	  
	  title = TString::Format("Z_{%s} TPC %s Au_{Like}+Al at #sqrt{s_{NN}} = %.1f ",
				  GetParticleSymbol(pIndex).Data(),
				  GetSideName(sideIndex).Data(),
				  ENERGY);	  
	  title += kinematicString;
	  
	  zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index] = 
	    new TH1F(name,title,300,-2.5,4);
	  
	  zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetXaxis()->
	    SetTitle(Form("Z_{%s} (TPC)",GetParticleSymbol(pIndex).Data()));
	  

	  // --- The TOF nSigma Histograms
	  //----------------------------------------
	  name = GetParticleName(pIndex)+"_nSigma_TOF_"+GetSideName(sideIndex)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index);
	  
	  title = TString::Format("n#sigma_{%s} TOF %s Au_{Like}+Al at #sqrt{s_{NN}} = %.1f ",
				  GetParticleSymbol(pIndex).Data(),
				  GetSideName(sideIndex).Data(),
				  ENERGY);	  
	  title += kinematicString;
	  
	  nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]
	    = new TH1F(name,title,300,-10,50);
	  
	  nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetXaxis()->
	    SetTitle(Form("n#sigma_{%s} (TOF)",GetParticleSymbol(pIndex).Data()));


	  // --- The TOF invBeta Histograms
	  //----------------------------------------
	  name = GetParticleName(pIndex)+"_invBeta_TOF_"+GetSideName(sideIndex)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index);
	  
	  title = TString::Format("1/#beta_{%s} TOF %s Au_{Like}+Al at #sqrt{s_{NN}} = %.1f ",
				  GetParticleSymbol(pIndex).Data(),
				  GetSideName(sideIndex).Data(),
				  ENERGY);	  
	  title += kinematicString;
	  
	  invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]
	    = new TH1F(name,title,300,.5,2);
	  
	  invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetXaxis()->
	    SetTitle(Form("#frac{1}{#beta}_{%s} (TOF)",GetParticleSymbol(pIndex).Data()));

	  //The TOF Z Histograms
	  //----------------------------------------
	  name = GetParticleName(pIndex)+"_z_TOF_"+GetSideName(sideIndex)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index);
	  
	  title = TString::Format("Z_{%s} TOF %s Au_{Like}+Al at #sqrt{s_{NN}} = %.1f ",
				  GetParticleSymbol(pIndex).Data(),
				  GetSideName(sideIndex).Data(),
				  ENERGY);	  
	  title += kinematicString;
	  
	  zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]
	    = new TH1F(name,title,300,-.3,.4);
	  
	  zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetXaxis()->
	    SetTitle(Form("Z_{%s} (TOF)",GetParticleSymbol(pIndex).Data()));


	  //------------------------------------------------------------------
	  // --- Now The Pure Sample Histograms ---
	  //------------------------------------------------------------------
	  for (Int_t pureIndex=0; pureIndex<10; pureIndex++){
	    

	    // --- The nSigma TPC Pure Sample
	    name = nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetName();
	    name += TString::Format("_Pure%s",GetParticleName(pureIndex).Data());

	    title = nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetTitle();
	    title += TString::Format(" Pure %s Sample",GetParticleSymbol(pureIndex).Data());

	    nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex] = 
	      new TH1F(name,title,300,-40,50);

	    nSigmaTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->
	      GetXaxis()->SetTitle(nSigmaTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->
				   GetXaxis()->GetTitle());


	    // --- The dEdx TPC Pure Sample
	    name = dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetName();
	    name += TString::Format("_Pure%s",GetParticleName(pureIndex).Data());

	    title = dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetTitle();
	    title += TString::Format(" Pure %s Sample",GetParticleSymbol(pureIndex).Data());

	    dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex] = 
	      new TH1F(name,title,300,-1,50);

	    dEdxTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->
	      GetXaxis()->SetTitle(dEdxTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->
				   GetXaxis()->GetTitle());


	    // --- The Z TPC Pure Sample
	    name = zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetName();
	    name += TString::Format("_Pure%s",GetParticleName(pureIndex).Data());

	    title = zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetTitle();
	    title += TString::Format(" Pure %s Sample",GetParticleSymbol(pureIndex).Data());

	    zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex] = 
	      new TH1F(name,title,300,-2.5,4);

	    zTPCPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->
	      GetXaxis()->SetTitle(zTPCHisto[sideIndex][pIndex][yIndex][mTm0Index]->
				   GetXaxis()->GetTitle());


	    // --- The nSigma TOF Pure Sample
	    name = nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetName();
	    name += TString::Format("_Pure%s",GetParticleName(pureIndex).Data());

	    title = nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetTitle();
	    title += TString::Format(" Pure %s Sample",GetParticleSymbol(pureIndex).Data());

	    nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex] = 
	      new TH1F(name,title,300,-10,50);

	    nSigmaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->
	      GetXaxis()->SetTitle(nSigmaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->
				   GetXaxis()->GetTitle());


	    // --- The invBeta TOF Pure Sample
	    name = invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetName();
	    name += TString::Format("_Pure%s",GetParticleName(pureIndex).Data());

	    title = invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetTitle();
	    title += TString::Format(" Pure %s Sample",GetParticleSymbol(pureIndex).Data());
	 
	    invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex] = 
	      new TH1F(name,title,300,.5,2);

	    invBetaTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->
	      GetXaxis()->SetTitle(invBetaTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->
				   GetXaxis()->GetTitle());


   	    // --- The Z TOF Pure Sample
	    name = zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetName();
	    name += TString::Format("_Pure%s",GetParticleName(pureIndex).Data());

	    title = zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->GetTitle();
	    title += TString::Format(" Pure %s Sample",GetParticleSymbol(pureIndex).Data());
	    
	    zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex] = 
	      new TH1F(name,title,300,-.3,.4);

	    zTOFPureSampleHisto[sideIndex][pIndex][yIndex][mTm0Index][pureIndex]->
	      GetXaxis()->SetTitle(zTOFHisto[sideIndex][pIndex][yIndex][mTm0Index]->
				   GetXaxis()->GetTitle());


	  }//End Make Pure Samples

	}
      }
    }
  }//End Create nSigma Histograms

  //Create the number of Events per Side Histogram
  nEventsSideHisto = new TH1F("nEventsSide","Number of Events on Each Side",2,-.5,1.5);
  
  cout <<"Finished Defining Histograms." <<endl;

}


//____________________________________________________________________________
void FillHistogramInRange(TH1F *Histo, Double_t Value){

  //This Function fill Histo with Value only if Value is 
  //between the min and max range of the histogram

  //*** This Assumes equal Bin Withds!

  Int_t nBins = Histo->GetNbinsX();
  Float_t binWidth = Histo->GetBinWidth(1);

  Double_t minRange = Histo->GetBinLowEdge(1);
  Double_t maxRange = Histo->GetBinLowEdge(nBins)+binWidth;

  if (Value >= minRange && Value <= maxRange)
    Histo->Fill(Value);

}

//_____________________________________________________________________________________
void LoadPIDFunctions(TFile *pidFile){

  TString name[nParticles] =
    {"Pion", "Pion", "Kaon", "Kaon", "Proton",
     "Proton", "Electron", "Electron", "Deuteron", "Deuteron"};

  for (Int_t i=0; i<nParticles; i++){
    tpcPIDFunc[i] = (TF1*)pidFile->Get(name[i]);
    tofPIDFunc[i] = (TF1*)pidFile->Get("TOF"+name[i]);
  }

}

//_____________________________________________________________________
Double_t ComputeTPCZ(TRACK *tempTrack, Int_t particleID){

  Double_t measdEdx = tempTrack->GetdEdx()*pow(10,6);
  Double_t trackP = sqrt(pow(tempTrack->GetpT(),2)+pow(tempTrack->GetpZ(),2));
  Double_t expecteddEdx = tpcPIDFunc[particleID]->Eval(trackP);

  return log(measdEdx/expecteddEdx);
}

//_____________________________________________________________________
Double_t ComputeTOFZ(TRACK *tempTrack, Int_t particleID){

  Double_t measInvBeta = 1.0/tempTrack->GetTOFBeta();
  Double_t trackP = sqrt(pow(tempTrack->GetpT(),2)+pow(tempTrack->GetpZ(),2));
  Double_t expectedInvBeta = tofPIDFunc[particleID]->Eval(trackP);

  return log(measInvBeta/expectedInvBeta);
}
