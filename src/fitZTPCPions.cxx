/*****************************************************************
 **
 **
 **
 **
 **
 **
 **
 *****************************************************************/


#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"
#include "../inc/fitZTPCUtilities.h"

//Save?
Bool_t SAVEOBJ = true;
Bool_t SAVEIMG = true;

// --- Functions
void LoadZPlots(TFile *inFile, Int_t SIDE);
void FitPionMinus(Int_t PID);
void FitPionPlus(Int_t PID);
Double_t FindMaxBinCenter(TH1F *h, Double_t min, Double_t max);
Double_t FindMinBinCenter(TH1F *h, Double_t min, Double_t max);

//Generate the Spectra
void DefineSpectra(Double_t ENERGY, Int_t SIDE);
Bool_t MakeSpectra(TH1F *fitHisto, TF1 *fitFunc, Int_t yIndex, Int_t mTm0Index, 
		   Double_t ENERGY, Int_t PID, Int_t SIDE,TString IMAGEDIR="");

// --- Global Variables ---
Int_t rapidityIndex;
Bool_t killElectron;

//Canvases
TCanvas *fittingCanvas;
TCanvas *fitParCanvas;
TCanvas *spectraCanvas;

//Particle Parameterizations
ParticlePars *pion, *electron, *proton;

//Histograms for the TPC Z Variable
TH1F *tpcPionMinusHisto[nRapidityBins][nmTm0Bins];
TH1F *tpcPionPlusHisto[nRapidityBins][nmTm0Bins];

//Fit Functions
TF1 *tpcPionMinusFit[nRapidityBins][nmTm0Bins];
TF1 *tpcPionPlusFit[nRapidityBins][nmTm0Bins];

//The Histo with the number of events on each side
TH1F *nEventsHisto;

//Spectra Graphs
TGraphErrors *rawSpectra[nRapidityBins][2];//[PionPlus,PionMinus]
Int_t spectraPoint[nRapidityBins][2];


// MAIN _______________________________________________________________________
void fitZTPCPions(TString ZTPCFILE, TString SPECTRAFILE,
		  Double_t ENERGY, Int_t SIDE, Int_t RAPIDITYINDEX=-1, TString IMAGEDIR=""){

  //If the ImageDir argument is empty then turn off saving images
  if (!IMAGEDIR.CompareTo("") && SAVEIMG == true)
    SAVEIMG = false;

  //If we are going to save images then create subdirectories
  if (SAVEIMG && !gSystem->OpenDirectory(IMAGEDIR+"/PionPlus/"+GetSideName(SIDE))){
    gSystem->mkdir(IMAGEDIR+"/PionPlus/");
    gSystem->mkdir(IMAGEDIR+"/PionPlus/"+GetSideName(SIDE));
  }
  if (SAVEIMG && !gSystem->OpenDirectory(IMAGEDIR+"PionMinus/"+GetSideName(SIDE))){
    gSystem->mkdir(IMAGEDIR+"/PionMinus/");
    gSystem->mkdir(IMAGEDIR+"/PionMinus/"+GetSideName(SIDE));
  }

  //Pass the rapidity index value to the global
  rapidityIndex = RAPIDITYINDEX;

  //Get the Particle Yield File and Load the Histograms
  TFile *inFile = new TFile(ZTPCFILE,"READ");
  LoadZPlots(inFile,SIDE);
  
  //Load the PID Functions
  TFile *pidFile = new TFile("../src/PIDFunctions.root","READ");
  LoadPIDFunctions(pidFile);
  
  //Create the Output File
  TFile *outFile = new TFile(SPECTRAFILE,"UPDATE");

  //Create the Output File Directory Structure

  if (SAVEOBJ){
    outFile->mkdir(GetSideName(SIDE)+"/zTPC_Fit_Histos/"+GetParticleName(0)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/zTPC_Fit_Curves/"+GetParticleName(0)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/RawSpectra/"+GetParticleName(0)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/zTPC_Fit_Histos/"+GetParticleName(1)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/zTPC_Fit_Curves/"+GetParticleName(1)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/RawSpectra/"+GetParticleName(1)+"/");
    outFile->mkdir(GetSideName(SIDE)+"/FitParameterizations/");
  }

  //Set Style Options
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(112);

  //Create the Global Canvases
  fittingCanvas = new TCanvas("fittingCanvas","Fitting Canvas",20,20,800,600);
  fittingCanvas->SetLogy();
  fitParCanvas = new TCanvas("fitParCanvas","Fit Parameter Canvas",20,20,1100,1100);
  fitParCanvas->Divide(3,3);
  spectraCanvas = new TCanvas("spectraCanvas","Spectra Canvas",20,850,800,600);
  spectraCanvas->SetLogy();

  //Make the Spectra
  DefineSpectra(ENERGY,SIDE);

  //Create the Particle parameterization Structs
  pion = new ParticlePars();
  electron = new ParticlePars();
  proton = new ParticlePars();

  DefineParticlePars(pion,"pion");
  DefineParticlePars(electron,"electron");
  DefineParticlePars(proton,"proton");

  //Kill the electron?
  killElectron = false;

  //Fit the Pion Minuses
  FitPionMinus(1);

  //Make the Pion Minus Spectra
  for (Int_t yIndex=5; yIndex<nRapidityBins-3; yIndex++){

    //If the user has specified a particular rapidity index and
    //if this yIndex is not it, then skip it.
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;
    
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){  
      Bool_t success = MakeSpectra(tpcPionMinusHisto[yIndex][mTm0Index],
				   tpcPionMinusFit[yIndex][mTm0Index],
				   yIndex,mTm0Index,ENERGY,1,SIDE,IMAGEDIR);

      if (success && SAVEOBJ){
	//Save the zTPC Fit Histo
	outFile->cd(GetSideName(SIDE)+"/zTPC_Fit_Histos/"+GetParticleName(1)+"/");
	tpcPionMinusHisto[yIndex][mTm0Index]->Write();
	
	//Save the zTPC Fit Curve
	outFile->cd(GetSideName(SIDE)+"/zTPC_Fit_Curves/"+GetParticleName(1)+"/");
	tpcPionMinusFit[yIndex][mTm0Index]->Write();	
      }
      
    }//End Loop Over mTm0Index

    //Save the Parameterizations
    if (SAVEOBJ){
      outFile->cd(GetSideName(SIDE)+"/FitParameterizations/");
      pion->width[yIndex]->Write();
      pion->widthFit[yIndex]->Write();
      electron->mean[yIndex]->Write();
      electron->meanFit[yIndex]->Write();
      electron->width[yIndex]->Write();
      electron->widthFit[yIndex]->Write();
      electron->amp[yIndex]->Write();
      electron->ampFit[yIndex]->Write();
    }
    
    //Save the Spectra
    if (rawSpectra[yIndex][1]->GetN() > 3 && SAVEOBJ){
      outFile->cd(GetSideName(SIDE)+"/RawSpectra/"+GetParticleName(1));
      rawSpectra[yIndex][1]->Write();
    }
    //gSystem->Sleep(2000);
    
  }//End Loop Over yIndex

  //Fit the Pion Pluses
  FitPionPlus(0);

  //Make the Pion Plus Spectra
  for (Int_t yIndex=5; yIndex<nRapidityBins-3; yIndex++){

    //If the user has specified a particular rapidity index and 
    //if this yIndex is not it, then skip it.
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;

    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){  
      Bool_t success = MakeSpectra(tpcPionPlusHisto[yIndex][mTm0Index],
				   tpcPionPlusFit[yIndex][mTm0Index],
				   yIndex,mTm0Index,ENERGY,0,SIDE,IMAGEDIR);      

      if (success && SAVEOBJ){
	//Save the zTPC Fit Histo
	outFile->cd(GetSideName(SIDE)+"/zTPC_Fit_Histos/"+GetParticleName(0)+"/");
	tpcPionPlusHisto[yIndex][mTm0Index]->Write();
	
	//Save the zTPC Fit Curve
	outFile->cd(GetSideName(SIDE)+"/zTPC_Fit_Curves/"+GetParticleName(0)+"/");
	tpcPionPlusFit[yIndex][mTm0Index]->Write();
      }

    }//End Loop Over mTm0Index

    //Save the Parameterizations
    if (SAVEOBJ){
      outFile->cd(GetSideName(SIDE)+"/FitParameterizations/");
      proton->width[yIndex]->Write();
      proton->widthFit[yIndex]->Write();
      proton->mean[yIndex]->Write();
      proton->meanFit[yIndex]->Write();
    }

    //Save the Spectra
    if (rawSpectra[yIndex][0]->GetN() > 3 && SAVEOBJ){
      outFile->cd(GetSideName(SIDE)+"/RawSpectra/"+GetParticleName(0));
      rawSpectra[yIndex][0]->Write();
    }
    //gSystem->Sleep(2000);

  }//End Loop Over yIndex

}


//__________________________________________________________________
void FitPionMinus(Int_t PID){

  for (Int_t yIndex=5; yIndex<nRapidityBins-3; yIndex++){

    /********************
     **ROUND 1: In round 1 we fit a single gaussian in a short range
     **   to capture the mean and width of the pion minus
     **
     **NOTE: The pion mean is a random fluctuation around zero so it
     **      is never parameterized.  The pion width on the other hand
     **      is not a random process so it is parameterized
     ********************/
    
    //If the user has specified a particular rapidity index and
    //if this yIndex is not it, then skip it
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;

    cout <<"Working on Rapidity Index: "  <<yIndex <<endl;

    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){
  
      //Make Sure the Histogram Exists
      if (!tpcPionMinusHisto[yIndex][mTm0Index])
	continue;

     //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->Draw("E");

      //Create the Fit
      tpcPionMinusFit[yIndex][mTm0Index] = new TF1("","gaus(0)",-.2,.2);

      //Set the Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameters(100,0,.08);
      if (tpcPionMinusFit[yIndex][mTm0Index-1])
	tpcPionMinusFit[yIndex][mTm0Index]->
	  SetParameters(tpcPionMinusFit[yIndex][mTm0Index-1]->GetParameter(0),
			tpcPionMinusFit[yIndex][mTm0Index-1]->GetParameter(1),
			tpcPionMinusFit[yIndex][mTm0Index-1]->GetParameter(2));
      
      //Parameter Limits
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(0,0,10000);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(2,.02,.15);

      //Fit the Histogram
      tpcPionMinusHisto[yIndex][mTm0Index]->Fit(tpcPionMinusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();

      //Make Sure there are greater than 10 Degrees of Freedom in the Fit.
      //If NDF < 10 it usually means there is insufficient statistics to
      //do a good fit
      if (tpcPionMinusFit[yIndex][mTm0Index]->GetNDF() < 10)
	break;

      //Mean: Add Point to the Parameter Graphs
      AddPointToParGraphmTm0(pion->mean[yIndex],&pion->meanPoint[yIndex],
			     tpcPionMinusFit[yIndex][mTm0Index],1,mTm0Index);
      AddPointToParGraphY(pion->meanY[mTm0Index],&pion->meanYPoint[mTm0Index],
      			  tpcPionMinusFit[yIndex][mTm0Index],1,yIndex);

      //Width: Add Points to the Parameter Graphs
      AddPointToParGraphmTm0(pion->width[yIndex],&pion->widthPoint[yIndex],
			     tpcPionMinusFit[yIndex][mTm0Index],2,mTm0Index);
      AddPointToParGraphY(pion->widthY[mTm0Index],&pion->widthYPoint[mTm0Index],
      			  tpcPionMinusFit[yIndex][mTm0Index],2,yIndex);

      //Draw the Parameter Graphs
      fitParCanvas->cd(2);
      pion->mean[yIndex]->Draw("APZ");
      //fitParCanvas->cd(2);
      //pion->meanY[mTm0Index]->Draw("APZ");
      fitParCanvas->cd(3);
      pion->width[yIndex]->Draw("APZ");
      //fitParCanvas->cd(4);
      //pion->widthY[mTm0Index]->Draw("APZ");

      fitParCanvas->Update();
      
    }//End of Loop Over mTm0Index for Round 1

    //Fit the Pion Width As a Function of mTm0
    pion->widthFit[yIndex] = new TF1(Form("pionMinus_WidthFit_%02d",yIndex),
				     SwooshFunc,.05,1.0,4);
    pion->widthFit[yIndex]->SetParameters(.2,.5,.3,.1);
    pion->widthFit[yIndex]->SetParLimits(3,.0,3);
    
    //Get the Maximum mTm0 from the graph and use it as a bound on the parameter
    Double_t mTm0Max = pion->width[yIndex]->GetX()[pion->width[yIndex]->GetN()-1];
    pion->widthFit[yIndex]->SetParLimits(2,.1,mTm0Max);

    fitParCanvas->cd(3);
    pion->width[yIndex]->Fit(pion->widthFit[yIndex],"RQ");
    
    fitParCanvas->Update();

    cout <<"Finished Parameterizing the Pion Width!" <<endl;

  /********************
   **ROUND 2: In round 2 we use the fit to the pion width above to fix the pion 
   **         width. The pion mean is fixed to the previous value. Here we also 
   **         fit the Yield Histogram with a two gaus fit to catch the electrons.
   **         The electron mean is constained to be within 10% of its predicted
   **         value.  The electron mean is then fit.
   **
   **NOTE:    The Error on the pion mean is irrelevant since it does not 
   **         enter into the calculation of the yield.
   **
   ********************/
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //Make Sure the Histogram Exists
      if (!tpcPionMinusHisto[yIndex][mTm0Index] || !tpcPionMinusFit[yIndex][mTm0Index])
	continue;
      
     //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->Draw("E");

      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Previous Pion Mean Value
      //      Double_t pionMeanPrev = tpcPionMinusFit[yIndex][mTm0Index]->
      //GetParameter(1);
      Double_t pionMeanPrev = pion->mean[yIndex]->Eval(mTm0);

      //Create the Fit
      if (tpcPionMinusFit[yIndex][mTm0Index])
	tpcPionMinusFit[yIndex][mTm0Index]->Delete();
      tpcPionMinusFit[yIndex][mTm0Index] = new TF1("","gaus(0)+gaus(3)",-1,2);

      //Pion Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(1,pionMeanPrev);
      tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(2,pion->widthFit[yIndex]->
						       Eval(GetmTm0RangeCenter(mTm0Index)));

      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(0,1,10000);

      //Electron Mean Guess
      Double_t eZGuess = ComputeTPCZ(0,6,yIndex,mTm0Index);

      //Electron Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(3,tpcPionMinusFit[yIndex][mTm0Index-1] ?
						       tpcPionMinusFit[yIndex][mTm0Index-1]->
						       GetParameter(3) : 100);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(4,eZGuess);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(5,.08);

      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(3,0,500);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(4,eZGuess-eZGuess*.1,
						       eZGuess+eZGuess*.1);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(5,.02,.15);

      //The electron Amplitude should always decrease after mTm0 = .2
      if (GetmTm0RangeCenter(mTm0Index) >= .2){
	tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(3,0,
							 tpcPionMinusFit[yIndex][mTm0Index-1]->
							 GetParameter(3));
      }

      //Fit the Yield Histogram
      tpcPionMinusHisto[yIndex][mTm0Index]->Fit(tpcPionMinusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();

      //Add the Points to the Fit Par Graphs until statistics run out
      Int_t eMeanBin = tpcPionMinusHisto[yIndex][mTm0Index]->
	FindBin(tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(4));
      Double_t eBinContent = tpcPionMinusHisto[yIndex][mTm0Index]->Integral(eMeanBin-2,eMeanBin+2);
      if (eBinContent < 10)
	break;
      
      AddPointToParGraphmTm0(electron->mean[yIndex], &electron->meanPoint[yIndex],
			     tpcPionMinusFit[yIndex][mTm0Index],4,mTm0Index);


      //Draw the Parameter Graphs
      fitParCanvas->cd(5);
      electron->mean[yIndex]->Draw("APZ");
      fitParCanvas->Update();

      //gSystem->Sleep(2000);
      

    }//End Loop Over mTm0Index for Round 2

    //Get the Number of Points in the electron mean graph and decide if 
    //it is going to be killed or not
    Int_t nElectronMeanPoints = electron->mean[yIndex]->GetN();
    if (nElectronMeanPoints < 5){
      killElectron = true;
      cout <<"The electron has been killed due to too few points in the mean parameterization step" <<endl;
    }

    //Create the Fit for the Electron Mean if there are enough points
    if (!killElectron){
      electron->meanFit[yIndex] = new TF1(Form("electronMinus_MeanFit_%02d",yIndex),
					  ConfoundMeanFit,.03,1.0,5);
      electron->meanFit[yIndex]->FixParameter(0,0);
      electron->meanFit[yIndex]->FixParameter(1,6);
      electron->meanFit[yIndex]->FixParameter(2,yIndex);
      electron->meanFit[yIndex]->SetParameter(3,0.0);
      electron->meanFit[yIndex]->SetParameter(4,1.0);
      electron->meanFit[yIndex]->SetParLimits(4,.5,1.5);
      
      //Fit the Electron Mean
      fitParCanvas->cd(5);
      electron->mean[yIndex]->Fit(electron->meanFit[yIndex],"RQ");
      fitParCanvas->Update();
    }
    else
      electron->meanFit[yIndex] = NULL;

    //gSystem->Sleep(2000);
    cout <<"Finished Parameterizing the Electron Mean!" <<endl;
    
    /********************
     **ROUND 3: The pion mean and width continue to be fixed as in round 2.
     **         The electron mean is now fixed from the parameterization in
     **         round 2.  The electron width is now parameterized.
     **          
     **
     **NOTE:    
     **
     ********************/
    
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){
      
      //Make Sure the Histogram Exists
      if (!tpcPionMinusHisto[yIndex][mTm0Index] || !tpcPionMinusFit[yIndex][mTm0Index])
	continue;
      
      //Make Sure the electron hasn't already been killed
      if (killElectron){
	cout <<"The electron has been killed. Skipping parameterizing its width." <<endl;
	break;
      }

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->Draw("E");

      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Previous Pion Mean Value
      Double_t pionMeanPrev = pion->mean[yIndex]->Eval(mTm0);
      
      //Create the Fit
      if (tpcPionMinusFit[yIndex][mTm0Index])
	tpcPionMinusFit[yIndex][mTm0Index]->Delete();
      tpcPionMinusFit[yIndex][mTm0Index] = new TF1("","gaus(0)+gaus(3)",-1,2);
      
      //Pion Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(1,pionMeanPrev);
      tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(2,pion->widthFit[yIndex]->
						       Eval(GetmTm0RangeCenter(mTm0Index)));
      
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(0,1,1000000);
      
      //Electron Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->
	SetParameter(3,tpcPionMinusHisto[yIndex][mTm0Index]->
		     GetBinContent(tpcPionMinusHisto[yIndex][mTm0Index]->
				   FindBin(electron->meanFit[yIndex]->Eval(mTm0))));
      tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(4,electron->meanFit[yIndex]->
						       Eval(GetmTm0RangeCenter(mTm0Index)));
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(5,.08);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(5,.05,.1);
      
      //The Electron Parameter Limits
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(3,0,150);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(5,.05,.1);
      
      //The electron Amplitude should always decrease after mTm0 = .2
      if (GetmTm0RangeCenter(mTm0Index) >= .2){
	tpcPionMinusFit[yIndex][mTm0Index]->SetParLimits(3,0,
							 tpcPionMinusFit[yIndex][mTm0Index]->
							 GetParameter(3)*1.05);
      }
        
      //Fit the Yield Histogram
      tpcPionMinusHisto[yIndex][mTm0Index]->Fit(tpcPionMinusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();
      
      //Add the Points to the Fit Par Graphs until statistics run out
      Int_t eMeanBin = tpcPionMinusHisto[yIndex][mTm0Index]->
        FindBin(tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(4));
      Double_t eBinContent = tpcPionMinusHisto[yIndex][mTm0Index]->Integral(eMeanBin-2,eMeanBin+2);
      if (eBinContent < 10)
	break;
      
      //Add Point to the electron Width Graph
      AddPointToParGraphmTm0(electron->width[yIndex], &electron->widthPoint[yIndex],
			     tpcPionMinusFit[yIndex][mTm0Index], 5, mTm0Index);
      
      fitParCanvas->cd(6);
      electron->width[yIndex]->Draw("APZ");
      fitParCanvas->Update();
      
    }//End loop over mTm0Index for Round 3

    //Get the number of points in the electron graph and kill the electron if there isn't enough
    Int_t nElectronWidthPoints = electron->width[yIndex]->GetN();
    if (nElectronWidthPoints < 5 && !killElectron){
      killElectron = true;
      cout <<"The electron has been killed due to too few points in the width parameterization step." <<endl;
    }

    //Define the Fit for the electron Width
    if (!killElectron){
      electron->widthFit[yIndex] = new TF1(Form("electronMinus_WidthFit_%02d",yIndex),
					   "pol0",0.0,1.0);
      
      //Fit the Electron Width
      fitParCanvas->cd(6);
      electron->width[yIndex]->Fit(electron->widthFit[yIndex],"RQ");
      fitParCanvas->Update();
    
    }
    else 
      electron->widthFit[yIndex] = NULL;

    cout <<"Finished Parameterizing the the Electron Width!" <<endl;

    /********************
     **ROUND 4: In this round we parameterize the electron amplitude.
     **         The pion mean and width and the electron mean and
     **         width are all fixed.
     **         
     **          
     **
     **NOTE:    
     **
     ********************/

    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //Make Sure the Histogram Exists
      if (!tpcPionMinusHisto[yIndex][mTm0Index] || !tpcPionMinusFit[yIndex][mTm0Index])
	continue;
      
      //Check to make sure the electron hasn't already been killed
      if (killElectron){
	cout <<"The electron has already been killed. Skipping parameterizing its amplitude." <<endl;
	break;
      }
      
      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->Draw("E");

      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Previous Pion Mean Value
      Double_t pionMeanPrev = pion->mean[yIndex]->Eval(mTm0);

      //Create Fit
      if (tpcPionMinusFit[yIndex][mTm0Index])
	tpcPionMinusFit[yIndex][mTm0Index]->Delete();
      tpcPionMinusFit[yIndex][mTm0Index] = new TF1(Form("TPCPionMinusFit_%02d_%02d",yIndex,mTm0Index),
						   "gaus(0)+gaus(3)",-1,2);

     //Set the Parameter Names
      tpcPionMinusFit[yIndex][mTm0Index]->SetParNames(Form("%s_{Amp.}",GetParticleSymbol(PID).Data()),
						      "#mu_{#pi}","#sigma_{#pi}","e_{Amp.}",
						      "#mu_{e}","#sigma_{e}");

      //Pion Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(1,pionMeanPrev);
      tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(2,pion->widthFit[yIndex]->
						       Eval(GetmTm0RangeCenter(mTm0Index)));

      //Electron Parameters
      tpcPionMinusFit[yIndex][mTm0Index]->
	SetParameter(3,tpcPionMinusHisto[yIndex][mTm0Index]->
		     GetBinContent(tpcPionMinusHisto[yIndex][mTm0Index]->
				   FindBin(electron->meanFit[yIndex]->Eval(mTm0))));
      tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(4,electron->meanFit[yIndex]->
						       Eval(GetmTm0RangeCenter(mTm0Index)));
      tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(5,electron->widthFit[yIndex]->
						       Eval(GetmTm0RangeCenter(mTm0Index)));

      //Fit the Yield Histogram
      tpcPionMinusHisto[yIndex][mTm0Index]->Fit(tpcPionMinusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();

      //Add the Points to the Fit Par Graphs until statistics run out
      Int_t eMeanBin = tpcPionMinusHisto[yIndex][mTm0Index]->
        FindBin(tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(4));
      Double_t eBinContent = tpcPionMinusHisto[yIndex][mTm0Index]->Integral(eMeanBin-2,eMeanBin+2);
      if (eBinContent < 10)
        break;
      
      //Add Point to the Electron Amplitude Graph
      AddPointToParGraphmTm0(electron->amp[yIndex], &electron->ampPoint[yIndex],
			     tpcPionMinusFit[yIndex][mTm0Index],3,mTm0Index);
      AddPointToParGraphY(electron->ampY[mTm0Index], &electron->ampYPoint[mTm0Index],
			  tpcPionMinusFit[yIndex][mTm0Index],3,yIndex);

      fitParCanvas->cd(4);
      electron->amp[yIndex]->Draw("APZ");
      //fitParCanvas->cd(4);
      //electron->ampY[mTm0Index]->Draw("APZ");
      fitParCanvas->Update();


    }//End Loop Over mTm0Index Round 4
   
    //gSystem->Sleep(2000);

    //Get the number of electron points in the amp graph
    //If there are too few then kill the electron
    Int_t nElectronAmpPoints = electron->amp[yIndex]->GetN();
    if (nElectronAmpPoints < 5 && !killElectron){
      killElectron = true;
      cout <<"The electron has been killed due to too few points in the electron amplitude parameterization." <<endl;
    }

    if (!killElectron){
      //Find the Maximum of the Electron Amp Graph
      Double_t maxmTm0(0);
      Int_t iMax = TMath::LocMax(electron->amp[yIndex]->GetN(),electron->amp[yIndex]->GetY());
      maxmTm0 = electron->amp[yIndex]->GetX()[iMax];
      
      //Create the Electron Amplitude Fit
      electron->ampFit[yIndex] = new TF1(Form("electronMinus_AmpFit_%02d",yIndex),
					 "expo",maxmTm0,1.0);
      
      fitParCanvas->cd(4);
      electron->amp[yIndex]->Fit(electron->ampFit[yIndex],"RQ");
      fitParCanvas->Update();
    }
    else
      electron->ampFit[yIndex] = NULL;

    cout <<"Finished Parameterizing the Electron Amplitude!" <<endl;

    /********************
     **ROUND 5: This is the final fitting round for the pion minuses. 
     **         The Pion Width is fixed. All electron parameters are
     **         also fixed.
     **          
     **
     **NOTE:    
     **
     ********************/
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){

      //Make Sure the Histogram Exists
      if (!tpcPionMinusHisto[yIndex][mTm0Index] || !tpcPionMinusFit[yIndex][mTm0Index])
        continue;
      
      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionMinusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionMinusHisto[yIndex][mTm0Index]->Draw("E");
      
      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);
      
      //Previous Pion Mean Value
      Double_t pionMeanPrev = pion->mean[yIndex]->Eval(mTm0);


      //If the Electron has not been killed
      if (!killElectron){

	//Previous Electron Amp Value
	Double_t electronAmpPrev(0);
	electronAmpPrev = electron->amp[yIndex]->Eval(mTm0);
	
	//Create Fit
	if (tpcPionMinusFit[yIndex][mTm0Index])
	  tpcPionMinusFit[yIndex][mTm0Index]->Delete();
	tpcPionMinusFit[yIndex][mTm0Index] = new TF1(Form("TPCPionMinusFit_%02d_%02d",yIndex,mTm0Index),
						     "gaus(0)+gaus(3)",-1,2);
	
	//Set the Parameter Names
	tpcPionMinusFit[yIndex][mTm0Index]->SetParNames(Form("%s_{Amp.}",GetParticleSymbol(PID).Data()),
							"#mu_{#pi}","#sigma_{#pi}","e_{Amp.}",
							"#mu_{e}","#sigma_{e}");
	
	//Pion Parameters
	tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(0,1000);
	tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(1,pionMeanPrev);
	tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(2,pion->widthFit[yIndex]->
							 Eval(GetmTm0RangeCenter(mTm0Index)));
	
	//Get the Range of the Electron Amplitude fit
	Double_t minRange(0), maxRange(0);
	electron->ampFit[yIndex]->GetRange(minRange,maxRange);
	
	//Electron Parameters
	if (mTm0 <= minRange || mTm0 > maxRange)
	  tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(3,electronAmpPrev);
	else
	  tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(3,electron->ampFit[yIndex]->
							   Eval(GetmTm0RangeCenter(mTm0Index)));
	tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(4,electron->meanFit[yIndex]->
							 Eval(GetmTm0RangeCenter(mTm0Index)));
	tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(5,electron->widthFit[yIndex]->
							 Eval(GetmTm0RangeCenter(mTm0Index)));
      }//End if Electron has not been killed

      //If the Electron has been killed then we do a single Gaussian Fit
      else {
	
	if (tpcPionMinusFit[yIndex][mTm0Index])
	  tpcPionMinusFit[yIndex][mTm0Index]->Delete();
	tpcPionMinusFit[yIndex][mTm0Index] = 
	  new TF1(Form("TPCPionMinusFit_%02d_%02d",yIndex,mTm0Index),
		  "gaus(0)",-1,pionMeanPrev+2*pion->widthFit[yIndex]->Eval(mTm0));

	//Set the Parameter Names
        tpcPionMinusFit[yIndex][mTm0Index]->SetParNames(Form("%s_{Amp.}",GetParticleSymbol(PID).Data()),
                                                        "#mu_{#pi}","#sigma_{#pi}");
	
	//Pion Parameters
	tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(0,1000);
	tpcPionMinusFit[yIndex][mTm0Index]->SetParameter(1,pionMeanPrev);
	tpcPionMinusFit[yIndex][mTm0Index]->FixParameter(2,pion->widthFit[yIndex]->
							 Eval(GetmTm0RangeCenter(mTm0Index)));

      }

      //Fit the Yield Histogram
      tpcPionMinusHisto[yIndex][mTm0Index]->Fit(tpcPionMinusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();
            
    }//End Loop over mTm0 for loop 5

    cout <<"Finished Final Fitting Loop Of Pion Minuses " <<endl;

  }//End Loop Over yIndex


}

//_____________________________________________________________________________
void FitPionPlus(Int_t PID){

  for (Int_t yIndex=5; yIndex<nRapidityBins-3; yIndex++){
    
    /********************
     **ROUND 1: In round one we fix the pion mean and width and the electron mean,
     **         width, and amplitude to those used in the pion minus fits.  We fit
     **         the yield histogram with a three gaussian function to catch the protons.
     **         The proton mean is plotted as a function of mTm0 and fit.
     **
     **NOTE:    
     ********************/

    //If the user has specified a particular rapidity index and 
    //if this yIndex is not it, then skip it
    if (rapidityIndex >= 0 && yIndex != rapidityIndex)
      continue;

    cout <<"Working on Rapidity Index: " <<yIndex <<endl;

    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){
      
      //Make Sure the Histogram Exists
      if (!tpcPionPlusHisto[yIndex][mTm0Index])
	continue;

      //Make sure the Pion Minus Fit Exists
      if (!tpcPionMinusFit[yIndex][mTm0Index])
	continue;
      
      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionPlusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionPlusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionPlusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionPlusHisto[yIndex][mTm0Index]->Draw("E");

      //A Guess for the Proton Peak
      Double_t zProtonGuess = ComputeTPCZ(0,4,yIndex,mTm0Index);

      //Find the Min Bin on either side of the proton peak
      Double_t minRange = TMath::Max(zProtonGuess-.3,
				     tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(1));
      Double_t maxRange = zProtonGuess+.3;

      Double_t minBelow = FindMinBinCenter(tpcPionPlusHisto[yIndex][mTm0Index],
					   minRange,zProtonGuess);
      Double_t minAbove = FindMinBinCenter(tpcPionPlusHisto[yIndex][mTm0Index],
					   zProtonGuess,maxRange);


      //Use a single Gaussian to Parameterize the Mean of the Proton
      tpcPionPlusFit[yIndex][mTm0Index] = new TF1("","gaus(0)",minBelow,minAbove);
      
      //The Proton Parameters
      tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(0,1000);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(1,zProtonGuess);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(2,.08);

      //The Proton Parameter Limits
      tpcPionPlusFit[yIndex][mTm0Index]->SetParLimits(0,0,100000);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParLimits(1,zProtonGuess*.8,zProtonGuess*1.2);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParLimits(2,.02,.15);

      //Fit The Yield Histogram
      tpcPionPlusHisto[yIndex][mTm0Index]->Fit(tpcPionPlusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();

      //If the distance between the pion and proton peaks is too small then stop
      if (fabs(tpcPionPlusFit[yIndex][mTm0Index]->GetParameter(1)-
	       tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(1))
	  < 1.5 * (tpcPionPlusFit[yIndex][mTm0Index]->GetParameter(2)+
		   tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(2))){
	tpcPionPlusFit[yIndex][mTm0Index]->Delete();
	tpcPionPlusFit[yIndex][mTm0Index] = NULL;
	break;
      }

      //Add The Proton Mean to the Graph
      AddPointToParGraphmTm0(proton->mean[yIndex],&proton->meanPoint[yIndex],
			     tpcPionPlusFit[yIndex][mTm0Index],1,mTm0Index);

      fitParCanvas->cd(8);
      proton->mean[yIndex]->Draw("APZ");
      fitParCanvas->Update();


    }//End of Loop Over mTm0Index for round 1

    //Create a Function to fit the Proton Mean
    proton->meanFit[yIndex] = new TF1(Form("protonPlus_MeanFit_%02d",yIndex),
				      ConfoundMeanFit1,0,1.0,6);
    proton->meanFit[yIndex]->FixParameter(0,0);
    proton->meanFit[yIndex]->FixParameter(1,4);
    proton->meanFit[yIndex]->FixParameter(2,yIndex);
    proton->meanFit[yIndex]->SetParameter(3,0.0);
    proton->meanFit[yIndex]->SetParameter(4,1.0);
    proton->meanFit[yIndex]->SetParLimits(4,.5,1.5);
    proton->meanFit[yIndex]->SetParameter(5,0.0);
    //proton->meanFit[yIndex]->SetParLimits(5);
    
    //Fit the Proton Mean
    fitParCanvas->cd(8);
    proton->mean[yIndex]->Fit(proton->meanFit[yIndex],"RQ");
    
    fitParCanvas->Update();
    //gSystem->Sleep(2000);
    
    cout <<"Finished Parameterizing the Proton Mean!" <<endl;
    
    
    /********************
     **ROUND 2: In Round 2 All the above from round 1 applies, but here we fix
     **         the proton mean to the parameterization from round 1. The Width is
     **         plotted and fit as a function of mTm0.
     **
     **NOTE:    
     ********************/
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){
      
      //Make Sure the Histogram Exists
      if (!tpcPionPlusHisto[yIndex][mTm0Index])
	continue;

      //Make sure the Pion Minus Fit Exists
      if (!tpcPionMinusFit[yIndex][mTm0Index])
	continue;
 
      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionPlusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionPlusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionPlusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionPlusHisto[yIndex][mTm0Index]->Draw("E");

      //Find the Minimum Bin on either side of the proton peak
      Double_t protonMean = proton->meanFit[yIndex]->Eval(mTm0);
      Double_t minRange = TMath::Max(protonMean-.3,
				     tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(1));
      Double_t maxRange = protonMean+.3;
      Double_t minBelowPeak = FindMinBinCenter(tpcPionPlusHisto[yIndex][mTm0Index],
					       minRange,protonMean);
      Double_t minAbovePeak = FindMinBinCenter(tpcPionPlusHisto[yIndex][mTm0Index],
					       protonMean,maxRange);


      //Use a single Gaussian to Parameterize the Width of the Proton
      if (tpcPionPlusFit[yIndex][mTm0Index])
	tpcPionPlusFit[yIndex][mTm0Index]->Delete();
      tpcPionPlusFit[yIndex][mTm0Index] = new TF1("","gaus(0)",minBelowPeak,minAbovePeak);

      //Proton Parameters: Fix the Mean, Set the Amp and Width
      tpcPionPlusFit[yIndex][mTm0Index]->
	SetParameter(0,tpcPionPlusHisto[yIndex][mTm0Index]->
		     GetBinContent(tpcPionPlusHisto[yIndex][mTm0Index]->FindBin(protonMean)));
      tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(1,protonMean);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(2,.08);
      
      //Proton Parameter Limits
      tpcPionPlusFit[yIndex][mTm0Index]->
	SetParLimits(0,0,tpcPionPlusFit[yIndex][mTm0Index]->GetParameter(0)*1.05);
      tpcPionPlusFit[yIndex][mTm0Index]->
	SetParLimits(2,.05,.15);

      //Fit the Yield Histogram
      tpcPionPlusHisto[yIndex][mTm0Index]->Fit(tpcPionPlusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();
      
      //If the distance between the pion and proton peaks is too small then stop
      if (fabs(tpcPionPlusFit[yIndex][mTm0Index]->GetParameter(1)-
	       tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(1))
          < 1.5*(tpcPionPlusFit[yIndex][mTm0Index]->GetParameter(2)+
		 tpcPionMinusFit[yIndex][mTm0Index]->GetParameter(2))){
	tpcPionPlusFit[yIndex][mTm0Index]->Delete();
	tpcPionPlusFit[yIndex][mTm0Index] = NULL;
	break;
      }

      //Add the Width Parameter to the Graph
      AddPointToParGraphmTm0(proton->width[yIndex], &proton->widthPoint[yIndex],
			     tpcPionPlusFit[yIndex][mTm0Index],2,mTm0Index);
      
      fitParCanvas->cd(9);
      proton->width[yIndex]->Draw("APZ");
      fitParCanvas->Update();
      

    }//End Loop Over mTm0Index For Round 2
    
    //Create a function to fit the proton Width
    proton->widthFit[yIndex] = new TF1(Form("protonPlus_widthFit_%02d",yIndex),
				       SwooshFunc,.05,1.0,4);
    proton->widthFit[yIndex]->SetParameters(.2,.5,.3,.1);
    proton->widthFit[yIndex]->SetParLimits(3,.0,5);
    
    Double_t maxmTm0ProtonPoint = proton->width[yIndex]->GetX()[proton->width[yIndex]->GetN()-1];
    proton->widthFit[yIndex]->SetParLimits(2,.1, maxmTm0ProtonPoint < .5 ? 
					   maxmTm0ProtonPoint : .5);
    
    //Fit the Proton Width
    fitParCanvas->cd(9);
    proton->width[yIndex]->Fit(proton->widthFit[yIndex],"RQ");
    fitParCanvas->Update();
    
    cout <<"Finished Parameterizing the Proton Width!" <<endl;

    /********************
     **ROUND 3: In round 3 the proton width is fixed using the parameterization
     **         from round 2. This is the Final Fit
     **
     **NOTE:    
     ********************/
    for (Int_t mTm0Index=2; mTm0Index<nmTm0Bins; mTm0Index++){
      
      //Make Sure the Histogram Exists
      if (!tpcPionPlusHisto[yIndex][mTm0Index])
	continue;

      //Make sure the Pion Minus Fit Exists
      if (!tpcPionMinusFit[yIndex][mTm0Index])
	continue;

      //Draw the Yield Plot
      fittingCanvas->cd();
      tpcPionPlusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionPlusHisto[yIndex][mTm0Index]->SetAxisRange(-1,2.5);
      tpcPionPlusHisto[yIndex][mTm0Index]->SetMarkerStyle(kOpenCircle);
      tpcPionPlusHisto[yIndex][mTm0Index]->Draw("E");

      Double_t mTm0 = GetmTm0RangeCenter(mTm0Index);
      Double_t protonMean = proton->meanFit[yIndex]->Eval(mTm0);
      Double_t protonAmpSeed = tpcPionPlusHisto[yIndex][mTm0Index]->
	GetBinContent(tpcPionPlusHisto[yIndex][mTm0Index]->FindBin(protonMean));
      Double_t protonWidth = proton->widthFit[yIndex]->Eval(mTm0);
      Double_t pionMean = 0;
      Double_t pionWidth = pion->widthFit[yIndex]->Eval(mTm0);
      Double_t pionAmpSeed = tpcPionPlusHisto[yIndex][mTm0Index]->
	GetBinContent(tpcPionPlusHisto[yIndex][mTm0Index]->FindBin(pionMean));
      Double_t electronMean = electron->meanFit[yIndex]->Eval(mTm0);
      Double_t electronWidth = electron->widthFit[yIndex]->Eval(mTm0);
      Double_t electronAmpSeed = tpcPionPlusHisto[yIndex][mTm0Index]->
	GetBinContent(tpcPionPlusHisto[yIndex][mTm0Index]->FindBin(electronMean));
      
      if (tpcPionPlusFit[yIndex][mTm0Index])
	tpcPionPlusFit[yIndex][mTm0Index]->Delete();

      //Check to make sure the pion mean is reasonable
      if (fabs(pionMean) > fabs(pionWidth)){
	tpcPionPlusFit[yIndex][mTm0Index] = NULL;
	continue;
      }

      //if the distance between the proton and pion peaks is too small then stop
      if (fabs(pionMean-protonMean) < 1.5*(pionWidth+protonWidth)){
	cout <<fabs(pionMean-protonMean) <<" " <<1.5*(pionWidth+protonWidth) <<endl;
	tpcPionPlusFit[yIndex][mTm0Index] = NULL;
	break;
      }
      
      //Find the Minimum on either side of the pion peak                                                                     
      Double_t minBelowPeak = FindMinBinCenter(tpcPionPlusHisto[yIndex][mTm0Index],
					       pionMean-.5,pionMean);
      Double_t minAbovePeak = FindMinBinCenter(tpcPionPlusHisto[yIndex][mTm0Index],
					       pionMean,pionMean+.3);

      //The Final form of the fit depends on whether the electron has been parameterized or not
      //If the electron has been parameterized then it will be a three gaussian fit, otherwise
      //it will be a one Gaussian if
      if (killElectron){

	tpcPionPlusFit[yIndex][mTm0Index] = new TF1("","gaus(0)",minBelowPeak,minAbovePeak);

	//Set the Parameter Names 
        tpcPionPlusFit[yIndex][mTm0Index]->SetParNames(Form("%s_{Amp.}",GetParticleSymbol(PID).Data()),
						       "#mu_{#pi}","#sigma_{#pi}");
      }
      else {
	
	tpcPionPlusFit[yIndex][mTm0Index] = new TF1("","gaus(0)+gaus(3)+gaus(6)",minBelowPeak,protonMean+.3);
	
	//Set the Parameter Names
        tpcPionPlusFit[yIndex][mTm0Index]->SetParNames(Form("%s_{Amp.}",GetParticleSymbol(PID).Data()),
						       "#mu_{#pi}","#sigma_{#pi}","e_{Amp.}",
						       "#mu_{e}","#sigma_{e}",
						       "p_{Amp.}","#mu_{p}","#sigma_{p}");

	//Electron Parameters: Fix the Mean and Width, Step Amplitude
	tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(4,electronMean);
	tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(5,electronWidth);

	//Electron Amplitude: If the electron is not overlapping with another particle, then
	//we set the amplitude according to the bin at its mean, 
	if (fabs(electronMean-pionMean) > (pionWidth+electronWidth)*1.25 &&
	    fabs(electronMean-protonMean) > (protonWidth+electronWidth)*1.5){
	  tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(3,electronAmpSeed);
	  tpcPionPlusFit[yIndex][mTm0Index]->
	    SetParLimits(3,electronAmpSeed*.5,electronAmpSeed*1.01);
	}
	else {
	  tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(3,electron->ampFit[yIndex]->Eval(mTm0));
	}

	//Proton Parameters: Fix Mean and Width, Set amplitude
	tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(6,protonAmpSeed);
	tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(7,protonMean);
	tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(8,protonWidth);
	
	tpcPionPlusFit[yIndex][mTm0Index]->SetParLimits(6,0,protonAmpSeed*1.05);

      }

      //Set the Fit Function Name
      tpcPionPlusFit[yIndex][mTm0Index]->SetName(Form("TPCPionPlusFit_%02d_%02d",
						      yIndex,mTm0Index));

      //Pion Parameters: Fix Width, Set Amp and Mean
      tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(0,pionAmpSeed);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParameter(1,pionMean);
      tpcPionPlusFit[yIndex][mTm0Index]->FixParameter(2,pionWidth);
      
      //Set Pion Parameter Limits
      tpcPionPlusFit[yIndex][mTm0Index]->SetParLimits(0,0,pionAmpSeed*1.1);
      tpcPionPlusFit[yIndex][mTm0Index]->SetParLimits(1,minBelowPeak,minAbovePeak);

      //Fit the Yield Histogram
      tpcPionPlusHisto[yIndex][mTm0Index]->Fit(tpcPionPlusFit[yIndex][mTm0Index],"RQ");
      fittingCanvas->Update();

    }//End Loop Over mTm0Index Round 3

    cout <<"Finished Final Fit for Pion Pluses!" <<endl;

  }//End Loop Over yIndex

}

//________________________________________________________________
void LoadZPlots(TFile *inFile, Int_t SIDE){

  //This Function Loads the tpcZ Pion Plots

  //Loop Over the Rapidity Bins and mTm0 Bins and Load the Histograms
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
    for (Int_t mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){
      
      //The Pion Plus
      inFile->cd(GetSideName(SIDE)+"/"+GetParticleName(0)+"/zTPC/");

      tpcPionPlusHisto[yIndex][mTm0Index] = (TH1F*)gDirectory->
	Get(GetParticleName(0)+"_z_TPC_"+GetSideName(SIDE)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index));

      //The Pion Minus
      inFile->cd(GetSideName(SIDE)+"/"+GetParticleName(1)+"/zTPC/");
      
      tpcPionMinusHisto[yIndex][mTm0Index] = (TH1F*)gDirectory->
	Get(GetParticleName(1)+"_z_TPC_"+GetSideName(SIDE)+
	    TString::Format("_%02d_%02d",yIndex,mTm0Index));

      if (tpcPionPlusHisto[yIndex][mTm0Index])
	tpcPionPlusHisto[yIndex][mTm0Index]->GetYaxis()->SetTitle("dN_{ch+}/dZ(TPC)");
      if (tpcPionMinusHisto[yIndex][mTm0Index])
	  tpcPionMinusHisto[yIndex][mTm0Index]->GetYaxis()->SetTitle("dN_{ch-}/dZ(TPC)");
    }
  }

  inFile->cd();
  nEventsHisto = (TH1F*)inFile->Get("nEventsSide");

}

//________________________________________________________________________
void DefineSpectra(Double_t ENERGY, Int_t SIDE){

  //Loop Over the Rapidity Bins and Define the Spectra
  for (Int_t pIndex=0; pIndex<2; pIndex++){
    for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){
      
      rawSpectra[yIndex][pIndex] = new TGraphErrors();
      rawSpectra[yIndex][pIndex]->SetMarkerStyle(20);
      rawSpectra[yIndex][pIndex]->SetName("rawSpectra_"+GetSideName(SIDE)+"_"+GetParticleName(pIndex)+
				  +TString::Format("_%02d",yIndex));
      rawSpectra[yIndex][pIndex]->SetTitle(GetParticleSymbol(pIndex)+" Raw Spectra "+GetSideName(SIDE)+
				   TString::Format(" Au_{Like}+Al #sqrt{s_{NN}} = %.2f (%.2f #leq y < %.2f)",
						   ENERGY,GetRapidityRangeLow(yIndex,SIDE),
						   GetRapidityRangeHigh(yIndex,SIDE))+
				   TString::Format(";m_{T}-m_{%s};",GetParticleSymbol(pIndex).Data()));
      
      spectraPoint[yIndex][pIndex] = 0;
    } 
  }
}


//___________________________________________________________________________
Bool_t MakeSpectra(TH1F *fitHisto, TF1 *fitFunc, Int_t yIndex, Int_t mTm0Index,
		   Double_t ENERGY, Int_t PID, Int_t SIDE,TString IMAGEDIR){

  //Adds a point to the spectra 
  //Return false on fail, and true on success

  //Make Sure the Histogram and Fit Exist
  if (!fitHisto || !fitFunc)
    return false;

  //Set the Attributes of the Full Fitting Function

  //Draw the Final Version of the Fit
  fittingCanvas->Clear();
  fittingCanvas->cd();
  fittingCanvas->SetTopMargin(.05);
  fittingCanvas->SetRightMargin(.05);
  fitHisto->SetMarkerStyle(kOpenCircle);
  fitHisto->SetTitle("");
  fitHisto->SetAxisRange(-2.0,3.0);
  fitHisto->Draw("E");

  //Draw the Fit
  //fitFunc->SetLineStyle(7);
  fitFunc->SetLineWidth(3);
  fitFunc->SetLineColor(kBlack);
  fitFunc->SetNpx(10000);
  fitFunc->Draw("SAME");

  //Create a Label
  TPaveText label(.12,.8,.35,.92,"BL NDC");
  label.SetTextAlign(11);
  label.AddText(Form("Z(%s) Yield Extraction Fit",GetParticleSymbol(PID).Data()));
  label.AddText(Form("  Au_{Like}+Al #sqrt{s_{NN}} = %.2f GeV",ENERGY));
  label.AddText(Form("  %.2f #leq y_{%s} < %.2f | %.2f #leq m_{T}-m_{%s} < %.2f",
		     GetRapidityRangeLow(yIndex,SIDE),GetParticleSymbol(PID).Data(),
		     GetRapidityRangeHigh(yIndex,SIDE),GetmTm0RangeLow(mTm0Index),
		     GetParticleSymbol(PID).Data(),GetmTm0RangeHigh(mTm0Index)));
  label.GetLine(0)->SetTextSize(.03);
  label.GetLine(1)->SetTextSize(.02);
  label.GetLine(2)->SetTextSize(.02);
  label.SetFillColor(kWhite);
  label.SetBorderSize(0);
  label.Draw("SAME");

  //Make A Function for Each of the Particles
  TF1 pionGaus("pion","gaus(0)",-20,20);
  pionGaus.FixParameter(0,fitFunc->GetParameter(0));
  pionGaus.FixParameter(1,fitFunc->GetParameter(1));
  pionGaus.FixParameter(2,fitFunc->GetParameter(2));
  pionGaus.SetParError(0,fitFunc->GetParError(0));
  pionGaus.SetParError(1,fitFunc->GetParError(1));
  pionGaus.SetParError(2,fitFunc->GetParError(2));
  pionGaus.SetLineColor(GetParticleColor(PID));
  pionGaus.SetLineWidth(3);
  pionGaus.SetLineStyle(7);
  pionGaus.SetNpx(10000);
  pionGaus.Draw("SAME");
  
  TF1 electronGaus("electron","gaus(0)",-20,20);
  if (!killElectron){
    electronGaus.FixParameter(0,fitFunc->GetParameter(3));
    electronGaus.FixParameter(1,fitFunc->GetParameter(4));
    electronGaus.FixParameter(2,fitFunc->GetParameter(5));
    electronGaus.SetParError(0,fitFunc->GetParError(3));
    electronGaus.SetParError(1,fitFunc->GetParError(4));
    electronGaus.SetParError(2,fitFunc->GetParError(5));
    electronGaus.SetLineColor(GetParticleColor(6));
    electronGaus.SetLineWidth(3);
    electronGaus.SetLineStyle(7);
    electronGaus.SetNpx(10000);
    electronGaus.Draw("SAME");
  }

  //Set NParameter Switch
  //Controls Whether protons will be drawn
  Bool_t parSwitch = false;
  if (fitFunc->GetNpar() > 6 && !killElectron)
    parSwitch = true;

  TF1 protonGaus("proton","gaus(0)",-20,20);
  if (parSwitch){
    protonGaus.FixParameter(0,fitFunc->GetParameter(6));
    protonGaus.FixParameter(1,fitFunc->GetParameter(7));
    protonGaus.FixParameter(2,fitFunc->GetParameter(8));
    protonGaus.SetParError(0,fitFunc->GetParError(6));
    protonGaus.SetParError(1,fitFunc->GetParError(7));
    protonGaus.SetParError(2,fitFunc->GetParError(8));
    protonGaus.SetLineColor(GetParticleColor(4));
    protonGaus.SetLineWidth(3);
    protonGaus.SetLineStyle(7);
    protonGaus.SetNpx(10000);
    protonGaus.Draw("SAME");

  }

  //Get the Stats Box and Set its attributes
  TPaveStats *statsBox = (TPaveStats*)fitHisto->FindObject("stats");
  if (PID == 1){
    statsBox->SetX1NDC(.7);
    statsBox->SetY1NDC(.50);
    statsBox->SetX2NDC(.97);
    statsBox->SetY2NDC(.93);
  }
  else {
    statsBox->SetX1NDC(.14);
    statsBox->SetY1NDC(.12);
    statsBox->SetX2NDC(.37);
    statsBox->SetY2NDC(.78);
  }
  gPad->Update();

  //Make A Legend
  TLegend leg;
  leg.SetOption("brNDC");
  if (parSwitch){
    leg.SetX1NDC(.65);
    leg.SetY1NDC(.89);
    leg.SetX2NDC(.93);
    leg.SetY2NDC(.94);
  }
  else {
    leg.SetX1NDC(.15);
    leg.SetY1NDC(.5);
    leg.SetX2NDC(.22);
    leg.SetY2NDC(.75);
  }
  leg.SetNColumns(parSwitch ? 5 : 1);
  leg.AddEntry(fitFunc,"Fit","L");
  leg.AddEntry("pion","#pi","L");
  if (!killElectron)
    leg.AddEntry("electron","e","L");
  if (parSwitch && !killElectron){
    leg.AddEntry("proton","p","L");
  }
  leg.SetFillColor(kWhite);
  leg.SetBorderSize(0);
  leg.Draw("SAME");
  
  fittingCanvas->Update();

  //Extract the Yield
  Double_t amp = pionGaus.GetParameter(0);
  Double_t ampError = pionGaus.GetParError(0);
  Double_t width = pionGaus.GetParameter(2);
  Double_t widthError = pionGaus.GetParError(2);
  Double_t pi = 3.14159;

  Double_t zBinWidth = (Double_t)fitHisto->GetBinWidth(2);
  Double_t mTm0BinCenter = GetmTm0RangeCenter(mTm0Index);
  Double_t nEvents = (Double_t)nEventsHisto->GetBinContent(nEventsHisto->FindBin((Double_t)SIDE));

  //Set the Normalization Factor
  Double_t normFactor = (1.0/(mTm0BinCenter+GetParticleMass(PID))) * (1.0/mTm0Width) * (1.0/rapidityWidth) * 
    (1.0/nEvents) * (1.0/(2.0*pi));

  //Compute the Raw Yield
  Double_t rawYield, rawYieldError;
  rawYield = ( (amp * width * sqrt(2.0*pi)) / zBinWidth ) * normFactor;
  rawYieldError = ( (amp * width * sqrt(pow(ampError/amp,2)+pow(widthError/width,2)) * sqrt(2.0*pi)) / zBinWidth) * normFactor;
  

  // --- Make Sure this is a good point before adding it to the Spectra

  //If this is a Pion Plus Spectra Stop when the pion and proton peak are close together
  Double_t deltaMean = fabs(pionGaus.GetParameter(1)-protonGaus.GetParameter(1));
  Double_t sumWidths = pionGaus.GetParameter(2)+protonGaus.GetParameter(2);
  if (parSwitch && deltaMean < 2.0*sumWidths){
    return false;
  }

  //Add the Point to the Spectra
  rawSpectra[yIndex][PID]->SetPoint(spectraPoint[yIndex][PID],
			       GetmTm0RangeCenter(mTm0Index),
			       rawYield);
  rawSpectra[yIndex][PID]->SetPointError(spectraPoint[yIndex][PID],
				    0,
				    rawYieldError);
  spectraPoint[yIndex][PID]++;
  
  spectraCanvas->cd();
  rawSpectra[yIndex][PID]->Draw("APZ");
  spectraCanvas->Update();

  //Save the Fit Image
  if (SAVEIMG){
    
    TString fullImageDir = IMAGEDIR+"/"+GetParticleName(PID);
    fullImageDir += "/"+GetSideName(SIDE);

    SaveImage(fittingCanvas,
	      fullImageDir,
	      Form("zTPCFit_%s_%s_%02d_%02d",
		   GetParticleName(PID).Data(),
		   GetSideName(SIDE).Data(),
		   yIndex,mTm0Index),
	      "eps");

    //Save a macro of a specific fit so we can easily adjust it
    //so that it can be used as a final fig in the paper.
    if (yIndex == 20 && mTm0Index == 11){
      SaveImage(fittingCanvas,fullImageDir,
		Form("zTPCFit_%s_%s_%02d_%02d",
		     GetParticleName(PID).Data(),
		     GetSideName(SIDE).Data(),
		     yIndex,mTm0Index),
		"C");
    } 
  }

  return true;

}

//__________________________________________________________________
Double_t FindMaxBinCenter(TH1F *h, Double_t min, Double_t max){

  //Loop Over the Bins the the range min to max and return the 
  //bin ceter of the bin with the largest content

  Double_t binCenter(0);
  Double_t maxValue(-1);

  for (Int_t iBin=h->FindBin(min); iBin<h->FindBin(max); iBin++){

    Double_t binContent = h->GetBinContent(iBin);
    if (h->GetBinContent(iBin) > maxValue){
      binCenter = h->GetBinCenter(iBin);
      maxValue = binContent;
    }
  }

  return binCenter;

}

//___________________________________________________________________
Double_t FindMinBinCenter(TH1F *h, Double_t min, Double_t max){

  //Loop over the Bins in the range min to max and return the 
  //bin center of the bin with the smallest content
  
  Double_t binCenter(0);
  Double_t minValue(10000000000);

  for (Int_t iBin=h->FindBin(min); iBin<h->FindBin(max); iBin++){

    Double_t binContent = h->GetBinContent(iBin);
    if (binContent < minValue){
      binCenter = h->GetBinCenter(iBin);
      minValue = binContent;
    }
    
  }
  
  return binCenter;
}
