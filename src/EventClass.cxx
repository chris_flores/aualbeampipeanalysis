#include "TrackClass.h"
#include "EventClass.h"

class TClonesArray;

//The Implementation of the Event Class

ClassImp(EVENT);

//__________________________________________________________________
EVENT::EVENT(){

  //Event Constructor
  //Set All Values to -999

  runNumber       = -999;
  eventNumber     = -999;
  nPions           = -999;
  trigID3         = -999;
  nGlobalTracks   = -999;
  nPrimaryTracks  = -999;
  refMult         = -999;
  vpdVz           = -999;
  xVertex         = -999;
  yVertex         = -999;
  zVertex         = -999;
  tpcDirection    = -999;
  vpdDiff         = -999;
  bbcZVertex      = -999;
  totalXMomentum  = -999;
  totalYMomentum  = -999;
  totalZMomentum  = -999;
  eventTime       = -999;
  productionTime  = -999;
  centerOfMassEnergy      = -999;
  initialBeamIntensity    = -999;
  zdcCoincidenceRate      = -999;
  bbcCoincidenceRate      = -999;
  backgroundRate          = -999;
  bbcBlueBackgroundRate   = -999;
  bbcYellowBackgroundRate = -999;
  beamLifeTime            = -999;

  TrackClonesArray = new TClonesArray("TRACK",1);


}

//__________________________________________________________________
EVENT::EVENT(Float_t *eventarray){

  //Sets all the Event attributes from the passed in array
  runNumber       = (Long64_t)eventarray[0];
  eventNumber     = (Long64_t)eventarray[1];
  nPions          = (Int_t)eventarray[2];
  trigID3         = (Int_t)eventarray[3];
  nGlobalTracks   = (Int_t)eventarray[4];
  nPrimaryTracks  = (Int_t)eventarray[5];
  refMult         = (Int_t)eventarray[6];
  vpdVz           = (Double_t)eventarray[7];
  xVertex         = (Double_t)eventarray[8];
  yVertex         = (Double_t)eventarray[9];
  zVertex         = (Double_t)eventarray[10];
  tpcDirection    = (Double_t)eventarray[11];
  vpdDiff         = (Double_t)eventarray[12];
  bbcZVertex      = (Double_t)eventarray[13];
  totalXMomentum  = (Double_t)eventarray[14];
  totalYMomentum  = (Double_t)eventarray[15];
  totalZMomentum  = (Double_t)eventarray[16];
  eventTime       = (Double_t)eventarray[17];
  productionTime  = (Double_t)eventarray[18];
  centerOfMassEnergy      = (Double_t)eventarray[19];
  initialBeamIntensity    = (Double_t)eventarray[20];
  zdcCoincidenceRate      = (Double_t)eventarray[21];
  bbcCoincidenceRate      = (Double_t)eventarray[22];
  backgroundRate          = (Double_t)eventarray[23];
  bbcBlueBackgroundRate   = (Double_t)eventarray[24];
  bbcYellowBackgroundRate = (Double_t)eventarray[25];
  beamLifeTime            = (Double_t)eventarray[26];

  TrackClonesArray = new TClonesArray("TRACK",1);

}

//__________________________________________________________________
EVENT::~EVENT(){

  //EVENT Destructor
  delete TrackClonesArray;

}

//__________________________________________________________________
void EVENT::PrintEvent(Bool_t printtrack){

  //Print Event Information
  cout <<"--- Event: " <<eventNumber <<" --- " <<"RunNumber: " << runNumber <<" ---" <<"\n"
       <<"  " <<"PrimaryTracks: " <<nPrimaryTracks <<" nPions: " <<nPions <<"\n"
       <<"  " <<"Vx: " <<xVertex <<" Vy: " <<yVertex <<" Vz: " <<zVertex <<"\n"
       <<"  " <<"Entries in Track Array: " <<TrackClonesArray->GetEntries() <<"\n";
  
  //Print Primary Track Information if Requested
  if (printtrack){

    for (Int_t i=0; i<nPrimaryTracks; i++){
      ((TRACK*)TrackClonesArray->At(i))->PrintTrack();
    }

  }//End Print Tracks

}

//__________________________________________________________________
void EVENT::ResetEvent(){

  //Clear the Track TClonesArray
  TrackClonesArray->Clear("C");
}

//__________________________________________________________________
void EVENT::AddTrackArray(TClonesArray *trackarray){

  //Adds the User Defined TClonesArray as the Track Array
  TrackClonesArray->AbsorbObjects(trackarray);
}

//__________________________________________________________________
void EVENT::AddTrack(Float_t *trackarray, Int_t idx){

  TRACK *tempTrack = (TRACK*)TrackClonesArray->ConstructedAt(idx);
  tempTrack->SetTrackAttributes(trackarray);

}

//__________________________________________________________________
void EVENT::SetEventAttributes(Float_t *eventarray){

  //Sets all the Event attributes from the passed in array
  runNumber       = (Long64_t)eventarray[0];
  eventNumber     = (Long64_t)eventarray[1];
  nPions          = (Int_t)eventarray[2];
  trigID3         = (Int_t)eventarray[3];
  nGlobalTracks   = (Int_t)eventarray[4];
  nPrimaryTracks  = (Int_t)eventarray[5];
  refMult         = (Int_t)eventarray[6];
  vpdVz           = (Double_t)eventarray[7];
  xVertex         = (Double_t)eventarray[8];
  yVertex         = (Double_t)eventarray[9];
  zVertex         = (Double_t)eventarray[10];
  tpcDirection    = (Double_t)eventarray[11];
  vpdDiff         = (Double_t)eventarray[12];
  bbcZVertex      = (Double_t)eventarray[13];
  totalXMomentum  = (Double_t)eventarray[14];
  totalYMomentum  = (Double_t)eventarray[15];
  totalZMomentum  = (Double_t)eventarray[16];
  eventTime       = (Double_t)eventarray[17];
  productionTime  = (Double_t)eventarray[18];
  centerOfMassEnergy      = (Double_t)eventarray[19];
  initialBeamIntensity    = (Double_t)eventarray[20];
  zdcCoincidenceRate      = (Double_t)eventarray[21];
  bbcCoincidenceRate      = (Double_t)eventarray[22];
  backgroundRate          = (Double_t)eventarray[23];
  bbcBlueBackgroundRate   = (Double_t)eventarray[24];
  bbcYellowBackgroundRate = (Double_t)eventarray[25];
  beamLifeTime            = (Double_t)eventarray[26];

}


//__________________________________________________________________
TRACK *EVENT::GetTrack(Int_t i){

  return (TRACK *)TrackClonesArray->At(i);
}


//__________________________________________________________________
Int_t EVENT::GetTrackArrayEntries(){
  return TrackClonesArray->GetEntries();
}
