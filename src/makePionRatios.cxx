/*********************************************************
 **
 **
 **
 **
 **
 **
 *********************************************************/

#include "../inc/includeList.h"
#include "../inc/utilityFunctions.h"

//SAVE?
const Bool_t SAVEOBJ=true;

// --- Functions ---
void MakeRatio(TGraphErrors *numerator,
	       TGraphErrors *denominator,
	       TGraphErrors *ratioGraph);

// MAIN ______________________________________________________
void makePionRatios(TString SPECTRAFILE, Int_t SIDE, Int_t RAPIDITYINDEX=-1){

  //Open the Spectrum file and make a new directory if we are saving
  TFile *spectraFile = new TFile(SPECTRAFILE,SAVEOBJ ? "UPDATE":"READ");
  spectraFile->cd(Form("%s",GetSideName(SIDE).Data()));
  if (SAVEOBJ)
    gDirectory->mkdir("PionRatios");

  //Create a canvas
  TCanvas *ratioCanvas = new TCanvas("ratioCanvas","Pion Ratios",20,20,1200,600);
  ratioCanvas->Divide(2,1);
  
  //Variables used in the loop
  TGraphErrors *pionPlusSpectrum = NULL;
  TGraphErrors *pionMinusSpectrum = NULL;
  TGraphErrors *pionRatio = NULL;

  //Loop Over the rapidity bins and create the ratios
  for (Int_t yIndex=0; yIndex<nRapidityBins; yIndex++){

    //If the user has specified a particular rapidity bin and if this 
    //is not it, then skip it
    if (RAPIDITYINDEX != -1 && yIndex != RAPIDITYINDEX)
      continue;

    //Get the Spectra
    pionPlusSpectrum = (TGraphErrors *)spectraFile->
      Get(Form("%s/RawSpectra/PionPlus/rawSpectra_%s_PionPlus_%d",
	       GetSideName(SIDE).Data(),GetSideName(SIDE).Data(),yIndex));
    pionMinusSpectrum = (TGraphErrors *)spectraFile->
      Get(Form("%s/RawSpectra/PionMinus/rawSpectra_%s_PionMinus_%d",
	       GetSideName(SIDE).Data(),GetSideName(SIDE).Data(),yIndex));

    //Make Sure that the Spectra Exist
    if (pionPlusSpectrum == NULL || pionMinusSpectrum == NULL)
      continue;

    pionPlusSpectrum->SetMarkerColor(kRed);
    pionMinusSpectrum->SetMarkerColor(kBlack);

    ratioCanvas->cd(1);
    pionMinusSpectrum->Draw("APZ");
    pionPlusSpectrum->Draw("PZ");
    ratioCanvas->Update();

    //Make the Graph For the Ratio
    pionRatio = new TGraphErrors();
    pionRatio->SetName(Form("pionRatio_%s_%d",GetSideName(SIDE).Data(),yIndex));
    MakeRatio(pionPlusSpectrum,pionMinusSpectrum,pionRatio);
    
    ratioCanvas->cd(2);
    pionRatio->Draw("APZ");

    //Save The Ratio
    if (SAVEOBJ){
      spectraFile->cd(Form("%s/PionRatios/",GetSideName(SIDE).Data()));
      pionRatio->Write();
    }

    pionPlusSpectrum->Delete();
    pionMinusSpectrum->Delete();
    pionRatio->Delete();

  }//End Loop Over yIndex

  spectraFile->Close();
}

//__________________________________________________________
void MakeRatio(TGraphErrors *numerator,
               TGraphErrors *denominator,
               TGraphErrors *ratioGraph){

  //Get the Number of Points for each Spectra
  Int_t nPoints0 = numerator->GetN();
  Int_t nPoints1 = denominator->GetN();

  //Loop over the mTm0 bins, find the the points from the numerator and denominator
  //graphs that are in this bin and take the ratio
  for (Int_t mTm0Index=0; mTm0Index<nmTm0Bins; mTm0Index++){

    Double_t ratio(0), ratioErr(0), xAvg(0);
    
    Int_t pt0(-1);
    Double_t x0(0), y0(0), xerr0(0), yerr0(0);
    for (Int_t i=0; i<nPoints0; i++){
      x0 = numerator->GetX()[i];
      if (x0 > GetmTm0RangeLow(mTm0Index) && x0 <= GetmTm0RangeHigh(mTm0Index)){
	pt0 = i;
	y0 = numerator->GetY()[i];
	xerr0 = numerator->GetEX()[i];
	yerr0 = numerator->GetEY()[i];
	break;
      }
    }

    Int_t pt1(-1);
    Double_t x1(0), y1(0), xerr1(0), yerr1(0);
    for (Int_t i=0; i<nPoints1; i++){
      x1 = denominator->GetX()[i];
      if (x1 > GetmTm0RangeLow(mTm0Index) && x1 <= GetmTm0RangeHigh(mTm0Index)){
        pt1 =i;
	y1 = denominator->GetY()[i];
	xerr1 = denominator->GetEX()[i];
	yerr1 = denominator->GetEY()[i];
        break;
      }
    }

    //If no points were found then skip this mTm0 Index
    if (pt0 == -1 || pt1 == -1)
      continue;

    //Just in case the x values of the points are different, take the average
    xAvg = (x0 + x1) / 2.0;
    
    //Compute the Ratio of the Points
    ratio = y0 / y1;
    ratioErr = ratio * sqrt(pow(yerr0/y0,2) + pow(yerr1/y1,2));

    //Set the Point in the ratio graph
    ratioGraph->SetPoint(ratioGraph->GetN(),xAvg,ratio);
    ratioGraph->SetPointError(ratioGraph->GetN()-1,mTm0Width/2.0,ratioErr);

  }//End Loop Over mTm0Index



}
