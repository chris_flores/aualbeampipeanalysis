#!/bin/bash

#Help
help_func(){
    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        runMakeYieldHistograms.bash - produced the zTPC and nSigma distributions from the skimmed data"
    echo "    USAGE:"
    echo "        ./runMakeYieldHistograms.bash [OPTION] ... [collisionEnergy] [skimDataFile] [yieldHistoFile]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        collisionEnergy  - floating point number for the center-of-mass energy"
    echo "        skimDataFile     - path to and name of the root file containing the skimmed data"
    echo "        yieldHistoFile   - path to and name of the root file that will be created to contain"
    echo "                           the resulting yield histograms"
    echo "    OPTIONS:"
    echo "        -h - print this help information"
    echo ""
}

#Check for Options                                                                                                 
while [ "$#" -gt 0 ]; do
    while getopts "h" opts; do
        case "$opts" in
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi

done

#Assign user inputs to variables                                                                                   
energy=${POSITIONALPARAM[0]}
skimFile=${POSITIONALPARAM[1]}
outFile=${POSITIONALPARAM[2]}

#Path and name of the root file containg the Bichsel Curves
pidFile=../src/PIDFunctions.root

#Check for all three arguments
if [ "${#POSITIONALPARAM[@]}" -ne 3 ]
then
    echo "ERROR: Requires three parameters: energy, inputfile, outputfile"
    exit 1
fi

#Check for a good energy
if [[ "$energy" != "3.0" && "$energy" != "3.5" && "$energy" != "4.5" ]] 
then
    echo "ERROR: Incorrect Energy! Must be either 3.0, 3.5, or 4.5"
    exit 1
fi

echo "Running runMakeYieldHistograms.C for $energy GeV and input skim file $skimFile ."

time root -l -b -q ../macros/runMakeYieldHistograms.C\(\"$skimFile\",\"$outFile\",\"$pidFile\",$energy\)

#Check that the outFile exists and is non zero size
if [ ! -s $outFile ]
then
    echo "ERROR: The $outFile doesn't exist or has non-zero size."
    exit 1
fi

exit 0
