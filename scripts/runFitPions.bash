#!/bin/bash

#Help
help_func(){
    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        runFitPions.bash - extracts the raw spectra of positive and negative pions"
    echo "    USAGE:" 
    echo "        ./runFitPions.bash [OPTION] ... [collisionEnergy] [detectorSide] [yieldHistoFile] [spectraFile]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        collisionEnergy  - floating point number for the center-of-mass energy"
    echo "        detectorSide     - integer for which side of the detector: 0-PosY, 1-NegY"
    echo "        yieldHistoFile   - path to and name of root file containing the yield histograms"
    echo "        spectraFile      - path to and name of root file that will contain the spectra"
    echo "                           if it exists it will be updated."
    echo "    OPTIONS:"
    echo "        -y - only do a specific rapidity index (must be an integer)"
    echo "             mid rapidity indexes: 15,17,20 for 3.0,3.5,4.5 GeV respectively"
    echo "        -d - save images to this directory (if it does not exist it will be created)"
    echo "             if no directory is specified then no images will be saved"
    echo "        -h - print this help information"
    echo ""
}

#Check for Options
while [ "$#" -gt 0 ]; do
    while getopts "y:d:h" opts; do
	case "$opts" in
	    y) rapidityIndex="${OPTARG}"; shift;;
	    d) imageDir="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
	    ?) exit 1;;
	    *) echo "For help use option: -h"; exit 1;;
	esac
	shift
	OPTIND=1
    done
    
    if [ "$#" -gt 0 ]; then
	POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
	shift
	OPTIND=1
    fi

done

#Assign user inputs to variables
energy=${POSITIONALPARAM[0]}
side=${POSITIONALPARAM[1]}
inputFile=${POSITIONALPARAM[2]}
outputFile=${POSITIONALPARAM[3]}

#Check to make sure the minimum arguments are fullfilled
if [ "${#POSITIONALPARAM[@]}" -ne 4 ]; then
    echo "ERROR: Requires at least 4 parameters."
    echo "       For help use option: -h"
    exit 1
fi

#Check for a good energy
if [[ "$energy" != "3.0" && "$energy" != "3.5" && "$energy" != "4.5" ]]; then
    echo "ERROR: Incorrect Energy! Must be either 3.0, 3.5, or 4.5"
    exit 1
fi

#Check for a good side
if [[ "$side" != "0" && "$side" != "1" ]]; then
    echo "ERROR: Incorrect detector side! Must be either 0 for PosY or 1 for NegY"
    exit 1
fi

#Check that in inputfile exists
if [ ! -s $inputfile ]; then
    echo "ERROR: $inputfile does not exist"
    exit 1
fi

#If the imageDir is not empty check if it exists, if not make it
if [ ! -z $imageDir ] && [ ! -d $imageDir ]; then
    echo "Creating Image Directory: $imageDir"
    mkdir $imageDir
fi

#If the rapidityIndex is empty then set it to -1 which means to 
#do all the rapidity bins
if [ -z $rapidityIndex ]; then
    rapidityIndex=-1
fi

#Run the code
root -l -b -q ../macros/runFitPions.C\(\"$inputFile\",\"$outputFile\",$energy,$side,$rapidityIndex,\"$imageDir\"\)

#Check that the spectra output file exists and has non zero size
if [ ! -s $outputfile ]; then
    echo "ERROR: $outputfile either does not exist or has non-zero size."
    exit 1
fi

exit 0
