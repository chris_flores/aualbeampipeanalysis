#!/bin/bash

#Creates a text file (.data) with the mTm0 and energy loss correction value

#Arguments
#    correctionFile - ROOT File containing the correction curves
#    outFileDir     - directory to save the text file to
#    pid            - integer for which particle species
#                     (0-PionPlus,4-Proton)
#    side           - integer for which side of the detector
#                     (0-PosY,1-NegY)
#    rapidityIndex  - integer for which rapidity bin to be printed
#                     (yMid is 15,17,20 for 3.0, 3.5, 4.5 GeV respectively)

corrFile=$1
outFileDir=$2
pid=$3
side=$4
rapidityIndex=$5

#Make Sure there are 5 arguments
if [ "$#" -ne 5 ]; then
    echo "ERROR: Incorrect number of arguments!"
    exit 1
fi

root -l -q -b ../macros/printEnergyLossCurve.C\(\"$corrFile\",\"$outFileDir\",$pid,$side,$rapidityIndex\)

exit 0