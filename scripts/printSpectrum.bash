#!/bin/bash

#Creates a text file (.data) with the mTm0, yield, and yield Error.

#Arguments:
#    spectraFile   - ROOT file containing the spectrum to be printed
#    outFileDir    - directory to save the text file to
#    pid           - integer for which particle species
#                    (0-PionPlus,1-PionMinus,4-Proton)
#    side          - integer for which side of the detector
#                    (0-PosY,1-NegY)
#    rapidityIndex - integer for which rapidity bin to be printed
#                    (yMid is 15,17,20 for 3.0, 3.5, 4.5 GeV Respectively)
#    mTm0Min       - (OPTIONAL) float for lowest permitted mT-m0 in spectrum
#    mTm0Max       - (OPTIONAL) float for highest permitted mT-m0 in spectrum
#    type          - (OPTIONAL) integer for type of spectrum (DEFAULT is raw)
#                    (0-Raw,1-Corrected [if it exisits in spectraFile] )

spectraFile=$1
outFileDir=$2
pid=$3
side=$4
rapidityIndex=$5

mTm0Min=$6
mTm0Max=$7
type=$8

if [ "$#" -ne 5 -a "$#" -ne 8 ]
then
    echo "Incorrect number of arguments!"
    exit 1
fi

if [ "$#" -eq 5 ]
then
    root -l -q -b ../macros/printSpectrum.C\(\"$spectraFile\",\"$outFileDir\",$pid,$side,$rapidityIndex\)
fi

if [ "$#" -eq 8 ]
then
    root -l -q -b ../macros/printSpectrum.C\(\"$spectraFile\",\"$outFileDir\",$pid,$side,$rapidityIndex,$mTm0Min,$mTm0Max,$type\)
fi


