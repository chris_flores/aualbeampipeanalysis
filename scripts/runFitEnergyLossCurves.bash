#!/bin/bash

#Help
help_func(){
    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        runFitEnergyLossCurves.bash - fit the embedding efficiency graphs with curves"
    echo "    USAGE:"
    echo "        ./runFitEnergyLossCurves.bash [OPTION] ... [collisionEnergy] [embeddingFile] [correctionFile] [pid] [detectorSide]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        collisionEnergy  - floating point number for the center-of-mass energy"
    echo "        embeddingFile    - path to and name of the file containing the effieiency graphs to be fit"
    echo "        correctionFile   - path to and name of the file containing the efficiency curves"
    echo "                           (will be created by running this script)"
    echo "        pid              - integer for the particle id (not all particles are enabled)"
    echo "                           (0-PionPlus,1-PionMinus,2-KaonPlus,3-KaonMinus,4-Proton,5-AntiProton)"
    echo "        detectorSide     - integer for which side of the detector"
    echo "                           (0-PosY,1-NegY)"
    echo "    OPTIONS:"
    echo "        -d - save images to this directory (if it does not exist it will be created)"
    echo "        -h - display this help information"
    echo ""
}

#Check for Options
imageDir=""
while [ "$#" -gt 0 ]; do
    while getopts "y:d:h" opts; do
        case "$opts" in
            d) imageDir="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done
    
    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Assign user inputs to variables
energy=${POSITIONALPARAM[0]}
embFile=${POSITIONALPARAM[1]}
corrFile=${POSITIONALPARAM[2]}
pid=${POSITIONALPARAM[3]}
side=${POSITIONALPARAM[4]}

#Check to make sure the minimum number of arguments are fullfilled
if [ "${#POSITIONALPARAM[@]}" -ne 5 ]; then
    echo "ERROR: Requires at least 5 parameters"
    echo "       For help use option: -h"
    exit 1
fi

#Check for a good energy
if [[ "$energy" != "3.0" && "$energy" != "3.5" && "$energy" != "4.5" ]]; then
    echo "ERROR: Incorrect Energy! Must be either 3.0, 3.5, or 4.5"
    exit 1
fi

#Check for a good side
if [[ "$side" != "0" && "$side" != "1" ]]; then
    echo "ERROR: Incorrect detector side! Must be either 0 for PosY or 1 for NegY"
    exit 1
fi

#Check that in embedding file exists
if [ ! -s $embFile ]; then
    echo "ERROR: $embFile does not exist"
    exit 1
fi

#Check to make sure the image directory exists
#if it doesn't and if it is set to something then create it
if [ ! -z $imageDir ] && [ ! -d $imageDir ]; then
    echo "Creating Image Directory: $imageDir"
    mkdir $imageDir
fi

#Run the code
root -l -b -q ../macros/runFitEnergyLossCurves.C\(\"$embFile\",\"$corrFile\",$energy,$pid,$side,\"$imageDir\"\)

exit 0
