#!/bin/bash

#Creates a text file (.data) of the Pion Ratio for a given rapidity bin

#Arguments:
#    spectraFile   - ROOT file containing the spectra and pion ratio
#    outFileDir    - directory to save the text file to
#    side          - integer for which side of the detector 
#                    (0-PosY,1-NegY)
#    rapidityIndex - integer for which rapidity bin to be printed
#                    (yMid is 15,17,20 for 3.0, 3.5, 4.5 respectively)

spectraFile=$1
outFileDir=$2
side=$3
rapidityIndex=$4

if [ "$#" -ne 4 ]; then
    echo "ERROR: Incorrect number of arguments."
    echo "    This script requires 4: <spectraFile> <outFileDir> <side> <rapidityIndex>"
    exit 1
fi

root -l -b -q ../macros/printPionRatio.C\(\"$spectraFile\",\"$outFileDir\",$side,$rapidityIndex\)

exit 0