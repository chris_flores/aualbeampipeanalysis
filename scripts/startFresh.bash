#!/bin/bash

#######################################################################
####    Delete all files created by superman mode
####
####    Simply pass in the floating point number for the energy
#######################################################################

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        startFresh.bash - delete all the files created by superman mode"
    echo "    USAGE:"
    echo "        ./startFresh.bash [OPTION] ... [configurationFile]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configurationFile  - name of the configuration file for this energy"
    echo "    OPTIONS:"
    echo "        -h - print this help information"
    echo "        -s - do not delete the skim file."
    echo "        -y - do not delete the yield histogram file"
    echo "        -c - do not delete the correction file. This is useful if you have already"
    echo "             run supermanMode succesfully and do not want wait for the efficiency"
    echo "             and energy loss curves to be regenerated (which is the longest part)."
    echo "    NOTES:"
    echo "        This script gets its variables from the user's configuration file"
    echo ""

}

removeSkim=true
removeYield=true
removeCorr=true

#Check for options
while [ "$#" -gt 0 ]; do
    while getopts ":sych" opts; do
        case "$opts" in
	    s) removeSkim=false;;
            y) removeYield=false;;
	    c) removeCorr=false;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure there is only one argument
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: You must pass one argument: <configurationFile>"
    echo "       Please pass the name of the config file for this energy."
    exit 1
fi

#Get the configuration file
configFile=${POSITIONALPARAM[0]}

#Get the configuration from the config file 
source $configFile

#Delete Each of the outputs if they exist
if [ -e $skimFile -a "$removeSkim" = true ]; then
    rm $skimFile
fi 

if [ -e $yieldFile -a "$removeYield" = true ]; then
    rm $yieldFile
fi

if [ -e $spectraFile ]; then
    rm $spectraFile
fi

if [ -e $correctionFile -a "$removeCorr" = true ]; then
    rm $correctionFile
fi

#Remove text data files
find $energyDir -type f -name "*.data*" -delete

#Remove Images
find $energyDir/Images/ZTPCFits/ -type f -delete
find $energyDir/Images/Efficiency/ -type f -delete
find $energyDir/Images/EnergyLoss/ -type f -delete
find $energyDir/Images/Spectra/ -type f -delete

#Remove Logs
find logs/ -type f -name "*$energy.log" -delete