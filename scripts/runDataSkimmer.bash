#!/bin/bash

#Help
help_func(){
    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        runDataSkimmer.bash - applies the final event and track cuts to the raw data file"
    echo "    USAGE:"
    echo "        ./runDataSkimmer.bash [OPTION] ... [collisionEnergy] [rawDataFile] [skimDataFile]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        collisionEnergy  - floating point number for collision energy"
    echo "        rawDataFile      - path to and name of the root file containing the raw data"
    echo "        skimDataFile     - path to and name of the root file that will be created to"
    echo "                           contain the resulting skimmed data"
    echo "    OPTIONS:"
    echo "        -h - print this help information"
    echo ""
}


#Check for Options                                                                                                 
while [ "$#" -gt 0 ]; do
    while getopts "y:d:h" opts; do
        case "$opts" in
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi

done

#Assign user inputs to variables                                                                                   
energy=${POSITIONALPARAM[0]}
inputfile=${POSITIONALPARAM[1]}
outputfile=${POSITIONALPARAM[2]}

#Check for all three arguments
if [ "${#POSITIONALPARAM[@]}" -ne 3 ]
then
    echo "ERROR: Requires three parameters: energy, inputfile, outputfile"
    exit 1
fi

#Check for a good energy
echo $energy
if [[ "$energy" != "3.0" && "$energy" != "3.5" && "$energy" != "4.5" ]] 
then
    echo "ERROR: Incorrect Energy! Must be either 3.0, 3.5, or 4.5"
    exit 1
fi

echo "Running runDataSkimmer.C for energy $energy GeV and input file $inputfile"

time root -l -b -q ../macros/runDataSkimmer.C\(\"$inputfile\",\"$outputfile\",$energy\)

#Check to make sure the output file exists and has non zero size
if [ ! -s $outputfile ]
then
    echo "ERROR: $outputfile either doesn't exist or has non-zero size."
    exit 1
fi 

exit 0
