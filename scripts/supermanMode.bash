#!/bin/bash

#This script runs steps 2-8 from README.txt. 

help_func(){

    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        supermanMode.bash - runs steps 2 through 9 in as outlined in the README"
    echo "    USAGE:"
    echo "        ./supermanMode.bash [OPTION] ... [configurationFile]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        configurationFile  - name of configuration file for this run" 
    echo "    OPTIONS:"
    echo "        -h - print this help information"
    echo "        -s - start on this step number"
    echo "        -t - stop on this step number"
    echo "        -o - only do this step number. This takes precedence over s and t options."
    echo "        -y - only do this rapidity bin index."
    echo "             by default only the mid rapidity index is done"
    echo "             to do the full rapidity range use -y -1"
    echo "    NOTES:"
    echo "        This script gets its variables from the user configuration file."
    echo ""

}


#Check for Options
while [ "$#" -gt 0 ]; do
    while getopts "o:s:t:y:h" opts; do
        case "$opts" in
	    s) startStep="${OPTARG}"; shift;;
	    t) stopStep="${OPTARG}"; shift;;
	    o) onlyStep="${OPTARG}"; startStep=$onlyStep; stopStep=$onlyStep; shift;;
            y) rapidityIndex="${OPTARG}"; shift;;
	    h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done
    
    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#Make sure only one argument remains after all the options
if [ "${#POSITIONALPARAM[@]}" -ne 1 ]; then
    echo "ERROR: This script requires only one argument. For usage information use option -h."
    echo "       If you have used options check your formating."
    exit 1
fi

#Assign user inputs to variables
configFile=${POSITIONALPARAM[0]}

#Make Sure the configuration variable is set
if [ -z $configFile ]; then
    echo "ERROR: Configuration file was not set. For usage information use option -h"
    exit 1
fi

#Make Sure the configuration file exists
if [ ! -e $configFile ]; then
    echo "ERROR: Configruation file ($configFile) was not found!"
    exit 1
fi

#Get the Configuration from the Config file
source $configFile

#If the user has specified a start step them make sure it is reasonable
if [ ! -z $startStep ]; then
    if [ $startStep -le 1 ]; then
	echo "ERROR: The starting step must be at least 2 and less than"
	echo "       the total number of steps!"
	exit 1
    fi
fi

#If start step is empty set it to 2 to start at the data skimmer
if [ -z $startStep ]; then
    startStep=2
fi

#If stopStep is empty then set it to 100 to proceed through all the steps
if [ -z $stopStep ]; then
    stopStep=100
fi

#If the user does not specify a rapidity Index then use the mid rapidity
#index specified in the config file
maxRapidityIndex=25
if [ -z $rapidityIndex ]; then
    rapidityIndex=$midRapidity
fi

#Check to Make usre the rapidity index is less than the maximum allowed value
if [ $rapidityIndex -gt $maxRapidityIndex ]; then
    echo "ERROR: The Rapidity Index selected ($rapidityIndex) is greater than $maxRapidityIndex."
    exit 1
fi

#Print the Current Configuration and query the reader for confirmation to contiue
echo "Running Configuration: "
echo "    Energy:           $energy GeV"
echo "    ConfigFile:       $configFile"
echo "    EnergyDir:        $energyDir"
echo "    ImageDir:         $imgDir"
echo "    RawDataFile:      $rawFile"
echo "    SkimDataFile:     $skimFile (will be created)"
echo "    YieldHistoFile:   $yieldFile (will be created)"
echo "    SpectraFile:      $spectraFile (will be created)"
echo "    EmbeddingFile:    $embeddingFile"
echo "    MidYIndex:        $midRapidity"
echo "    DoRapidityIndex:  $rapidityIndex"

read -p "Do you wish to continue (yes/no)? " yn
case $yn in
    [Yy]* ) echo "INITIALIZING SUPERMAN MODE!";;
    [Nn]* ) echo "EXITING!"; exit 1;;
    * ) echo "Please answer yes or no!"; exit 1;;
esac

#Check that Energy Directory Exists
echo "Checking for Energy Directory ($energyDir) .... "
if [ ! -d $energyDir ]; then
    echo "    ERROR: $energyDir does not exist!"
    exit 1
else
    echo "    $energyDir FOUND!"
fi

#Check that Image Directory Exists
echo "Checking for Image Directory ($imgDir) .... "
if [ ! -d $imgDir ]; then
    echo "    ERROR: $imgDir does not exist!"
    exit 1
else
    echo "    $imgDir FOUND!"
fi

#Check that the Data Directory Exists
echo "Checking for Data Directory ($dataDir)"
if [ ! -d $dataDir ]; then
    echo "    ERROR: $dataDir does not exist!"
    exit 1
else
    echo "    $dataDir FOUND!"
fi

#Check that the Raw Data File Exists
if [ $startStep -eq 2 ]; then
    echo "Checking for Raw Data File ($rawFile) ...."
    if [ ! -s $rawFile ]; then
	echo "    ERROR: $rawFile does not exist!"
	exit 1
    else
	echo "    $rawFile FOUND!"
    fi
fi

#Check that the Embedding File Exits
if [ $stopStep -ge 8 ]; then
    echo "Checking for Embedding File ($embeddingFile) ...."
    if [ ! -s $embeddingFile -a ! -z $embeddingFile ]; then
	echo "    WARNING: Embedding File does not exist!"
	read -p "    Would you like to continue without the efficiency and energy loss corrections (yes/no)? " yn
	case $yn in
	    [Yy]* ) echo "Continuing..."; stopStep=7;;
	    [Nn]* ) echo "EXITING!"; exit 1;;
	    * ) echo "Please answer yes or no!"; exit 1;;
	esac   
    else
	echo "    $embeddingFile FOUND!"
    fi
fi

############################################# 
#Step 2: Run the Data Skimmer
if [ $startStep -eq 2 -a $stopStep -ge 2 ]; then

    #Check that the Skim File Doesn't already exist
    if [ -e $skimFile ]; then
	echo "    ERROR: $skimFile already exists! Please delete or rename it."
	exit 1
    fi

    echo "Running the Data Skimmer...."
    ./runDataSkimmer.bash $energy $rawFile $skimFile > logs/runDataSkimmer_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS!"
    startStep=$((startStep+1))
fi

############################################# 
#Step 3: Make the Yield Histograms
if [ $startStep -eq 3 -a $stopStep -ge 3 ]; then

    #Check to Make sure that the Skim File Exists
    if [ ! -e $skimFile ]; then
	echo "    ERROR: The skimmed data file does not exist! Did you run step 2?"
	exit 1
    fi

    #Check that the Yield File Doesn't already exist
    if [ -e $yieldFile ]; then
	echo "    ERROR: $yeidFile already exists! Please delete or rename it."
	exit 1
    fi

    echo "Making the Yield Histograms...."
    ./runMakeYieldHistograms.bash $energy $skimFile $yieldFile > logs/runMakeYieldHistograms_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESSS!"
    startStep=$((startStep+1))
fi

############################################# 
#Step 4: Extract Raw Yield of Pions
if [ $startStep -eq 4 -a $stopStep -ge 4 ]; then
    
    #Check that the Spectra File Doesn't already exist
    if [ -e $spectraFile ]; then
	echo "    ERROR: $spectraFile already exists! Please delete or rename it."
	exit 1
    fi

    echo "Extracting the Raw Yield of Pions"
    ./runFitPions.bash $energy 0 $yieldFile $spectraFile -d $imgDir/ZTPCFits -y $rapidityIndex > logs/runFitPions_PosY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS PosY!"


    ./runFitPions.bash $energy 1 $yieldFile $spectraFile -d $imgDir/ZTPCFits -y $rapidityIndex > logs/runFitPions_NegY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS NegY!"
    startStep=$((startStep+1))
fi

#############################################
#Step 5: Construct the Pion Ratios
if [ $startStep -eq 5 -a $stopStep -ge 5 ]; then
    echo "Constructing the Pion Ratios"
    ./runMakePionRatios.bash -y $rapidityIndex 0 $spectraFile> logs/runMakePionRatios_PosY_$energy.log 2>&1
    if [ "$?" -ne 0 ]; then
	exit 1
    fi
    echo "    SUCCESS PosY!"
    
    ./runMakePionRatios.bash -y $rapidityIndex 1 $spectraFile> logs/runMakePionRatios_NegY_$energy.log 2>&1
    if [ "$?" -ne 0 ]; then
	exit 1
    fi
    echo "    SUCCESS NegY!"
    startStep=$((startStep+1))
fi

#############################################
#Step 6: Extract Raw Yield of Protons
if [ $startStep -eq 6 -a $stopStep -ge 6 ]; then
    echo "Extracting the Raw Yield of Protons"
    ./runFitProtons.bash $energy 0 $yieldFile $spectraFile -d $imgDir/ZTPCFits -y $rapidityIndex > logs/runFitProtons_PosY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS PosY!"
    
    ./runFitProtons.bash $energy 1 $yieldFile $spectraFile -d $imgDir/ZTPCFits -y $rapidityIndex > logs/runFitProtons_NegY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS NegY!"
    startStep=$((startStep+1))
fi

############################################# 
#Step 7: Print the Raw Spectra and Pion ratios
if [ $startStep -eq 7 -a $stopStep -ge 7 ]; then
    echo "Printing the Raw Spectra"

    if [ $rapidityIndex -ge 0 ]; then 
	./printSpectrum.bash $spectraFile $dataDir 0 0 $rapidityIndex > logs/printSpectrum_PionPlus_PosY_$energy.log 2>&1
	./printSpectrum.bash $spectraFile $dataDir 0 1 $rapidityIndex > logs/printSpectrum_PionMinus_NegY_$energy.log 2>&1
	./printSpectrum.bash $spectraFile $dataDir 1 0 $rapidityIndex > logs/printSpectrum_PionPlus_PosY_$energy.log 2>&1
	./printSpectrum.bash $spectraFile $dataDir 1 1 $rapidityIndex > logs/printSpectrum_PionMinus_NegY_$energy.log 2>&1
	./printSpectrum.bash $spectraFile $dataDir 4 0 $rapidityIndex > logs/printSpectrum_Proton_PosY_$energy.log 2>&1
	./printSpectrum.bash $spectraFile $dataDir 4 1 $rapidityIndex > logs/printSpectrum_Proton_NegY_$energy.log 2>&1
	./printPionRatio.bash $spectraFile $dataDir 0 $rapidityIndex > logs/printPionRatio_PosY_$energy.log 2>&1
	./printPionRatio.bash $spectraFile $dataDir 1 $rapidityIndex > logs/printPionRatio_NegY_$energy.log 2>&1
    else
	rapidityIndex=0
	while [ $rapidityIndex -le $maxRapidityIndex ]; do
	    ./printSpectrum.bash $spectraFile $dataDir 0 0 $rapidityIndex >> logs/printSpectrum_PionPlus_PosY_$energy.log 2>&1
            ./printSpectrum.bash $spectraFile $dataDir 0 1 $rapidityIndex >> logs/printSpectrum_PionMinus_NegY_$energy.log 2>&1
	    ./printSpectrum.bash $spectraFile $dataDir 1 0 $rapidityIndex >> logs/printSpectrum_PionPlus_PosY_$energy.log 2>&1
            ./printSpectrum.bash $spectraFile $dataDir 1 1 $rapidityIndex >> logs/printSpectrum_PionMinus_NegY_$energy.log 2>&1
	    ./printSpectrum.bash $spectraFile $dataDir 4 0 $rapidityIndex >> logs/printSpectrum_Proton_PosY_$energy.log 2>&1
	    ./printSpectrum.bash $spectraFile $dataDir 4 1 $rapidityIndex >> logs/printSpectrum_Proton_NegY_$energy.log 2>&1
	    ./printPionRatio.bash $spectraFile $dataDir 0 $rapidityIndex >> logs/printPionRatio_PosY_$energy.log 2>&1
	    ./printPionRatio.bash $spectraFile $dataDir 1 $rapidityIndex >> logs/printPionRatio_NegY_$energy.log 2>&1
	    rapidityIndex=$((rapidityIndex+1))
	done
    fi    

    echo "You should check to make sure all the spectra printed"
    startStep=$((startStep+1))
fi

############################################# 
#Step 8: Fit the Efficiency Graphs
if [ $startStep -eq 8 -a $stopStep -ge 8 ]; then
    echo "Fitting the Efficiency Curves"
    ./runFitEfficiencyCurves.bash $energy $embeddingFile $correctionFile 0 0 -d $imgDir/Efficiency > logs/runFitEfficiencyCurves_PionPlus_PosY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS! Pion PosY"
    
    ./runFitEfficiencyCurves.bash $energy $embeddingFile $correctionFile 0 1 -d $imgDir/Efficiency > logs/runFitEfficiencyCurves_PionPlus_NegY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS! Pion Negy"
    
    ./runFitEfficiencyCurves.bash $energy $embeddingFile $correctionFile 4 0 -d $imgDir/Efficiency > logs/runFitEfficiencyCurves_Proton_PosY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS! Proton PosY"
    
    ./runFitEfficiencyCurves.bash $energy $embeddingFile $correctionFile 4 1 -d $imgDir/Efficiency > logs/runFitEfficiencyCurves_Proton_NegY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS! Proton NegY"
    startStep=$((startStep+1))
fi

#############################################
#Step 9: Fit the Energy Loss Graphs
if [ $startStep -eq 9 -a $stopStep -ge 9 ]; then
    echo "Fitting the Energy Loss Curves (This could take several minutes!)"

    ./runFitEnergyLossCurves.bash $energy $embeddingFile $correctionFile 0 0 -d $imgDir/EnergyLoss > logs/runFitEnergyLossCurves_PionPlus_PosY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
        exit 1
    fi
    echo "    SUCCESS! Pion PosY"

    ./runFitEnergyLossCurves.bash $energy $embeddingFile $correctionFile 0 1 -d $imgDir/EnergyLoss > logs/runFitEnergyLossCurves_PionPlus_NegY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
        exit 1
    fi
    echo "    SUCCESS! Pion NegY"
    
    ./runFitEnergyLossCurves.bash $energy $embeddingFile $correctionFile 4 0 -d $imgDir/EnergyLoss > logs/runFitEnergyLossCurves_Proton_PosY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS! Proton PosY"
    
    ./runFitEnergyLossCurves.bash $energy $embeddingFile $correctionFile 4 1 -d $imgDir/EnergyLoss > logs/runFitEnergyLossCurves_Proton_NegY_$energy.log 2>&1
    if [ "$?" -ne "0" ]; then
	exit 1
    fi
    echo "    SUCCESS! Proton NegY"

    #Print the curves
    ./printEnergyLossCurves.bash $correctionFile $dataDir 0 0 $rapidityIndex > logs/printEnergyLossCurves_PionPlus_PosY_$energy.log 2>&1
    ./printEnergyLossCurves.bash $correctionFile $dataDir 0 1 $rapidityIndex > logs/printEnergyLossCurves_PionPlus_NegY_$energy.log 2>&1
    ./printEnergyLossCurves.bash $correctionFile $dataDir 4 0 $rapidityIndex > logs/printEnergyLossCurves_Proton_PosY_$energy.log 2>&1
    ./printEnergyLossCurves.bash $correctionFile $dataDir 4 1 $rapidityIndex > logs/printEnergyLossCurves_Proton_NegY_$energy.log 2>&1

    startStep=$((startStep+1))
fi

    echo "FINISHED...EXITING SUPERMAN MODE!"
    
    exit 0
