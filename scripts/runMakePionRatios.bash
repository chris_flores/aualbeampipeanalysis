#!/bin/bash

#Help
help_func(){
    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        runMakePionRatios.bash - extracts the raw spectra of positive and negative pions"
    echo "    USAGE:"
    echo "        ./runMakePionRatios.bash [OPTION] ... [detectorSide] [spectraFile]"
    echo "    REQUIRED ARGUMENTS:"
    echo "        detectorSide     - integer for which side of the detector: 0-PosY, 1-NegY"
    echo "        spectraFile      - path to and name of root file that will contain the spectra"
    echo "                           if it exists it will be updated."
    echo "    OPTIONS:"
    echo "        -y - only do a specific rapidity index (must be an integer)"
    echo "             mid rapidity indexes: 15,17,20 for 3.0,3.5,4.5 GeV respectively"
    echo "        -h - print this help information"
    echo ""
}

#Check for Options
while [ "$#" -gt 0 ]; do
    while getopts "y:h" opts; do
        case "$opts" in
            y) rapidityIndex="${OPTARG}"; shift;;
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi

done

#Assign user inputs to variables
side=${POSITIONALPARAM[0]}
inputFile=${POSITIONALPARAM[1]}

#Check to make sure the minimum arguments are fullfilled
if [ "${#POSITIONALPARAM[@]}" -ne 2 ]; then
    echo "ERROR: Incorrect number of arguments. This script requires 2."
    echo "       For more information please use option: -h"
    exit 1
fi

#Check for a good side
if [[ "$side" != "0" && "$side" != "1" ]]; then
    echo "ERROR: Incorrect detector side! Must be either 0 for PosY or 1 for NegY"
    exit 1
fi

#Check that in inputfile exists
if [ ! -s $inputfile ]; then
    echo "ERROR: $inputfile does not exist"
    exit 1
fi

#If the rapidityIndex is empty then set it to -1 which means to
#do all the rapidity bins
if [ -z $rapidityIndex ]; then
    rapidityIndex=-1
fi

#Run the code
root -l -b -q ../macros/runMakePionRatios.C\(\"$inputFile\",$side,$rapidityIndex\)

exit 0