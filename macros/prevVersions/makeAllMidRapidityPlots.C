/****************************************************************************
 **
 **
 **
 **
 **
 **
 **Author: Christopher E. Flores (UC Davis)
 **Email:  chrflores@ucdavis.edu
 **
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "../libs/includeList.h"
#include "utilityFunctions.h"

// --- Functions ---
void drawMidRapiditySpectra();
void loadMidRapiditySpectra();
//Double_t InvertBoseEinstein(Double_t Amp, Double_t Slope, Double_t pMass);
//Double_t BoseEinsteinFitFunc(Double_t *x, Double_t *par);
Double_t InvertBoseEinstein(Double_t Slope, Double_t pMass);
void makePionRatio();
Double_t pi_rat_red(Double_t *x, Double_t *par);

// -- Global Scope Variables
TFile *spectra30, *spectra35, *spectra45;
Int_t pidArray[4] = {0,1,4,5};
TString sideRap[2] = {"_PosY","_NegY"};
TString sideName[2] = {"_Left","_Right"};
TString energyName[3] = {"_3_0","_3_5","_4_5"};

Double_t energy;
//Int_t energyIndex;

//Variables for makeMidRapiditySpectra()
TGraphErrors *effSpectra30[2][3];
TGraphErrors *effSpectra35[2][3];
TGraphErrors *effSpectra45[2][3];

//Pion Ratio Fit Function
TF1 *coulombFitFunc[2][3]; //sideIndex, energyIndex


//Spectra Fit Functions
TF1 *spectraFitFunc[3][2][3]; //energies, sides, particles


// --- Main Function ---
void makeMidrapidityPlots(){

  spectra30 = new TFile("AuAl_3_0/AuAl_3_0_CoulombSpectra.root","READ");
  spectra35 = new TFile("AuAl_3_5/AuAl_3_5_CoulombSpectra.root","READ");
  spectra45 = new TFile("AuAl_4_5/AuAl_4_5_CoulombSpectra.root","READ");

  //Load the MidY Spectra
  loadMidRapiditySpectra();

  //Draw the Mid Rapidity Spectra on the Same Canvas
  drawMidRapiditySpectra();

  //Make and Fit the Pion Ratios
  makePionRatio();


}

//______________________________________________________________________
void drawMidRapiditySpectra(){

  //Create Four Canvases...one for each side
  TCanvas *midRapidityPionCanvas0 = new TCanvas("LeftMidYPionCanvas",
						"Positive Rapidity Spectra",20,20,600,500);
  TCanvas *midRapidityPionCanvas1 = new TCanvas("RightMidYPionCanvas",
					    "Negative Rapidity Spectra",650,20,600,500);

  TCanvas *midRapidityProtonCanvas0 = new TCanvas("LeftMidYProtonCanvas",
					    "Positive Rapidity Spectra",20,550,600,500);
  TCanvas *midRapidityProtonCanvas1 = new TCanvas("RightMidYProtonCanvas",
					    "Negative Rapidity Spectra",650,550,600,500);

  midRapidityPionCanvas0->SetLogy();
  midRapidityPionCanvas1->SetLogy();
  midRapidityProtonCanvas0->SetLogy();
  midRapidityProtonCanvas1->SetLogy();

  //Create Frames for Each Canvas
  midRapidityPionCanvas0->cd();
  TH1F *frame0 = midRapidityPionCanvas0->DrawFrame(0,.005,.805,1500);
  frame0->SetTitle("");
  frame0->SetXTitle("m_{T}-m_{0} (GeV)");
  frame0->SetYTitle("#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy}");
  frame0->GetYaxis()->SetTitleOffset(1.3);

  midRapidityPionCanvas1->cd();
  TH1F *frame1 = midRapidityPionCanvas1->DrawFrame(0,.005,.805,1500);
  frame1->SetTitle("");
  frame1->SetXTitle("m_{T}-m_{0} (GeV)");
  frame1->SetYTitle("#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy}");
  frame1->GetYaxis()->SetTitleOffset(1.3);

  midRapidityProtonCanvas0->cd();
  TH1F *frame2 = midRapidityProtonCanvas0->DrawFrame(0,.01,1.05,50);
  frame2->SetTitle("");
  frame2->SetXTitle("m_{T}-m_{0} (GeV)");
  frame2->SetYTitle("#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy}");
  frame2->GetYaxis()->SetTitleOffset(1.3);

  midRapidityProtonCanvas1->cd();
  TH1F *frame3 = midRapidityProtonCanvas1->DrawFrame(0,.01,1.05,50);
  frame3->SetTitle("");
  frame3->SetXTitle("m_{T}-m_{0} (GeV)");
  frame3->SetYTitle("#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy}");
  frame3->GetYaxis()->SetTitleOffset(1.3);

  //Make Legends for Each Graph
  Double_t legx1, legx2, legy1, legy2;
  legx1 = .5;
  legx2 = .88;
  legy1 = .6;
  legy2 = .88;

  TLegend *midYLegPion0 = new TLegend(legx1,legy1,legx2,legy2);
  midYLegPion0->SetFillColor(kWhite);
  midYLegPion0->SetBorderSize(0);
  midYLegPion0->SetHeader("Invariant Pion Yield (y_{Evt} > 0)");
  midYLegPion0->AddEntry(effSpectra45[0][0],"#pi^{+} 4.5 GeV","P");
  midYLegPion0->AddEntry(effSpectra45[0][1],"#pi^{-} 4.5 GeV","P");
  midYLegPion0->AddEntry(effSpectra35[0][0],"#pi^{+} 3.5 GeV","P");
  midYLegPion0->AddEntry(effSpectra35[0][1],"#pi^{-} 3.5 GeV","P");
  midYLegPion0->AddEntry(effSpectra30[0][0],"#pi^{+} 3.0 GeV","P");
  midYLegPion0->AddEntry(effSpectra30[0][1],"#pi^{-} 3.0 GeV","P");
  midYLegPion0->AddEntry((TObject*)0,"All with BE Fits","");

  TLegend *midYLegPion1 = new TLegend(legx1,legy1,legx2,legy2);
  midYLegPion1->SetFillColor(kWhite);
  midYLegPion1->SetBorderSize(0);
  midYLegPion1->SetHeader("Invariant Pion Yield (y_{Evt} < 0)");
  midYLegPion1->AddEntry(effSpectra45[1][0],"#pi^{+} 4.5 GeV","P");
  midYLegPion1->AddEntry(effSpectra45[1][1],"#pi^{-} 4.5 GeV","P");
  midYLegPion1->AddEntry(effSpectra35[1][0],"#pi^{+} 3.5 GeV","P");
  midYLegPion1->AddEntry(effSpectra35[1][1],"#pi^{-} 3.5 GeV","P");
  midYLegPion1->AddEntry(effSpectra30[1][0],"#pi^{+} 3.0 GeV","P");
  midYLegPion1->AddEntry(effSpectra30[1][1],"#pi^{-} 3.0 GeV","P");
  midYLegPion1->AddEntry((TObject*)0,"All with BE Fits","");

  TLegend *midYLegProton0 = new TLegend(legx1,legy1,legx2,legy2);
  midYLegProton0->SetFillColor(kWhite);
  midYLegProton0->SetBorderSize(0);
  midYLegProton0->SetHeader("Invariant Proton Yield (y_{Evt} > 0)");
  midYLegProton0->AddEntry(effSpectra45[0][2],"p 4.5 GeV","P");
  midYLegProton0->AddEntry(effSpectra35[0][2],"p 3.5 GeV","P");
  midYLegProton0->AddEntry(effSpectra30[0][2],"p 3.0 GeV","P");
  midYLegProton0->AddEntry((TObject*)0,"All with MB Fits","");

  TLegend *midYLegProton1 = new TLegend(legx1,legy1,legx2,legy2);
  midYLegProton1->SetFillColor(kWhite);
  midYLegProton1->SetBorderSize(0);
  midYLegProton1->SetHeader("Invariant Proton Yield (y_{Evt} < 0)");
  midYLegProton1->AddEntry(effSpectra45[1][2],"p 4.5 GeV","P");
  midYLegProton1->AddEntry(effSpectra35[1][2],"p 3.5 GeV","P");
  midYLegProton1->AddEntry(effSpectra30[1][2],"p 3.0 GeV","P");
  midYLegProton1->AddEntry((TObject*)0,"All with MB Fits","");

  // --- Set the Colors For Each Energy 
  // 3.0 GeV = Green
  effSpectra30[0][0]->SetMarkerColor(kGreen+2);
  effSpectra30[0][1]->SetMarkerColor(kGreen+2);
  effSpectra30[0][2]->SetMarkerColor(kGreen+2);
  effSpectra30[1][0]->SetMarkerColor(kGreen+2);
  effSpectra30[1][1]->SetMarkerColor(kGreen+2);
  effSpectra30[1][2]->SetMarkerColor(kGreen+2);

  // 3.5 GeV = Blue
  effSpectra35[0][0]->SetMarkerColor(kBlue);
  effSpectra35[0][1]->SetMarkerColor(kBlue);
  effSpectra35[0][2]->SetMarkerColor(kBlue);
  effSpectra35[1][0]->SetMarkerColor(kBlue);
  effSpectra35[1][1]->SetMarkerColor(kBlue);
  effSpectra35[1][2]->SetMarkerColor(kBlue);

  // 4.5 GeV = Red
  effSpectra45[0][0]->SetMarkerColor(kRed);
  effSpectra45[0][1]->SetMarkerColor(kRed);
  effSpectra45[0][2]->SetMarkerColor(kRed);
  effSpectra45[1][0]->SetMarkerColor(kRed);
  effSpectra45[1][1]->SetMarkerColor(kRed);
  effSpectra45[1][2]->SetMarkerColor(kRed);

  //Draw All the Pion Spectra for the Left Side
  midRapidityPionCanvas0->cd();
  effSpectra30[0][0]->Draw("P");
  effSpectra30[0][1]->Draw("P");
  effSpectra35[0][0]->Draw("P");
  effSpectra35[0][1]->Draw("P");
  effSpectra45[0][0]->Draw("P");
  effSpectra45[0][1]->Draw("P");
  midYLegPion0->Draw();

  //Draw All the Pion Spectra for the Right Side
  midRapidityPionCanvas1->cd();
  effSpectra30[1][0]->Draw("P");
  effSpectra30[1][1]->Draw("P");
  effSpectra35[1][0]->Draw("P");
  effSpectra35[1][1]->Draw("P");
  effSpectra45[1][0]->Draw("P");
  effSpectra45[1][1]->Draw("P");
  midYLegPion1->Draw();

  //Draw All the Proton Spectra for the Left Side
  midRapidityProtonCanvas0->cd();
  effSpectra30[0][2]->Draw("P");
  effSpectra35[0][2]->Draw("P");
  effSpectra45[0][2]->Draw("P");
  midYLegProton0->Draw();

  //Draw All the Proton Spectra for the Right Side
  midRapidityProtonCanvas1->cd();
  effSpectra30[1][2]->Draw("P");
  effSpectra35[1][2]->Draw("P");
  effSpectra45[1][2]->Draw("P");
  midYLegProton1->Draw();

  //Define Bose Einstein Functions to fit the Spectra
  for (Int_t energyIndex=0; energyIndex<3; energyIndex++){
    for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
      for (Int_t pIndex=0; pIndex<3; pIndex++){

	
	// --- Bose Einstein ---
	//Define a Bose-Einstein Function for Fitting
	//[0] = Normalization Constant
	//[1] = Slope Parameter
	//[2] = Particle Mass
	// spectraFitFunc[energyIndex][sideIndex][pIndex] = new
	//   TF1(GetParticleName(pidArray[pIndex])+sideRap[sideIndex]+
	//       energyName[energyIndex]+"_midY_Fit",
	//       "(3.0/(3.14*3.14*3.14))*([0]/([1]*[1]))*(1.0/(exp((x+[2])/[1])-1.0))",0,1);

	// spectraFitFunc[energyIndex][sideIndex][pIndex] = new
	//   TF1(GetParticleName(pidArray[pIndex])+sideRap[sideIndex]+
	//       energyName[energyIndex]+"_midY_Fit",
	//       "[0]*(1.0/(exp((x)/[1])-1.0))",0,1);


	// //Fix the Parameter for the Particle Mass
	// spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//   FixParameter(2,GetParticleMass(pidArray[pIndex]));

	// //Set The Slope Parameter
	// spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//   SetParameter(1,.08);

	//Initialize the Parameters
	//spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//SetParameter(0,6.0);

	// spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//   SetParameter(0,InvertBoseEinstein(spectraFitFunc[energyIndex][sideIndex][pIndex]->
	// 				    GetParameter(1),
	// 				    GetParticleMass(pidArray[pIndex])));
	



	// spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//   SetParameter(0,6);
	// spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//   SetParameter(1,.08);
	// spectraFitFunc[energyIndex][sideIndex][pIndex]->
	//   FixParameter(2,GetParticleMass(pidArray[pIndex]));

	//If the Particle is a Pion use a Bose Einstein
	if (pidArray[pIndex] == 0 || pidArray[pIndex] == 1){
	  spectraFitFunc[energyIndex][sideIndex][pIndex] = new
	    TF1(GetParticleName(pidArray[pIndex])+sideRap[sideIndex]+
		energyName[energyIndex]+"_midY_Fit",
		BoseEinsteinFitFunc,0.01,.6,3);
	}

	//If the Particle is a Proton Use a Single Temperature Thermal Fit
	if (pidArray[pIndex] == 4){
	  spectraFitFunc[energyIndex][sideIndex][pIndex] = new
	    TF1(GetParticleName(pidArray[pIndex])+sideRap[sideIndex]+
		energyName[energyIndex]+"_midY_Fit",
		ThermalFitFunc,0.01,.6,3);
	}

	spectraFitFunc[energyIndex][sideIndex][pIndex]->
	  SetParameter(0,6);
	spectraFitFunc[energyIndex][sideIndex][pIndex]->
	  SetParameter(1,.08);
	spectraFitFunc[energyIndex][sideIndex][pIndex]->
	  FixParameter(2,GetParticleMass(pidArray[pIndex]));



	spectraFitFunc[energyIndex][sideIndex][pIndex]->
	  SetParLimits(0,0,100);
	spectraFitFunc[energyIndex][sideIndex][pIndex]->
	  SetParLimits(1,0,.3);

	//Set the Line Width
	spectraFitFunc[energyIndex][sideIndex][pIndex]->SetLineWidth(4);

	//Set the Line Color
	if (energyIndex == 0)
	  spectraFitFunc[energyIndex][sideIndex][pIndex]->SetLineColor(kGreen+2);
	else if (energyIndex == 1)
	  spectraFitFunc[energyIndex][sideIndex][pIndex]->SetLineColor(kBlue);
	else if (energyIndex == 2)
	  spectraFitFunc[energyIndex][sideIndex][pIndex]->SetLineColor(kRed);

	//Set the Line Style (Only the Pion Minus need to be changed to dashed)
	if (pIndex == 1)
	  spectraFitFunc[energyIndex][sideIndex][pIndex]->SetLineStyle(7);

      }
    }
  }


  // --- Define the Fitting Range For Each Function ---
  
  //The Left Pion Canvas Functions
  spectraFitFunc[0][0][0]->SetRange(.1,.5);
  spectraFitFunc[0][0][1]->SetRange(.1,.6);
  spectraFitFunc[1][0][0]->SetRange(.1,.5);
  spectraFitFunc[1][0][1]->SetRange(.1,.6);
  spectraFitFunc[2][0][0]->SetRange(.1,.325);
  spectraFitFunc[2][0][1]->SetRange(.1,.6);

  //The Right Pion Canvas Functions
  spectraFitFunc[0][1][0]->SetRange(.1,.5);
  spectraFitFunc[0][1][1]->SetRange(.1,.6);
  spectraFitFunc[1][1][0]->SetRange(.1,.45);
  spectraFitFunc[1][1][1]->SetRange(.1,.6);
  spectraFitFunc[2][1][0]->SetRange(.1,.325);
  spectraFitFunc[2][1][1]->SetRange(.1,.6);

  //The Left Proton Canvas Functions
  spectraFitFunc[0][0][2]->SetRange(.350,.9);
  spectraFitFunc[1][0][2]->SetRange(.35,.9);
  spectraFitFunc[2][0][2]->SetRange(.625,.8);

  //The Right Proton Canvas Functions
  spectraFitFunc[0][1][2]->SetRange(.350,1.0);
  spectraFitFunc[1][1][2]->SetRange(.425,1.0);
  spectraFitFunc[2][1][2]->SetRange(.7,1.0);

  //Fit the Spectra
  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t pIndex=0; pIndex<3; pIndex++){
  
      effSpectra30[sideIndex][pIndex]->
	Fit(spectraFitFunc[0][sideIndex][pIndex],"R");
      effSpectra35[sideIndex][pIndex]->
	Fit(spectraFitFunc[1][sideIndex][pIndex],"R");
      effSpectra45[sideIndex][pIndex]->
	Fit(spectraFitFunc[2][sideIndex][pIndex],"R");
    }
  }
  

  //Update the Canvases
  midRapidityPionCanvas0->Update();
  midRapidityPionCanvas1->Update();
  midRapidityProtonCanvas0->Update();
  midRapidityProtonCanvas1->Update();

  // --- Print the Fit Results
  
  //The Pions
    for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
      cout <<endl <<endl;
      for (Int_t energyIndex=0; energyIndex <3; energyIndex++){
	for (Int_t pIndex=0; pIndex<2; pIndex ++){

	  //The Bose-Einstein Function must first be inverted to find dNdy for each spectra
	  // Double_t dNdy = 
	  //InvertBoseEinstein(spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParameter(0),
	  //		       spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParameter(1),
	  //		       GetParticleMass(pidArray[pIndex]));


	  cout <<"Fit Results for " <<sideRap[sideIndex] 
	       <<GetParticleName(pidArray[pIndex]) <<energyName[energyIndex] <<endl
	       <<Form("dN/dy: %.2f +- %.2f   ", 
		      spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParameter(0),
		      spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParError(0))
	       <<Form("Tslope: %.2f +- %.2f   ", 
		      (spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParameter(1))*1000,
		      (spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParError(1))*1000)
	       <<Form("X2/NDF: %.1f / %d   ", 
		      spectraFitFunc[energyIndex][sideIndex][pIndex]->GetChisquare(),
		      spectraFitFunc[energyIndex][sideIndex][pIndex]->GetNDF())
	       <<endl;
	}
      }
    }

  //The Protons
    for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
      cout <<endl <<endl;
      for (Int_t energyIndex=0; energyIndex <3; energyIndex++){
	Int_t pIndex =2;

	cout <<"Fit Results for " <<sideRap[sideIndex] 
	     <<GetParticleName(pidArray[pIndex]) <<energyName[energyIndex] <<endl
	     <<Form("dN/dy: %.2f +- %.2f   ", 
		    spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParameter(0),
		    spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParError(0))
	     <<Form("Tslope: %.2f +- %.2f   ", 
		    (spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParameter(1))*1000,
		    (spectraFitFunc[energyIndex][sideIndex][pIndex]->GetParError(1))*1000)
	     <<Form("X2/NDF: %.1f / %d   ", 
		    spectraFitFunc[energyIndex][sideIndex][pIndex]->GetChisquare(),
		    spectraFitFunc[energyIndex][sideIndex][pIndex]->GetNDF())
	     <<endl;
	
      }
    }

}


//______________________________________________________________________
void loadMidRapiditySpectra(){

  //Load All the Spectra from the File


  //First the 3.0 GeV Mid Y Spectra
  for (Int_t sideIndex =0; sideIndex < 2; sideIndex++){
    for (Int_t pIndex=0; pIndex <3; pIndex++){
      
      Int_t yIndex = 15;

      effSpectra30[sideIndex][pIndex] = (TGraphErrors*)spectra30->
	Get(GetParticleName(pidArray[pIndex])+"/EffSpectra/"+"effYield"+
	  GetParticleName(pidArray[pIndex])+sideName[sideIndex]+
	    TString::Format("_%02d",yIndex));

	}
  }


  //Second for the 3.5 GeV Mid Y Spectra
  for (Int_t sideIndex =0; sideIndex < 2; sideIndex++){
    for (Int_t pIndex=0; pIndex <3; pIndex++){
      
      Int_t yIndex = 17;

      effSpectra35[sideIndex][pIndex] = (TGraphErrors*)spectra35->
	Get(GetParticleName(pidArray[pIndex])+"/EffSpectra/"+"effYield"+
	    GetParticleName(pidArray[pIndex])+sideName[sideIndex]+
	    TString::Format("_%02d",yIndex));

    }
  }

  //Third for the 4.5 GeV Mid Y Spectra
  for (Int_t sideIndex =0; sideIndex < 2; sideIndex++){
    for (Int_t pIndex=0; pIndex <3; pIndex++){
      
      Int_t yIndex = 20;

      effSpectra45[sideIndex][pIndex] = (TGraphErrors*)spectra45->
	Get(GetParticleName(pidArray[pIndex])+"/EffSpectra/"+"effYield"+
	    GetParticleName(pidArray[pIndex])+sideName[sideIndex]+
	    TString::Format("_%02d",yIndex));

    }
  }


}



//________________________________________________________________________
void makePionRatio(){

  //Define Six new TGraphs to Hold the Ratios
  TGraphErrors *pionRatios[2][3];  //Two Sides, Three Energies

  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t ratioIndex=0; ratioIndex<3; ratioIndex++){

      //Pass Pointers of the Spectra in
      TGraphErrors *effSpectraPlus, *effSpectraMinus;
      if (ratioIndex == 0){
	effSpectraPlus = effSpectra30[sideIndex][0];
	effSpectraMinus = effSpectra30[sideIndex][1];
      }
      else if (ratioIndex == 1){
	effSpectraPlus = effSpectra35[sideIndex][0];
	effSpectraMinus = effSpectra35[sideIndex][1];
      }
      else if (ratioIndex == 2){
	effSpectraPlus = effSpectra45[sideIndex][0];
	effSpectraMinus = effSpectra45[sideIndex][1];
      }

      //Define the TGraphs
      pionRatios[sideIndex][ratioIndex] = new TGraphErrors();
      pionRatios[sideIndex][ratioIndex]->
	SetName("PionRatio"+sideName[sideIndex]+energyName[ratioIndex]);
      pionRatios[sideIndex][ratioIndex]->
	SetMarkerColor(effSpectraPlus->GetMarkerColor());
      pionRatios[sideIndex][ratioIndex]->
	SetMarkerStyle(effSpectraPlus->GetMarkerStyle());

      //Get the Number of Points for Each Graph
      Int_t nPtsPlus = effSpectraPlus->GetN();
      Int_t nPtsMinus = effSpectraMinus->GetN();
      Int_t nPtsRatio = 0;

      //Get the Values from the Spectra
      Double_t *xPlus, *yPlus, *xErrPlus, *yErrPlus;
      Double_t *xMinus, *yMinus, *xErrMinus, *yErrMinus;

      xPlus = effSpectraPlus->GetX();
      yPlus = effSpectraPlus->GetY();
      xErrPlus = effSpectraPlus->GetEX();
      yErrPlus = effSpectraPlus->GetEY();

      xMinus = effSpectraMinus->GetX();
      yMinus = effSpectraMinus->GetY();
      xErrMinus = effSpectraMinus->GetEX();
      yErrMinus = effSpectraMinus->GetEY();


      //Loop Over the Possible mTm0Bins and Compute the Ratio
      for (Int_t mTm0Index = 1; mTm0Index<11; mTm0Index++){

	//Find the Point of the Plus Pion Spectra that matches the mTm0Index
	Int_t ptPlus = -10;
	for (Int_t i=0; i<nPtsPlus; i++){
	  if (xPlus[i] > GetmTm0RangeLow(mTm0Index) &&
	      xPlus[i] < GetmTm0RangeHigh(mTm0Index)){
	    ptPlus = i;
	    break;
	  }	  
	}

	//Find the Point of the Minus Pion Spectra that matches the mTm0Index
	Int_t ptMinus = -10;
	for (Int_t i=0; i<nPtsMinus; i++){
	  if (xMinus[i] > GetmTm0RangeLow(mTm0Index) &&
	      xMinus[i] < GetmTm0RangeHigh(mTm0Index)){
	    ptMinus = i;
	    break;
	  }	  
	}	

	//Skip Points where there is no data
	if (ptPlus == -10 || ptMinus == -10)
	  continue;

	//Compute the Ratio and Ratio Error
	Double_t ratio = yPlus[ptPlus]/yMinus[ptMinus];
	Double_t ratioErr = ratio*sqrt( pow(yErrPlus[ptPlus]/yPlus[ptPlus],2) 
					 + pow(yErrMinus[ptMinus]/yMinus[ptMinus],2) );

      	pionRatios[sideIndex][ratioIndex]->
      	  SetPoint(nPtsRatio,xPlus[ptPlus],ratio);
	pionRatios[sideIndex][ratioIndex]->
	  SetPointError(nPtsRatio,0,ratioErr);

	nPtsRatio++;

      }//End mTm0Index Loop
      

    }
  }

  //Create Two new Canvases One for Each Side
  TCanvas *pionRatioCanvas[2];
  pionRatioCanvas[0] = new TCanvas("posY_Pion_Ratio","Positive Rapidity Pion Ratio");
  pionRatioCanvas[1] = new TCanvas("negY_Pion_Ratio","Negative Rapidity Pion Ratio");

  //Draw the Ratio Plots on Each Canvas
  pionRatioCanvas[0]->cd();
  pionRatios[0][0]->Draw("AP");
  pionRatios[0][1]->Draw("P");
  pionRatios[0][2]->Draw("P");

  pionRatioCanvas[1]->cd();
  pionRatios[1][0]->Draw("AP");
  pionRatios[1][1]->Draw("P");
  pionRatios[1][2]->Draw("P");


  //Create Functions to Fit the Ratios
  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t energyIndex=0; energyIndex<3; energyIndex++){
      
      //List of Parameters
      //[0] = Total Yield Ratio
      //[1] = Coulomb Potential
      //[2] = Pion Temp (FIXED)
      //[3] = Rapidity Slice (FIXED)
      //[4] = Proton Temp (Fixed)

      coulombFitFunc[sideIndex][energyIndex] = new 
	TF1("coulombFit"+sideName[sideIndex]+energyName[energyIndex],
	    pi_rat_red,1e-3,1.0,5);
      
    }
  }

  // --- Set The Parameters for Each of the Functions


  //Fix the Parameters 2,3,4 for all the Fits
  for (Int_t sideIndex=0; sideIndex<2; sideIndex++){
    for (Int_t energyIndex=0; energyIndex<3; energyIndex++){
      
      // //Pion Temperature
      // coulombFitFunc[sideIndex][energyIndex]->
      // 	FixParameter(2,spectraFitFunc[energyIndex][sideIndex][1]->
      // 		     GetParameter(1));

      // //Rapidity Slice
      // coulombFitFunc[sideIndex][energyIndex]->
      // 	FixParameter(3,0.0); //Should this be mid Rapidity ????

      // //Proton Temperature
      // coulombFitFunc[sideIndex][energyIndex]->
      // 	FixParameter(4,spectraFitFunc[energyIndex][sideIndex][2]->
      // 		     GetParameter(1));


      coulombFitFunc[sideIndex][energyIndex]->
      	FixParameter(2,.12);
      coulombFitFunc[sideIndex][energyIndex]->
      	FixParameter(3,0.0);
      coulombFitFunc[sideIndex][energyIndex]->
      	FixParameter(4,.140);

      //Initialize Parameter 0 and 1
      coulombFitFunc[sideIndex][energyIndex]->
	SetParameters(0,1.0);
      coulombFitFunc[sideIndex][energyIndex]->
	SetParameters(1,10);

      //Fit the Pion Ratios
      //pionRatios[sideIndex][energyIndex]->
      //Fit(coulombFitFunc[sideIndex][energyIndex],"R");

    }
  }

  coulombFitFunc[0][1]->Draw("SAME");

}



//___________________________________________________________________________
Double_t pi_rat_red(Double_t *x, Double_t *par)	// Function for pion ratios
{
  //double mEl = 0.51099907e-3;
  double mPi = 0.13956995   ;
  //	double mKa = 0.493677     ;
  double mPr = 0.93827231   ;
  //	double mMu = 0.1056584    ;
  //	double mDt = 1.875613     ;
	
	double a1, a2, a3;
	double E, R, Vc, p, m, mt, y, T, pt;
	double pi_rat_stnd, Red_Fac, mp, T_p, beta_pi, vel_rat, Veff, E_max;
	double fac, Jacobian, Teff, gamma;
	
	R  = par[0];		// Total yield ratio
	Vc = par[1]/1000.0;	// Coulomb Potential
	T  = par[2];		// Pion Temp (fixed)?? 
	y  = par[3];		// rapidity slice (fixed)
	T_p= par[4];		// Proton Temperature (fixed)??

	m  = mPi   ;
	mt = x[0] + m ;
	E  = mt*cosh(y);
//  E = mt;
	gamma = E/m;
	p  = sqrt(E*E - m*m);
	pt = sqrt(mt*mt - m*m);
	
//  R = 0.571 + 0.148*exp(-0.5*pow(y/0.359,2));
	
//  Determine the effective Coulomb Potential for a given mt bin
	mp = mPr;
	beta_pi = p/m;
	E_max = sqrt(mp*mp*beta_pi*beta_pi + mp*mp) - mp;
	Red_Fac = 1.0 - exp(-E_max / T_p);
//  vel_rat = sqrt(0.5 * mp * (beta_pi*beta_pi) / T_p);		// Barz' factor
//	Red_Fac = 1.0 - exp(-vel_rat*vel_rat);					// 2D factor
//	Red_Fac = erf(vel_rat);									// Barz' 3D factor
//	Red_Fac = Red_Fac - (2.0/sqrt(3.1415926)) * vel_r * exp(-vel_rat*vel_rat);		// Should vel_r be vel_rat?
//	Veff = Vc;
	Veff = Vc * Red_Fac;
//	Veff = Veff * cosh(y);

	
	if ( (p*p - 2.0*E*Veff + Veff*Veff) < 0 ) return 0;
	
//	Determine the transformation Jacobian
//	a1  = sqrt(p*p + 2.0*E*Vc + Vc*Vc);
//	a2  = sqrt(p*p - 2.0*E*Vc + Vc*Vc);

	a1  = sqrt((E+Veff)*(E+Veff) - m*m);
	a2  = sqrt((E-Veff)*(E-Veff) - m*m);
	Jacobian =  ((E-Veff)/(E+Veff))*a2/a1;
	
//	Determine the emission function factor
//	Teff= T /cosh(y);
	Teff= T;
//	fac = 1.0;
//	fac = exp(2.0*Veff/T);			// assumes exponential
	fac = (exp((E +Veff)/Teff)-1.0)/(exp((E -Veff)/Teff)-1.0); // BoseEin
//	fac = (exp((mt+Veff)/Teff)-1.0)/(exp((mt-Veff)/Teff)-1.0); // BoseEin
	
	return R * fac * Jacobian;
}
