//Draws the mid rapidity Proton Spectra for Figure 9
#include <cstdio>

//**************************************
//Variables needed for the fits
Double_t mTm0BinWidth = .025;

Double_t rangeLow_3_0 = .025;
Double_t rangeLow_3_5 = .025;
Double_t rangeLow_4_5 = .025;

//**************************************

TString PrintFitResults(TF1 *fitFunc);

void DrawFig9(Bool_t save = false){

  gSystem->Load("../bin/utilityFunctions_C.so");

  //Get the Spectra from the Data Files
  TGraphErrors *proton_3_0 = new TGraphErrors("../AuAl_3_0/Data/CorrectedSpectrum_PosY_ProtonPlus_15.final","%lg %lg %lg");
  TGraphErrors *proton_3_5 = new TGraphErrors("../AuAl_3_5/Data/CorrectedSpectrum_PosY_ProtonPlus_17.final","%lg %lg %lg");
  TGraphErrors *proton_4_5 = new TGraphErrors("../AuAl_4_5/Data/CorrectedSpectrum_PosY_ProtonPlus_20.final","%lg %lg %lg");

  //Create A Canvas                                                                                             
  TCanvas *canvas = new TCanvas("protonSpectra","Proton Spectra",20,20,800,600);
  canvas->SetLogy();
  canvas->SetTicks(1,1);
  canvas->SetTopMargin(.05);
  canvas->SetRightMargin(.05);
  canvas->SetLeftMargin(.125);

  TH1F *frame = canvas->DrawFrame(0,.1,.78,50);
  frame->SetXTitle("m_{T}-m_{p} (GeV/c^{2})");
  frame->SetYTitle("#frac{1}{N_{Evt}}#times#frac{1}{2#pim_{T}}#times#frac{d^{2}N}{dm_{T}dy}#cbar_{yMid}");
  frame->GetYaxis()->SetTitleOffset(1.35);

  //Set the Marker Style
  proton_3_0->SetMarkerStyle(kFullCircle);
  proton_3_5->SetMarkerStyle(kFullSquare);
  proton_4_5->SetMarkerStyle(kFullCross);

  //Set the Marker Color
  proton_3_0->SetMarkerColor(kGreen+3);
  proton_3_5->SetMarkerColor(kRed);
  proton_4_5->SetMarkerColor(kBlue);

  //Set Marker Size
  proton_3_0->SetMarkerSize(1.5);
  proton_3_5->SetMarkerSize(1.3);
  proton_4_5->SetMarkerSize(1.7);

  //Draw the Spectra
  proton_3_0->Draw("PZ");
  proton_3_5->Draw("PZ");
  proton_4_5->Draw("PZ");
  
  //Fit Range
  Double_t minFit(.025), maxFit(.75);

  //Create a Fit Function
  TF1 *proton_3_0_Fit = new TF1("proton_3_0_Fit",ThermalFitFunc,rangeLow_3_0,maxFit,3);
  proton_3_0_Fit->SetParameter(0,30);
  proton_3_0_Fit->SetParameter(1,.1);
  proton_3_0_Fit->FixParameter(2,GetParticleMass(4));
  
  TF1 *proton_3_5_Fit = new TF1("proton_3_5_Fit",ThermalFitFunc,rangeLow_3_5,maxFit,3);
  proton_3_5_Fit->SetParameter(0,30);
  proton_3_5_Fit->SetParameter(1,.1);
  proton_3_5_Fit->FixParameter(2,GetParticleMass(4));

  TF1 *proton_4_5_Fit = new TF1("proton_4_5_Fit",ThermalFitFunc,rangeLow_4_5,maxFit,3);
  proton_4_5_Fit->SetParameter(0,30);
  proton_4_5_Fit->SetParameter(1,.1);
  proton_4_5_Fit->FixParameter(2,GetParticleMass(4));

  //Set the Line Width
  Int_t lineWidth(3);
  proton_3_0_Fit->SetLineWidth(lineWidth);
  proton_3_5_Fit->SetLineWidth(lineWidth);
  proton_4_5_Fit->SetLineWidth(lineWidth);
  
  //Set the Line Style
  proton_3_0_Fit->SetLineStyle(1);
  proton_3_5_Fit->SetLineStyle(1);
  proton_4_5_Fit->SetLineStyle(1);
  
  //Set the Line Color
  proton_3_0_Fit->SetLineColor(kGreen+3);
  proton_3_5_Fit->SetLineColor(kRed);
  proton_4_5_Fit->SetLineColor(kBlue);

  //Fit the Spectra
  proton_3_0->Fit(proton_3_0_Fit,"REX0Q");
  proton_3_5->Fit(proton_3_5_Fit,"REX0Q");
  proton_4_5->Fit(proton_4_5_Fit,"REX0Q");

  canvas->Update();

  //Make the Legend
  TLegend *leg = new TLegend(.18,.18,.48,.35);
  leg->SetFillColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetHeader("Au_{Like}+Al Top 10% Central");
  leg->SetTextSize(.035);
  leg->AddEntry(proton_4_5,"p 4.5 GeV","P");
  leg->AddEntry(proton_3_5,"p 3.5 GeV","P");
  leg->AddEntry(proton_3_0,"p 3.0 GeV","P");
  leg->Draw();

  //Print the Fit Results
  cout <<endl;
  cout <<"PROTON SPECTRA FIT RESULTS" <<endl;
  cout <<"*************************************************************************" <<endl;
  printf("%-5s\t%-17s\t%-17s\t%-8s\n","sqrt(s_NN)","dN/dy","T_Slope (MeV)","Chi2/NDF");
  printf("%-5.01f\t%s\t%s\n",3.0,"p",PrintFitResults(proton_3_0_Fit).Data());
  printf("%-5.01f\t%s\t%s\n",3.5,"p",PrintFitResults(proton_3_5_Fit).Data());
  printf("%-5.01f\t%s\t%s\n",4.5,"p",PrintFitResults(proton_4_5_Fit).Data());
  cout <<"*************************************************************************" <<endl <<endl;


  //Save the Figures if requested
  if (save){
    
    if (!gSystem->OpenDirectory("FinalFigures")){
      gSystem->mkdir("FinalFigures");
      gSystem->cd("FinalFigures");
    }
    else
      gSystem->cd("FinalFigures");
    
    canvas->SaveAs("Fig9_ProtonSpectra.eps");
    
  }//End Save 
  
}


//______________________________________________________
TString PrintFitResults(TF1 *fitFunc){

  Double_t dNdy    = fitFunc->GetParameter(0);
  Double_t dNdyErr = fitFunc->GetParError(0);

  Double_t tSlope    = fitFunc->GetParameter(1)*1000;
  Double_t tSlopeErr = fitFunc->GetParError(1)*1000;

  Double_t chi2   = fitFunc->GetChisquare();
  Double_t ndf    = fitFunc->GetNDF();

  return TString::Format("%.04f +- %.04f\t%.04f +- %.04f\t%.04f",
                         dNdy,dNdyErr,tSlope,tSlopeErr,chi2/ndf);

}
