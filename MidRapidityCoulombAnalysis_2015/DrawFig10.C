//Draws the Pion Ratios for Figure 10

//*******************************************************

//Parameters Needed from the Pion Spectra
Double_t pionTemp_3_0 = .100; //Averaged pi+ and pi-
Double_t pionTemp_3_5 = .105; //average between 3.0 GeV and 4.5 GeV
Double_t pionTemp_4_5 = .110; //From pi-

//Parameters Needed from the Proton Spectra
Double_t protonTemp_3_0 = .128; //
Double_t protonTemp_3_5 = .141;
Double_t protonTemp_4_5 = .164;

//The Pion Ratio fit needs a value of rapidity=y-ycm.
//Since this code is particular code is meant to do the fits
//only at mid rapidity we set the value to 0;
Double_t yMinusYcm = 0;

//Fit Ranges
Double_t mTm0BinWidth = 0.025;

Double_t rangeLow_3_0 = 3 * mTm0BinWidth-mTm0BinWidth;
Double_t rangeLow_3_5 = 3 * mTm0BinWidth-mTm0BinWidth;
Double_t rangeLow_4_5 = 3 * mTm0BinWidth-mTm0BinWidth;

Double_t rangeHigh_3_0 = 20 * mTm0BinWidth;
Double_t rangeHigh_3_5 = 19 * mTm0BinWidth;
Double_t rangeHigh_4_5 = 12 * mTm0BinWidth;

//*******************************************************

//_______________________________________________________
Double_t pionRatioFit(Double_t *x, Double_t *par);
TString PrintFitResults(TF1 *fitFunc);

//_______________________________________________________
void DrawFig10(Bool_t save = false){

  gSystem->Load("../bin/utilityFunctions_C.so");

  gStyle->SetErrorX(0);
  
  //Get the Ratios from the Data Files
  TGraphErrors *pionRatio_3_0 = new TGraphErrors("../AuAl_3_0/Data/PionRatio_PosY_15.final","%lg %lg %lg %lg");
  TGraphErrors *pionRatio_3_5 = new TGraphErrors("../AuAl_3_5/Data/PionRatio_PosY_17.final","%lg %lg %lg %lg");
  TGraphErrors *pionRatio_4_5 = new TGraphErrors("../AuAl_4_5/Data/PionRatio_PosY_20.final","%lg %lg %lg %lg");

  //Create A Canvas
  TCanvas *canvas = new TCanvas("pionRatios","Pion Ratios",20,20,800,600);
  canvas->SetTicks(1,1);
  canvas->SetTopMargin(.05);
  canvas->SetRightMargin(.05);
  canvas->SetLeftMargin(.125);
  
  TH1F *frame = canvas->DrawFrame(0,.64,.54,1.16);
  frame->SetXTitle("m_{T}-m_{#pi} (GeV/c^{2})");
  frame->SetYTitle("#pi^{+}/#pi^{-}#cbar_{yMid}");
  frame->GetYaxis()->SetTitleOffset(.95);
  frame->GetYaxis()->SetTitleSize(.05);

  //Set The Marker Style
  pionRatio_3_0->SetMarkerStyle(kFullCircle);
  pionRatio_3_5->SetMarkerStyle(kFullSquare);
  pionRatio_4_5->SetMarkerStyle(kFullCross);
  
  //Set the marker Color
  pionRatio_3_0->SetMarkerColor(kGreen+3);
  pionRatio_3_5->SetMarkerColor(kRed);
  pionRatio_4_5->SetMarkerColor(kBlue);

  //Set Marker Size
  pionRatio_3_0->SetMarkerSize(1.5);
  pionRatio_3_5->SetMarkerSize(1.3);
  pionRatio_4_5->SetMarkerSize(1.7);

  //Draw the Ratios
  pionRatio_3_0->Draw("PZ");
  pionRatio_3_5->Draw("PZ");
  pionRatio_4_5->Draw("PZ");

  //Create the Coulomb Fits
  TF1 *pionRatio_3_0_Fit = new TF1("pionRatio_3_0_Fit",pionRatioFit,0,.5,5);
  pionRatio_3_0_Fit->SetParameter(0,.6);
  pionRatio_3_0_Fit->SetParameter(1,.35);
  pionRatio_3_0_Fit->FixParameter(2,pionTemp_3_0);
  pionRatio_3_0_Fit->FixParameter(3,protonTemp_3_0);
  pionRatio_3_0_Fit->FixParameter(4,yMinusYcm);

  TF1 *pionRatio_3_5_Fit = new TF1("pionRatio_3_5_Fit",pionRatioFit,0,.5,5);
  pionRatio_3_5_Fit->SetParameter(0,.6);
  pionRatio_3_5_Fit->SetParameter(1,.35);
  pionRatio_3_5_Fit->FixParameter(2,pionTemp_3_5);
  pionRatio_3_5_Fit->FixParameter(3,protonTemp_3_5);
  pionRatio_3_5_Fit->FixParameter(4,yMinusYcm);

  TF1 *pionRatio_4_5_Fit = new TF1("pionRatio_4_5_Fit",pionRatioFit,0,.5,5);
  pionRatio_4_5_Fit->SetParameter(0,.6);
  pionRatio_4_5_Fit->SetParameter(1,.35);
  pionRatio_4_5_Fit->FixParameter(2,pionTemp_4_5);
  pionRatio_4_5_Fit->FixParameter(3,protonTemp_4_5);
  pionRatio_4_5_Fit->FixParameter(4,yMinusYcm);

  //Set the Line Widths
  pionRatio_3_0_Fit->SetLineWidth(3);
  pionRatio_3_5_Fit->SetLineWidth(3);
  pionRatio_4_5_Fit->SetLineWidth(3);

  //Set the Line Colors
  pionRatio_3_0_Fit->SetLineColor(kGreen+3);
  pionRatio_3_5_Fit->SetLineColor(kRed);
  pionRatio_4_5_Fit->SetLineColor(kBlue);

  //Fit the ratios
  pionRatio_3_0->Fit(pionRatio_3_0_Fit,"EX0Q","",rangeLow_3_0,rangeHigh_3_0);
  pionRatio_3_5->Fit(pionRatio_3_5_Fit,"EX0Q","",rangeLow_3_5,rangeHigh_3_5);
  pionRatio_4_5->Fit(pionRatio_4_5_Fit,"EX0Q","",rangeLow_4_5,rangeHigh_4_5);

  //Create Functions to used for drawing the extrapolations 
  TF1 *pionRatio_3_0_FitExtrap = new TF1(*pionRatio_3_0_Fit);
  TF1 *pionRatio_3_5_FitExtrap = new TF1(*pionRatio_3_5_Fit);
  TF1 *pionRatio_4_5_FitExtrap = new TF1(*pionRatio_4_5_Fit);

  pionRatio_3_0_FitExtrap->SetRange(0,1);
  pionRatio_3_5_FitExtrap->SetRange(0,1);
  pionRatio_4_5_FitExtrap->SetRange(0,1);

  //Set the Extrapolation Line Style
  pionRatio_3_0_FitExtrap->SetLineStyle(7);
  pionRatio_3_5_FitExtrap->SetLineStyle(7);
  pionRatio_4_5_FitExtrap->SetLineStyle(7);

  //Set the Extrapolation Line Color
  pionRatio_3_0_FitExtrap->SetLineColor(kGreen+3);
  pionRatio_3_5_FitExtrap->SetLineColor(kRed);
  pionRatio_4_5_FitExtrap->SetLineColor(kBlue);

  //Draw the Extrapolation Functions
  pionRatio_3_0_FitExtrap->Draw("SAME");
  pionRatio_3_5_FitExtrap->Draw("SAME");
  pionRatio_4_5_FitExtrap->Draw("SAME");

  //Create a Legend
  TLegend *leg = new TLegend(.6,.16,.90,.35);
  leg->SetFillColor(kWhite);
  leg->SetBorderSize(0);
  leg->SetTextSize(.035);
  leg->SetHeader("Au_{Like}+Al 0-10% Central");
  leg->AddEntry(pionRatio_4_5,"4.5 GeV","P");
  leg->AddEntry(pionRatio_3_5,"3.5 GeV","P");
  leg->AddEntry(pionRatio_3_0,"3.0 GeV","P");
  leg->Draw();

  //Print the Fit Results
  cout <<endl;
  cout <<"PION RATIO COULOMB FIT RESULTS" <<endl;
  cout <<"*************************************************************************" <<endl;
  printf("%-5s\t%-17s\t%-17s\t%-8s\n","sqrt(s_NN)","Vc|y=0 (MeV)","Ri|y=0","Chi2/NDF");
  printf("%-10.01f\t%s\n",3.0,PrintFitResults(pionRatio_3_0_Fit).Data());
  printf("%-10.01f\t%s\n",3.5,PrintFitResults(pionRatio_3_5_Fit).Data());
  printf("%-10.01f\t%s\n",4.5,PrintFitResults(pionRatio_4_5_Fit).Data());
  cout <<"*************************************************************************" <<endl <<endl;

  //Save the Figures if requested
  if (save){
    
    if (!gSystem->OpenDirectory("FinalFigures")){
      gSystem->mkdir("FinalFigures");
      gSystem->cd("FinalFigures");
    }
    else
      gSystem->cd("FinalFigures");
    
    canvas->SaveAs("Fig10_PionRatios.eps");
    
  }//End Save     


}

//________________________________________________________
TString PrintFitResults(TF1 *fitFunc){
  
  Double_t initialPionRatio     = fitFunc->GetParameter(0);
  Double_t initialPionRatioErr = fitFunc->GetParError(0);

  Double_t coulombPotential    = fitFunc->GetParameter(1);
  Double_t coulombPotentialErr = fitFunc->GetParError(1);

  Double_t chi2 = fitFunc->GetChisquare();
  Double_t ndf = fitFunc->GetNDF();

  return TString::Format("%.04f +- %.04f\t%.04f +- %.04f\t%.04f",
                         coulombPotential,coulombPotentialErr,
			 initialPionRatio,initialPionRatioErr,
			 chi2/ndf);

}

//_________________________________________________________
Double_t pionRatioFit(Double_t *x, Double_t *par){

  Double_t mTm0 = x[0];

  //Fit Parameters
  Double_t initialRatio = par[0];
  Double_t coulombV = par[1]/1000.0;
  Double_t pionTemp = par[2];  //This Should be Fixed the pion Spectra Fit
  Double_t protonTemp = par[3];//This Should be Fixed from the proton Spectra Fit
  Double_t y = par[4];         //This Should be Fixed (y-yMid)

  //Additional Variables for -->Pion<--
  Double_t m0 = GetParticleMass(0);
  Double_t mT = mTm0 + m0;
  Double_t energy = mT*cosh(y);
  Double_t p = sqrt(energy*energy - m0*m0);
  Double_t betaGamma = p / m0;

  //Additional Variables for -->Proton<--
  Double_t mP = GetParticleMass(4);

  //Compute the Effective Coulomb Potential
  Double_t eMax = sqrt(pow(mP*betaGamma,2) + pow(mP,2)) - mP;
  Double_t effCoulombV = coulombV * (1.0 - exp(-eMax/protonTemp));


  //Compute the Transformation Jacobian
  Double_t num = sqrt(pow(energy - effCoulombV,2) - pow(m0,2));
  Double_t denom = sqrt(pow(energy + effCoulombV,2) - pow(m0,2));
  Double_t Jacobian = (energy - effCoulombV) / (energy + effCoulombV);
  Jacobian = Jacobian * ( num/denom );

  //Compute the Emmision Factor for Bose Einstein
  Double_t emmisionFactor = exp( (energy + effCoulombV)/pionTemp ) - 1.0;
  emmisionFactor = emmisionFactor / (exp( (energy - effCoulombV)/pionTemp) - 1.0);

  //Assemble it all Together
  return initialRatio * emmisionFactor * Jacobian;


}
