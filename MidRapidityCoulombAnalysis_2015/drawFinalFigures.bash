#!/bin/bash

#Help
help_func(){
    echo ""
    echo "HELP INFORMATION"
    echo "    NAME:"
    echo "        drawFinalFigures.bash - draw the final figures with fits"
    echo "    USAGE:"
    echo "        ./drawFinalFigures.bash "
    echo "    REQUIRED ARGUMENTS:"
    echo "        There are no required arguments."
    echo "    NOTE: "
    echo "        This script calls macros which are specifically for the coulomb analysis of"
    echo "        to be published in late 2015 or early 2016. "
    echo "        Figures will be save in ./FinalFigures/"
    echo ""
}

#Check for Options
imageDir=""
while [ "$#" -gt 0 ]; do
    while getopts "h" opts; do
        case "$opts" in
            h) help_func; exit 1;;
            ?) exit 1;;
            *) echo "For help use option: -h"; exit 1;;
        esac
        shift
        OPTIND=1
    done

    if [ "$#" -gt 0 ]; then
        POSITIONALPARAM=(${POSITIONALPARAM[@]} $1)
        shift
        OPTIND=1
    fi
done

#To Save or not to save ... that is the question
save=1

root -l -b -q DrawFig23567.C\($save\)

root -l -b -q DrawFig8.C\($save\)

root -l -b -q DrawFig9.C\($save\)

root -l -b -q DrawFig10.C\($save\)

root -l -b -q DrawFigPionEfficiency.C\($save\)

root -l -b -q DrawFig_YieldExtraction.C\($save\)
